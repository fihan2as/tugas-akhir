<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Pengumuman;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Psy\Command\WhereamiCommand;

class PengumumanController extends Controller
{
    public function buatpengumuman(Request $request)
    {
        $buatizin = Pengumuman::create([
            'id_admin' => Auth::user()->id,
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_pengumuman' => $request->tanggal_pengumuman,
            'tanggal_dibuat' => Carbon::now('Asia/Jakarta')->format('Y-m-d')
        ]);
        $tes = DB::table('akunpegawai')->where('id_admin', Auth::user()->id)->where('tokendevice', '!=', "")->pluck('tokendevice')->toArray();
        $notification_id = array_unique($tes);
        $pt = DB::table('users')->where('id', Auth::user()->id)->first();
        $title = "Pengumuman $pt->nama_perusahaan";
        $message = "Pengumuman $buatizin->judul";
        $res = send_notification_FCM($notification_id, $title, $message);

        return response()->json([
            'notif' => $res,
            'success' => true,
            'data' => $buatizin
        ]);
    }

    public function editpengumuman(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_pengumuman' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Update Data Gagal!',
            ]);
        } else {
            DB::table('pengumuman')->where('id', $request->id)->update([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'tanggal_pengumuman' => $request->tanggal_pengumuman
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Update Pengumuman Berhasil!',
            ]);
        }
    }

    public function hapuspengumuman($id)
    {
        $data = Pengumuman::findOrFail($id);
        $data->delete();
        return response()->json([
            'success' => true,
            'message' => 'Hapus Pengumuman Berhasil!',
        ]);
    }

    public function getpengumuman()
    {
        $timezone = 'Asia/Jakarta';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $tanggal = $date->format('Y-m-d');
        $pengumuman = DB::table('pengumuman')
            ->where('id_admin', Auth::user()->id)
            ->whereDate('tanggal_pengumuman', '>=', $tanggal)
            ->get()
            ->toArray();

        return response()->json([
            'success' => true,
            'data' => $pengumuman,
            'message' => 'Get Pengumuman Berhasil!',
        ]);
    }
    public function getpengumumanpeg()
    {
        $timezone = 'Asia/Jakarta';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $tanggal = $date->format('Y-m-d');
        $pengumuman = DB::table('pengumuman')
            ->where('id_admin', Auth::user()->id_admin)
            ->whereDate('tanggal_pengumuman', '>=', $tanggal)
            ->orderBy('tanggal_pengumuman')
            ->get();

        return response()->json([
            'success' => true,
            'data' => $pengumuman,
            'message' => 'Get Pengumuman Berhasil!',
        ]);
    }

    public function detailpengumuman($id)
    {
        $get = DB::table('pengumuman')
            ->where('id', $id)
            ->get();
        return response([
            'data' => $get,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
}
