<?php

namespace App\Http\Controllers\API;

use App\Models\Absensi;
use App\Models\AkunPegawai;
use App\Models\HariKerja;
use App\Models\Jabatan;
use App\Models\Lembur;
use App\Models\Pemberitahuan;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Mail\SendGaji;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\Penggajian;
use App\Models\Gaji;
use App\Models\RiwayatGaji;

class DataGajiController extends Controller
{
    public function cetakgaji($id)
    {
        $cetak = RiwayatGaji::find($id);
        $akun = DB::table('pegawais')->where('email', $cetak->email)->first();
        $infopt = DB::table('users')->where('id', $cetak->id_admin)->first();
        $jabatan = DB::table('jabatan')->where('id_admin', $cetak->id_admin)->where('id', $cetak->id_jabatan)->pluck('jabatan')->first();

        $data = [
            'namapt' => $infopt->nama_perusahaan,
            'kota' => $infopt->kota,
            'provinsi' => $infopt->provinsi,
            'detalamat' => $infopt->det_alamat,
            'kodepos' => $infopt->kode_pos,
            'nama' => $akun->nama_lengkap,
            'no_transaksi' => $cetak->no_transaksi,
            'nopegawai' => $akun->no_pegawai,
            'jabatanadmin' => $infopt->jabatan,
            'potongabsen' => $cetak->potong_absen,
            'tidakmasuk' => $cetak->tidak_masuk,
            'totallembur' => $cetak->total_lembur,
            'jamlembur' => $cetak->jamlembur_total,
            'jabatan' => $jabatan,
            'email' => $cetak->email,
            'gajibersih' => $cetak->gaji_bersih,
            'gajipokok' => $cetak->gaji_pokok,
            'gajiabsen' => $cetak->gaji_pokok - $cetak->potong_absen,
            'gajikotor' => $cetak->gaji_kotor,
            'bonus' => explode(',', $cetak->bonus),
            'nombon' => explode(',', $cetak->nominal_bonus),
            'tunjangan' => explode(',', $cetak->tunjangan),
            'nomtun' => explode(',', $cetak->nominal_tunjangan),
            'potongan' => explode(',', $cetak->potongan),
            'nompot' => explode(',', $cetak->nominal_potongan),
            'totalpotongan' => $cetak->total_potongan,
            'totaltunjangan' => $cetak->total_tunjangan,
            'totalbonus' => $cetak->total_bonus,
            'tanggal' => $cetak->tanggal_ambil
        ];

        return response()->json([
            'data' => $data,
            'success' => true
        ]);
    }
    public function sudahisi(Request $request)
    {
        $sudah = Gaji::where('id_admin', Auth::user()->id)->get()->toArray();
        $email = Gaji::where('id_admin', Auth::user()->id)->pluck('email');
        $jabatan = Gaji::where('id_admin', Auth::user()->id)->pluck('id_jabatan');
        return response([
            'data' => $sudah,
            'email' => $email,
            'jabatan' => $jabatan,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
    public function ajukancair(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Pencairan Gaji Gagal!',
            ]);
        } else {
            $status = DB::table('riwayatgaji')->where('id', $request->id)->update([
                'status' => 'Mengajukan',
            ]);
            return response()->json([
                'success' => true,
                'data' => $status,
                'message' => 'Pengajuan Cair Gaji!',
            ]);
        }
    }
    public function cairgaji(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Pencairan Gaji Gagal!',
            ]);
        } else {
            $status = DB::table('riwayatgaji')->where('id', $request->id)->update([
                'status' => $request->status,
            ]);
            $hasil = DB::table('riwayatgaji')->where('id', $request->id)->first();
            $akun = DB::table('pegawais')->where('email', $hasil->email)->first();
            $infopt = DB::table('users')->where('id', $akun->id_admin)->first();
            $jabatan = DB::table('jabatan')->where('id_admin', $akun->id_admin)->where('id', $akun->id_jabatan)->pluck('jabatan')->first();

            $data = [
                'namapt' => $infopt->nama_perusahaan,
                'kota' => $infopt->kota,
                'provinsi' => $infopt->provinsi,
                'detalamat' => $infopt->det_alamat,
                'kodepos' => $infopt->kode_pos,
                'nama' => $akun->nama_lengkap,
                'no_transaksi' => $hasil->no_transaksi,
                'nopegawai' => $akun->no_pegawai,
                'jabatanadmin' => $infopt->jabatan,
                'jabatan' => $jabatan,
                'judul' => "Pencairan Gaji",
                'email' => $hasil->email,
                'potongabsen' => $hasil->potong_absen,
                'tidakmasuk' => $hasil->tidak_masuk,
                'gajibersih' => $hasil->gaji_bersih,
                'gajipokok' => $hasil->gaji_pokok,
                'gajikotor' => $hasil->gaji_kotor,
                'gajiabsen' => $hasil->gaji_pokok - $hasil->potong_absen,
                'totallembur' => $hasil->total_lembur,
                'jamlembur' => $hasil->jamlembur_total,
                'bonus' => explode(',', $hasil->bonus),
                'nombon' => explode(',', $hasil->nominal_bonus),
                'tunjangan' => explode(',', $hasil->tunjangan),
                'nomtun' => explode(',', $hasil->nominal_tunjangan),
                'potongan' => explode(',', $hasil->potongan),
                'nompot' => explode(',', $hasil->nominal_potongan),
                'totalpotongan' => $hasil->total_potongan,
                'totaltunjangan' => $hasil->total_tunjangan,
                'totalbonus' => $hasil->total_bonus,
                'tanggal' => $hasil->tanggal_ambil
            ];
            $timezone = 'Asia/Jakarta';
            $date = new DateTime('now', new DateTimeZone($timezone));
            $tanggal = $date->format('Y-m-d');
            $localtime = $date->format('H:i:s');
            Pemberitahuan::create([
                'id_admin' => Auth::user()->id,
                'email' => $akun->email,
                'judul' => 'Pencairan Gaji ',
                'jenis' => 'Penggajian',
                'status' => 'Berhasil Cair',
                'tanggal' => $tanggal,
                'jam' => $localtime
            ]);
            $email = DB::table('riwayatgaji')->where('id', $request->id)->first();
            $tes = DB::table('akunpegawai')->where('id_admin', Auth::user()->id)->where('email', $email->email)->pluck('tokendevice')->toArray();
            $notification_id = array_unique($tes);
            $pt = DB::table('users')->where('id', Auth::user()->id)->first();
            $title = "$pt->nama_perusahaan";
            $message = "Gaji Anda Telah Dicairkan, Cek Email Anda";
            send_notification_FCM($notification_id, $title, $message);

            Mail::to($hasil->email)->send(new SendGaji($data));
            return response()->json([
                'data' => $status,
                'cair' => $hasil,
                'success' => true,
                'message' => 'Gaji Telah Cair!',
            ]);
        }
    }
    public function cairgajimobile(Request $request, $id)
    {

        $status = DB::table('riwayatgaji')->where('id', $id)->update([
            'status' => "Mengajukan",
        ]);
        $hasil = DB::table('riwayatgaji')->where('id', $id)->first();
        $akun = DB::table('pegawais')->where('email', $hasil->email)->first();
        $infopt = DB::table('users')->where('id', $akun->id_admin)->first();
        $jabatan = DB::table('jabatan')->where('id_admin', $akun->id_admin)->where('id', $akun->id_jabatan)->pluck('jabatan')->first();

        $data = [
            'namapt' => $infopt->nama_perusahaan,
            'kota' => $infopt->kota,
            'provinsi' => $infopt->provinsi,
            'detalamat' => $infopt->det_alamat,
            'kodepos' => $infopt->kode_pos,
            'nama' => $akun->nama_lengkap,
            'no_transaksi' => $hasil->no_transaksi,
            'nopegawai' => $akun->no_pegawai,
            'jabatanadmin' => $infopt->jabatan,
            'jabatan' => $jabatan,
            'judul' => "Pencairan Gaji",
            'email' => $hasil->email,
            'potongabsen' => $hasil->potong_absen,
            'tidakmasuk' => $hasil->tidak_masuk,
            'gajibersih' => $hasil->gaji_bersih,
            'gajipokok' => $hasil->gaji_pokok,
            'gajikotor' => $hasil->gaji_kotor,
            'gajiabsen' => $hasil->gaji_pokok - $hasil->potong_absen,
            'totallembur' => $hasil->total_lembur,
            'jamlembur' => $hasil->jamlembur_total,
            'bonus' => explode(',', $hasil->bonus),
            'nombon' => explode(',', $hasil->nominal_bonus),
            'tunjangan' => explode(',', $hasil->tunjangan),
            'nomtun' => explode(',', $hasil->nominal_tunjangan),
            'potongan' => explode(',', $hasil->potongan),
            'nompot' => explode(',', $hasil->nominal_potongan),
            'totalpotongan' => $hasil->total_potongan,
            'totaltunjangan' => $hasil->total_tunjangan,
            'totalbonus' => $hasil->total_bonus,
            'tanggal' => $hasil->tanggal_ambil
        ];
        $timezone = 'Asia/Jakarta';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $tanggal = $date->format('Y-m-d');
        $localtime = $date->format('H:i:s');
        Pemberitahuan::create([
            'id_admin' => Auth::user()->id,
            'email' => $akun->email,
            'judul' => 'Pencairan Gaji ',
            'jenis' => 'Penggajian',
            'status' => 'Berhasil Cair',
            'tanggal' => $tanggal,
            'jam' => $localtime
        ]);
        Mail::to($hasil->email)->send(new SendGaji($data));
        return response()->json([
            'data' => $status,
            'cair' => $hasil,
            'success' => true,
            'message' => 'Gaji Telah Cair!',
        ]);
    }
    public function getEmail(Request $request)
    {
        $data = Gaji::where('id_admin', Auth::user()->id)->where('id_jabatan', $request->id_jabatan)->get();
        return response()->json($data);
    }
    public function riwayatgaji(Request $request)
    {
        $riwayat = RiwayatGaji::where('id_admin', Auth::user()->id)->where('status', '=', "Mengajukan")->latest()->paginate(8);
        return response([
            'data' => $riwayat,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
    public function riwayatpenggajian(Request $request)
    {
        // $riwayat = RiwayatGaji::where('id_admin' ,Auth::user()->id)->where('status','=',"Cair")->latest()->paginate(8);
        // $number = RiwayatGaji::where('id_admin', Auth::user()->id)->where('status', '=', "Cair")->get()->toArray();
        //     return response([
        //         'data' => $riwayat,
        //         'number' => count($number),
        //         'message' => 'get data berhasil',
        //         'status' => true,
        //     ]);
        return RiwayatGaji::where('id_admin', Auth::user()->id)->where('status', '=', "Cair")->when(request('search'), function ($query) {
            $query->where('email', 'like', '%' . request('search') . '%');
        })->latest()->paginate(8);
    }
    public function searchriwayat($key)
    {
        $result = DB::table('riwayatgaji')
            ->select('*')
            ->where('riwayatgaji.id_admin', Auth::user()->id)
            ->where('riwayatgaji.status', 'Cair')
            ->where('email', 'like', '%' . $key . '%')
            ->orWhere('tanggal_ambil', 'like', '%' . $key . '%')
            ->where('riwayatgaji.status', 'Cair')
            ->where('riwayatgaji.id_admin', Auth::user()->id)
            ->latest()
            ->paginate(8);
        return $result;
    }
    public function searchcair($key)
    {
        $result = DB::table('riwayatgaji')
            ->select('*')
            ->where('riwayatgaji.id_admin', Auth::user()->id)
            ->where('riwayatgaji.status', 'Mengajukan')
            ->where('email', 'like', '%' . $key . '%')
            ->where('riwayatgaji.status', 'Mengajukan')
            ->where('riwayatgaji.id_admin', Auth::user()->id)
            ->latest()
            ->paginate(8);
        return $result;
    }
    public function searchriwayatpeg($key)
    {
        $result = DB::table('riwayatgaji')
            ->select('*')
            ->where('riwayatgaji.email', Auth::user()->email)
            ->where('tanggal_ambil', 'like', '%' . $key . '%')
            ->where('riwayatgaji.email', Auth::user()->email)
            ->get();

        return $result;
    }
    public function riwayatgajipeg(Request $request)
    {
        $riwayat = RiwayatGaji::where('email', Auth::user()->email)->where('status', '!=', 'Belum Diambil')->latest()->paginate(8);
        return response([
            'data' => $riwayat,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
    public function riwayatgajipegmobile(Request $request)
    {
        $riwayat = DB::table('riwayatgaji')->select('*')->where('email', Auth::user()->email)->where('status', '=', 'Mengajukan')->get()->toArray();

        foreach ($riwayat as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);

            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
            }
        }
        return response([
            'data' => $riwayat,
            'jabatan' => $valjab,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
    public function riwayatfinal(Request $request)
    {
        // $riwayat = RiwayatGaji::where('email', Auth::user()->email)->where('status', '=', 'Cair')->latest()->paginate(8);
        // return response([
        //     'data' => $riwayat,
        //     'message' => 'get data berhasil',
        //     'status' => true,
        // ]);
        return RiwayatGaji::where('email', Auth::user()->email)->where('status', '=', 'Cair')->when(request('search'), function ($query) {
            $query->where('tanggal_ambil', 'like', '%' . request('search') . '%');
        })->latest()->paginate(8);
    }
    public function riwayatfinalmobile(Request $request)
    {
        $riwayat = RiwayatGaji::where('email', Auth::user()->email)->where('status', '=', 'Cair')->latest()->get();
        foreach ($riwayat as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);

            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
            }
        }
        return response([
            'data' => $riwayat,
            'jabatan' => $valjab,
            'message' => 'get data berhasil',
            'status' => true,
        ]);
    }
    public function searchgaji($key)
    {
        $result = DB::table('penggajian')
            ->select('*')
            ->where('penggajian.id_admin', Auth::user()->id)
            ->where('email', 'like', '%' . $key . '%')
            ->where('penggajian.id_admin', Auth::user()->id)
            ->latest()
            ->get()->toArray();

        foreach ($result as $i => $tes) {
            $tun[$i] = explode(',', $tes->id_tunjangan);
            foreach ($tun[$i] as $index => $row) {
                $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                $val[$i] = $jenis[$i];
                $nom[$i] = $nominalALL[$i];
                $totaltun[$i] = array_sum($nominalALL[$i]);
            }
        }
        foreach ($result as $j => $tes2) {
            $bon[$j] = explode(',', $tes2->id_bonus);
            foreach ($bon[$j] as $nus => $coba) {
                $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                $valBon[$j] = $jenisBon[$j];
                $nomBon[$j] = $nominalBon[$j];
                $totalbon[$j] = array_sum($nominalBon[$j]);
            }
        }
        foreach ($result as $x => $tes3) {
            $pot[$x] = explode(',', $tes3->id_potongan);
            foreach ($pot[$x] as $tongan => $coba2) {
                $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                $valPot[$x] = $jenisPot[$x];
                $nomPot[$x] = $nominalPot[$x];
                $totalpot[$x] = array_sum($nominalPot[$x]);
            }
        }
        foreach ($result as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);

            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
            }
        }

        if ($result != null) {
            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab) as $key) {
                $akhir[$key] = array($totaltun[$key] + $nomjab[$key] + $totalbon[$key] - $totalpot[$key]);
            }
            return response()->json([
                'data' => $result,
                'tunjangan' => $val,
                'nominal' => $nom,
                'jabatan' => $valjab,
                'gaji' => $nomjab,
                'total_tunjangan' => $totaltun,
                'bonus' => $valBon,
                'nominal_bonus' => $nomBon,
                'total_bonus' => $totalbon,
                'potongan' => $valPot,
                'nominal_potongan' => $nomPot,
                'total_potongan' => $totalpot,
                'hasil' => $akhir,
                'message' => 'get data berhasil',
                'status' => true
            ]);
        } else {
            return response()->json([
                'message' => 'tidak ada data',
                'status' => true
            ]);
        }
    }
    public function allgaji(Request $request)
    {
        $tunjangan = DB::table('penggajian')
            ->select('*')
            ->where('id_admin', Auth::user()->id)
            ->where('status', 'Belum Diambil')
            ->latest()
            ->get()->toArray();
        $potongabsen = DB::table('penggajian')
            ->where('id_admin', Auth::user()->id)
            ->where('status', 'Belum Diambil')
            ->latest()
            ->pluck('potong_absen')->toArray();
        $lembur = DB::table('penggajian')
            ->where('id_admin', Auth::user()->id)
            ->where('status', 'Belum Diambil')
            ->latest()
            ->pluck('total_lembur')->toArray();
        $status = DB::table('penggajian')
            ->select('*')
            ->where('id_admin', Auth::user()->id)
            ->whereIn('status', ['Belum Diambil'])
            ->get();
        foreach ($tunjangan as $i => $tes) {
            $tun[$i] = explode(',', $tes->id_tunjangan);
            foreach ($tun[$i] as $index => $row) {
                $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                $val[$i] = $jenis[$i];
                $nom[$i] = $nominalALL[$i];
                $totaltun[$i] = array_sum($nominalALL[$i]);
            }
        }
        foreach ($tunjangan as $j => $tes2) {
            $bon[$j] = explode(',', $tes2->id_bonus);
            foreach ($bon[$j] as $nus => $coba) {
                $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                $valBon[$j] = $jenisBon[$j];
                $nomBon[$j] = $nominalBon[$j];
                $totalbon[$j] = array_sum($nominalBon[$j]);
            }
        }
        foreach ($tunjangan as $x => $tes3) {
            $pot[$x] = explode(',', $tes3->id_potongan);
            foreach ($pot[$x] as $tongan => $coba2) {
                $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                $valPot[$x] = $jenisPot[$x];
                $nomPot[$x] = $nominalPot[$x];
                $totalpot[$x] = array_sum($nominalPot[$x]);
            }
        }
        foreach ($tunjangan as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);
            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
            }
        }
        if ($tunjangan != null) {
            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potongabsen + $lembur) as $key) {
                $akhir[$key] = array($totaltun[$key] + $nomjab[$key] + $totalbon[$key] + $lembur[$key] - $totalpot[$key] - $potongabsen[$key]);
            }

            return response()->json([
                'data' => $tunjangan,
                'cekstatus' => $status,
                'tunjangan' => $val,
                'nominal' => $nom,
                'jabatan' => $valjab,
                'gaji' => $nomjab,
                'total_tunjangan' => $totaltun,
                'bonus' => $valBon,
                'nominal_bonus' => $nomBon,
                'total_bonus' => $totalbon,
                'potongan' => $valPot,
                'nominal_potongan' => $nomPot,
                'total_potongan' => $totalpot,
                'hasil' => $akhir,
                'message' => 'get data berhasil',
                'status' => true
            ]);
        } else {
            return response()->json([
                'message' => 'tidak ada data',
                'status' => null
            ]);
        }
    }
    public function buatgaji(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            // 'id_bonus' => 'required',
            // 'id_tunjangan' => 'required',
            // 'id_potongan' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Data tidak lengkap',
            ]);
        } else {
            $tunjanganArray = implode(",", $request->id_tunjangan);
            $bonusArray = implode(",", $request->id_bonus);
            $potonganArray = implode(",", $request->id_potongan);
            $user = Gaji::where('email', $request->email)->first();
            $jabat = $user->id_jabatan;
            $transaksi = random_int(10000000, 99999999);

            $tanpatun = DB::table('tunjangan')->where('email_admin', Auth::user()->email)->where('jenis_tunjangan', '-')->pluck('id')->first();
            $tanpabon = DB::table('bonus')->where('email_admin', Auth::user()->email)->where('jenis_bonus', '-')->pluck('id')->first();
            $tanpapot = DB::table('potongan')->where('email_admin', Auth::user()->email)->where('jenis_potongan', '-')->pluck('id')->first();
            if ($tunjanganArray == null && $bonusArray == null && $potonganArray == null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);
                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tanpatun,
                    'id_bonus' => $tanpabon,
                    'id_potongan' => $tanpapot,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'jam' => $totaljam,
                    'upah' => $totalupah,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($bonusArray == null && $potonganArray == null && $tunjanganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tunjanganArray,
                    'id_bonus' => $tanpabon,
                    'id_potongan' => $tanpapot,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($tunjanganArray == null && $potonganArray == null && $bonusArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tanpatun,
                    'id_bonus' => $bonusArray,
                    'id_potongan' => $tanpapot,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($tunjanganArray == null && $bonusArray == null && $potonganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tanpatun,
                    'id_bonus' => $tanpabon,
                    'id_potongan' => $potonganArray,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($tunjanganArray == null && $bonusArray != null && $potonganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tanpatun,
                    'id_bonus' => $bonusArray,
                    'id_potongan' => $potonganArray,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($bonusArray == null && $tunjanganArray != null && $potonganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tunjanganArray,
                    'id_bonus' => $tanpabon,
                    'id_potongan' => $potonganArray,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($potonganArray == null && $bonusArray != null && $tunjanganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tunjanganArray,
                    'id_bonus' => $bonusArray,
                    'id_potongan' => $tanpapot,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
            if ($tunjanganArray != null && $bonusArray != null && $potonganArray != null) {
                $absen = Absensi::where('email', $request->input('email'))
                    ->whereMonth('tanggal', Carbon::now()->month)
                    ->where(
                        function ($query) {
                            return $query
                                ->where('jam_kerja', '!=', null)
                                ->orWhere('keterangan', "Cuti")
                                ->orWhere('keterangan', "Izin")
                                ->orWhere('keterangan', "ReqAbsen");
                        }
                    )->count();
                $hari = HariKerja::where('id_admin', Auth::user()->id)
                    ->pluck('hari_kerja');
                $simpan = explode(',', $hari);
                $totalhari = (count($simpan) * 4) + 2;
                $akun = AkunPegawai::where('email', $request->input('email'))->pluck('id_jabatan');
                $jabatan = Jabatan::where('id', $akun)->first();
                $hitung = round(($absen / $totalhari) * $jabatan->gaji);
                $potongan = round($jabatan->gaji - $hitung);
                $tidakmasuk = $totalhari - $absen;

                $cekjam = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('jumlah_jam')->toArray();
                $cekupah = Lembur::where('email', $request->input('email'))
                    ->whereMonth('tanggal_lembur', Carbon::now()->month)
                    ->where('status_lembur', '=', "Diterima")
                    ->pluck('upah_lembur')->toArray();
                $totaljam = array_sum($cekjam);
                $totalupah = array_sum($cekupah);

                $buatgaji = Penggajian::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'id_golongan' => $request->id_golongan,
                    'tanggal' => Carbon::now(),
                    'id_tunjangan' => $tunjanganArray,
                    'id_bonus' => $bonusArray,
                    'id_potongan' => $potonganArray,
                    'no_transaksi' => $transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);

                $buatgaji2 = RiwayatGaji::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $request->email,
                    'id_jabatan' => $jabat,
                    'tanggal_ambil' => Carbon::now(),
                    'id_golongan' => $request->id_golongan,
                    'no_transaksi' => $buatgaji->no_transaksi,
                    'potong_absen' => $potongan,
                    'tidak_masuk' => $tidakmasuk,
                    'jamlembur_total' => $totaljam,
                    'total_lembur' => $totalupah,
                ]);
                $timezone = 'Asia/Jakarta';
                $date = new DateTime('now', new DateTimeZone($timezone));
                $tanggal = $date->format('Y-m-d');
                $localtime = $date->format('H:i:s');
                Pemberitahuan::create([
                    'id_admin' => Auth::user()->id,
                    'email' => $buatgaji2->email,
                    'judul' => 'Gaji Anda Telah Dibuat',
                    'jenis' => 'Penggajian',
                    'status' => 'Belum Diambil',
                    'tanggal' => $tanggal,
                    'jam' => $localtime
                ]);
                return response()->json([
                    'data' => $buatgaji,
                    'riwayat' => $buatgaji2,
                    'success' => true,
                    'message' => 'Buat Gaji Berhasil!',
                ]);
            }
        }
    }
    public function updategaji(Request $request)
    {

        $tanpatun = DB::table('tunjangan')->where('email_admin', Auth::user()->email)->where('jenis_tunjangan', '-')->pluck('id')->first();
        $tanpabon = DB::table('bonus')->where('email_admin', Auth::user()->email)->where('jenis_bonus', '-')->pluck('id')->first();
        $tanpapot = DB::table('potongan')->where('email_admin', Auth::user()->email)->where('jenis_potongan', '-')->pluck('id')->first();

        $tunjanganArray = implode(",", $request->id_tunjangan);
        $bonusArray = implode(",", $request->id_bonus);
        $potonganArray = implode(",", $request->id_potongan);
        $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
            'id_tunjangan' => $tunjanganArray,
            'id_bonus' => $bonusArray,
            'id_potongan' => $potonganArray,
        ]);
        $gaji = DB::table('penggajian')->where('id', $request->id)->first();
        if ($tunjanganArray == null && $bonusArray == null && $potonganArray == null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tanpatun,
                'id_bonus' => $tanpabon,
                'id_potongan' => $tanpapot,
            ]);
        }
        if ($tunjanganArray == null && $bonusArray == null && $potonganArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tanpatun,
                'id_bonus' => $tanpabon,
                'id_potongan' => $potonganArray,
            ]);
        }
        if ($tunjanganArray == null && $potonganArray == null && $bonusArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tanpatun,
                'id_bonus' => $bonusArray,
                'id_potongan' => $tanpapot,
            ]);
        }
        if ($bonusArray == null && $potonganArray == null && $tunjanganArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tunjanganArray,
                'id_bonus' => $tanpabon,
                'id_potongan' => $tanpapot,
            ]);
        }
        if ($tunjanganArray == null && $potonganArray != null && $bonusArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tanpatun,
                'id_bonus' => $bonusArray,
                'id_potongan' => $potonganArray,
            ]);
        }
        if ($bonusArray == null && $potonganArray != null && $tunjanganArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tunjanganArray,
                'id_bonus' => $tanpabon,
                'id_potongan' => $potonganArray,
            ]);
        }
        if ($potonganArray == null && $tunjanganArray != null && $bonusArray != null) {
            $updategaji = DB::table('penggajian')->where('id', $request->id)->update([
                'id_tunjangan' => $tunjanganArray,
                'id_bonus' => $bonusArray,
                'id_potongan' => $tanpapot,
            ]);
        }
        return response()->json([
            'data' => $updategaji,
            'tes' => count(explode(',', $gaji->id_tunjangan)),
            'success' => true,
            'message' => 'Update Gaji Berhasil!',
        ]);
    }
    public function detailgaji($id)
    {
        $detgaji = DB::table('penggajian')
            ->where('id', $id)
            ->get()->toArray();
        $potongabsen = DB::table('penggajian')
            ->where('id', $id)
            ->latest()
            ->pluck('potong_absen')->toArray();
        $lembur = DB::table('penggajian')
            ->where('id', $id)
            ->latest()
            ->pluck('total_lembur')->toArray();
        foreach ($detgaji as $i => $tes) {
            $tun[$i] = explode(',', $tes->id_tunjangan);
            foreach ($tun[$i] as $index => $row) {
                $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                $val[$i] = $jenis[$i];
                $nom[$i] = $nominalALL[$i];
                $totaltun[$i] = array_sum($nominalALL[$i]);
                $arrtun[$i] = array($totaltun[$i]);
            }
        }
        foreach ($detgaji as $j => $tes2) {
            $bon[$j] = explode(',', $tes2->id_bonus);
            foreach ($bon[$j] as $nus => $coba) {
                $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                $valBon[$j] = $jenisBon[$j];
                $nomBon[$j] = $nominalBon[$j];
                $totalbon[$j] = array_sum($nominalBon[$j]);
                $arrbon[$j] = array($totalbon[$j]);
            }
        }
        foreach ($detgaji as $x => $tes3) {
            $pot[$x] = explode(',', $tes3->id_potongan);
            foreach ($pot[$x] as $tongan => $coba2) {
                $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                $valPot[$x] = $jenisPot[$x];
                $nomPot[$x] = $nominalPot[$x];
                $totalpot[$x] = array_sum($nominalPot[$x]);
                $arrpot[$x] = array($totalpot[$x]);
            }
        }
        foreach ($detgaji as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);

            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
                $arrjab[$a] = array($nomjab[$a]);
            }
        }
        foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potongabsen + $lembur) as $key) {
            $akhir[$key] = array($totaltun[$key] + $nomjab[$key] + $lembur[$key] + $totalbon[$key] - $totalpot[$key] - $potongabsen[$key]);
        }
        foreach (array_keys($totaltun + $totalbon + $nomjab + $potongabsen + $lembur) as $key) {
            $subtotal[$key] = array($totaltun[$key] + $nomjab[$key] + $lembur[$key] + $totalbon[$key] - $potongabsen[$key]);
        }

        return response()->json([
            'data' => $detgaji,
            'tunjangan' => $val,
            'arrtun' => $arrtun,
            'arrbon' => $arrbon,
            'arrpot' => $arrpot,
            'arrjab' => $arrjab,
            'nominal' => $nom,
            'jabatan' => $valjab,
            'gaji' => $nomjab,
            'total_tunjangan' => $totaltun,
            'bonus' => $valBon,
            'nominal_bonus' => $nomBon,
            'total_bonus' => $totalbon,
            'potongan' => $valPot,
            'nominal_potongan' => $nomPot,
            'total_potongan' => $totalpot,
            'hasil' => $akhir,
            'subtotal' => $subtotal,
            'message' => 'get data berhasil',
            'status' => true
        ]);
    }
    public function countgaji()
    {
        $gajipeg = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->latest()->get();
        $count = $gajipeg->count();
        return response()->json([
            'data' => $count
        ]);
    }
    public function gajipegawai()
    {
        $gajipeg = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->get();
        $potongabsen = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->pluck('potong_absen')->toArray();
        $lembur = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->pluck('total_lembur')->toArray();
        $gajipeg2 = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->get()->toArray();
        if ($gajipeg2 != null) {
            foreach ($gajipeg as $i => $tes) {
                $tun[$i] = explode(',', $tes->id_tunjangan);
                foreach ($tun[$i] as $index => $row) {
                    $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                    $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                    $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                    $val[$i] = $jenis[$i];
                    $nom[$i] = $nominalALL[$i];
                    $totaltun[$i] = array_sum($nominalALL[$i]);
                }
            }
            foreach ($gajipeg as $j => $tes2) {
                $bon[$j] = explode(',', $tes2->id_bonus);
                foreach ($bon[$j] as $nus => $coba) {
                    $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                    $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                    $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                    $valBon[$j] = $jenisBon[$j];
                    $nomBon[$j] = $nominalBon[$j];
                    $totalbon[$j] = array_sum($nominalBon[$j]);
                }
            }
            foreach ($gajipeg as $x => $tes3) {
                $pot[$x] = explode(',', $tes3->id_potongan);
                foreach ($pot[$x] as $tongan => $coba2) {
                    $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                    $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                    $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                    $valPot[$x] = $jenisPot[$x];
                    $nomPot[$x] = $nominalPot[$x];
                    $totalpot[$x] = array_sum($nominalPot[$x]);
                }
            }
            foreach ($gajipeg as $a => $jab) {
                $jabat[$a] = explode(',', $jab->id_jabatan);

                foreach ($jabat[$a] as $batan => $tan) {
                    $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                    $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                    $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                    $valjab[$a] = $jenisjab[$a];
                    $nomjab[$a] = array_sum($jabgaji[$a]);
                }
            }
            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potongabsen + $lembur) as $key) {
                $akhir[$key] = array($totaltun[$key] + $lembur[$key] + $nomjab[$key] + $totalbon[$key] - $totalpot[$key] - $potongabsen[$key]);
            }

            return response()->json([
                'data' => $gajipeg,
                'tunjangan' => $val,
                'nominal' => $nom,
                'jabatan' => $valjab,
                'gaji' => $nomjab,
                'total_tunjangan' => $totaltun,
                'bonus' => $valBon,
                'nominal_bonus' => $nomBon,
                'total_bonus' => $totalbon,
                'lembur' => $lembur,
                'potongabsen' => $potongabsen,
                'potongan' => $valPot,
                'nominal_potongan' => $nomPot,
                'total_potongan' => $totalpot,
                'hasil' => $akhir,
                // 'subtotal' => $subtotal,
                'message' => 'get data berhasil',
                'status' => true
            ]);
        }
    }
    public function gajipegawaimobile()
    {
        $gajipeg = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->get();
        $potongabsen = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->pluck('potong_absen')->toArray();
        $lembur = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->pluck('total_lembur')->toArray();
        $gajipeg2 = Penggajian::where('email', Auth::user()->email)->where('status', '=', 'Belum Diambil')->get()->toArray();
        if ($gajipeg2 != null) {
            foreach ($gajipeg as $i => $tes) {
                $tun[$i] = explode(',', $tes->id_tunjangan);
                foreach ($tun[$i] as $index => $row) {
                    $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                    $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                    $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                    $val[$i] = $jenis[$i];
                    $nom[$i] = $nominalALL[$i];
                    $totaltun[$i] = array_sum($nominalALL[$i]);
                }
            }
            foreach ($gajipeg as $j => $tes2) {
                $bon[$j] = explode(',', $tes2->id_bonus);
                foreach ($bon[$j] as $nus => $coba) {
                    $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                    $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                    $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                    $valBon[$j] = $jenisBon[$j];
                    $nomBon[$j] = $nominalBon[$j];
                    $totalbon[$j] = array_sum($nominalBon[$j]);
                }
            }
            foreach ($gajipeg as $x => $tes3) {
                $pot[$x] = explode(',', $tes3->id_potongan);
                foreach ($pot[$x] as $tongan => $coba2) {
                    $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                    $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                    $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                    $valPot[$x] = $jenisPot[$x];
                    $nomPot[$x] = $nominalPot[$x];
                    $totalpot[$x] = array_sum($nominalPot[$x]);
                }
            }
            foreach ($gajipeg as $a => $jab) {
                $jabat[$a] = explode(',', $jab->id_jabatan);

                foreach ($jabat[$a] as $batan => $tan) {
                    $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                    $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                    $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                    $valjab[$a] = $jenisjab[$a];
                    $nomjab[$a] = array_sum($jabgaji[$a]);
                }
            }
            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potongabsen + $lembur) as $key) {
                $akhir[$key] = $totaltun[$key] + $nomjab[$key] + $lembur[$key] + $totalbon[$key] - $totalpot[$key] - $potongabsen[$key];
            }

            return response()->json([
                'data' => $gajipeg,
                'tunjangan' => $val,
                'nominal' => $nom,
                'jabatan' => $valjab,
                'gaji' => $nomjab,
                'total_tunjangan' => $totaltun,
                'bonus' => $valBon,
                'nominal_bonus' => $nomBon,
                'total_bonus' => $totalbon,
                'potongan' => $valPot,
                'nominal_potongan' => $nomPot,
                'total_potongan' => $totalpot,
                'hasil' => $akhir,
                // 'subtotal' => $subtotal,
                'message' => 'get data berhasil',
                'status' => true
            ]);
        } else {
            return response()->json([
                'data' => null,
                'message' => 'get data berhasil',
                'status' => false
            ]);
        };
    }
    public function detgajipeg($id)
    {
        $detgaji = DB::table('penggajian')
            ->where('id', $id)
            ->get()->toArray();
        $potongabsen = DB::table('penggajian')
            ->where('id', $id)
            ->pluck('potong_absen')->toArray();
        $lembur = DB::table('penggajian')
            ->where('id', $id)
            ->pluck('total_lembur')->toArray();
        foreach ($detgaji as $i => $tes) {
            $tun[$i] = explode(',', $tes->id_tunjangan);
            foreach ($tun[$i] as $index => $row) {
                $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                $val[$i] = $jenis[$i];
                $nom[$i] = $nominalALL[$i];
                $totaltun[$i] = array_sum($nominalALL[$i]);
                $arrtun[$i] = array($totaltun[$i]);
            }
        }
        foreach ($detgaji as $j => $tes2) {
            $bon[$j] = explode(',', $tes2->id_bonus);
            foreach ($bon[$j] as $nus => $coba) {
                $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                $valBon[$j] = $jenisBon[$j];
                $nomBon[$j] = $nominalBon[$j];
                $totalbon[$j] = array_sum($nominalBon[$j]);
                $arrbon[$j] = array($totalbon[$j]);
            }
        }
        foreach ($detgaji as $x => $tes3) {
            $pot[$x] = explode(',', $tes3->id_potongan);
            foreach ($pot[$x] as $tongan => $coba2) {
                $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                $valPot[$x] = $jenisPot[$x];
                $nomPot[$x] = $nominalPot[$x];
                $totalpot[$x] = array_sum($nominalPot[$x]);
                $arrpot[$x] = array($totalpot[$x]);
            }
        }
        foreach ($detgaji as $a => $jab) {
            $jabat[$a] = explode(',', $jab->id_jabatan);

            foreach ($jabat[$a] as $batan => $tan) {
                $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                $valjab[$a] = $jenisjab[$a];
                $nomjab[$a] = array_sum($jabgaji[$a]);
                $arrjab[$a] = array($nomjab[$a]);
            }
        }
        foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potongabsen + $lembur) as $key) {
            $akhir[$key] = array($totaltun[$key] + $lembur[$key] + $nomjab[$key] + $totalbon[$key] - $totalpot[$key] - $potongabsen[$key]);
        }
        foreach (array_keys($totaltun + $totalbon + $nomjab + $potongabsen + $lembur) as $key) {
            $subtotal[$key] = array($totaltun[$key] + $lembur[$key] + $nomjab[$key] + $totalbon[$key] - $potongabsen[$key]);
        }


        return response()->json([
            'data' => $detgaji,
            'tunjangan' => $val,
            'arrtun' => $arrtun,
            'arrbon' => $arrbon,
            'arrpot' => $arrpot,
            'arrjab' => $arrjab,
            'nominal' => $nom,
            'jabatan' => $valjab,
            'gaji' => $nomjab,
            'total_tunjangan' => $totaltun,
            'bonus' => $valBon,
            'nominal_bonus' => $nomBon,
            'total_bonus' => $totalbon,
            'lembur' => $lembur,
            'potongabsen' => $potongabsen,
            'potongan' => $valPot,
            'nominal_potongan' => $nomPot,
            'total_potongan' => $totalpot,
            'hasil' => $akhir,
            'subtotal' => $subtotal,
            'message' => 'get data berhasil',
            'status' => true
        ]);
    }
    public function detgajipegmobile($id)
    {
        $detgaji = DB::table('penggajian')
            ->where('id', $id)
            ->first();
        $potongabsen = DB::table('penggajian')
            ->where('id', $id)
            ->pluck('potong_absen')
            ->first();
        $lembur = DB::table('penggajian')
            ->where('id', $id)
            ->pluck('total_lembur')
            ->first();

        $tun = explode(',', $detgaji->id_tunjangan);
        foreach ($tun as $index => $row) {
            $data[$index] = DB::table('tunjangan')->where('id', $row)->first();
            $nominalALL[$index] = $data[$index]->nominal;
            $jenis[$index] = $data[$index]->jenis_tunjangan;
            $val = $jenis;
            $nom = $nominalALL;
            $totaltun = array_sum($nominalALL);
            $arrtun = array($totaltun);
        }
        $bon = explode(',', $detgaji->id_bonus);
        foreach ($bon as $nus => $coba) {
            $databon[$nus] = DB::table('bonus')->where('id', $coba)->first();
            $nominalBon[$nus] = $databon[$nus]->nominal;
            $jenisBon[$nus] = $databon[$nus]->jenis_bonus;
            $valBon = $jenisBon;
            $nomBon = $nominalBon;
            $totalbon = array_sum($nominalBon);
            $arrbon = array($totalbon);
        }
        $pot = explode(',', $detgaji->id_potongan);
        foreach ($pot as $tongan => $coba2) {
            $datapot[$tongan] = DB::table('potongan')->where('id', $coba2)->first();
            $nominalPot[$tongan] = $datapot[$tongan]->nominal;
            $jenisPot[$tongan] = $datapot[$tongan]->jenis_potongan;
            $valPot = $jenisPot;
            $nomPot = $nominalPot;
            $totalpot = array_sum($nominalPot);
            $arrpot = array($totalpot);
        }
        $jabat = explode(',', $detgaji->id_jabatan);

        foreach ($jabat as $batan => $tan) {
            $datajab[$batan] = DB::table('jabatan')->where('id', $tan)->first();
            $jabgaji[$batan] = $datajab[$batan]->gaji;
            $jenisjab[$batan] = $datajab[$batan]->jabatan;
            $valjab = $jenisjab;
            $nomjab = array_sum($jabgaji);
            $arrjab = array($nomjab);
        }
        $akhir = $totaltun + $lembur + $nomjab + $totalbon - $totalpot - $potongabsen;
        $subtotal = $totaltun + $lembur + $nomjab + $totalbon - $potongabsen;



        return response()->json([
            'data' => $detgaji,
            'tunjangan' => $val,
            'arrtun' => $arrtun,
            'arrbon' => $arrbon,
            'arrpot' => $arrpot,
            'arrjab' => $arrjab,
            'potong_absen' => $potongabsen,
            'nominal' => $nom,
            'jabatan' => $valjab,
            'gaji' => $nomjab,
            'total_tunjangan' => $totaltun,
            'bonus' => $valBon,
            'nominal_bonus' => $nomBon,
            'total_bonus' => $totalbon,
            'potongan' => $valPot,
            'nominal_potongan' => $nomPot,
            'total_potongan' => $totalpot,
            'hasil' => $akhir,
            'subtotal' => $subtotal,
            'message' => 'get data berhasil',
            'status' => true
        ]);
    }

    public function ambilgaji(Request $request)
    {
        $confirm = DB::table('penggajian')->where('id', $request->id)->update([
            'status' => $request->status,
        ]);
        if ($confirm = 'Belum Cair') {
            $detgaji = DB::table('penggajian')
                ->where('id', $request->id)
                ->get()->toArray();
            $potong = DB::table('penggajian')
                ->where('id', $request->id)
                ->pluck('potong_absen')
                ->toArray();
            $lembur = DB::table('penggajian')
                ->where('id', $request->id)
                ->pluck('total_lembur')
                ->toArray();
            foreach ($detgaji as $i => $tes) {
                $tun[$i] = explode(',', $tes->id_tunjangan);
                foreach ($tun[$i] as $index => $row) {
                    $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                    $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                    $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                    $val[$i] = $jenis[$i];
                    $nom[$i] = $nominalALL[$i];
                    $totaltun[$i] = array_sum($nominalALL[$i]);
                    $arrtun[$i] = array($totaltun[$i]);
                }
            }
            foreach ($detgaji as $j => $tes2) {
                $bon[$j] = explode(',', $tes2->id_bonus);
                foreach ($bon[$j] as $nus => $coba) {
                    $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                    $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                    $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                    $valBon[$j] = $jenisBon[$j];
                    $nomBon[$j] = $nominalBon[$j];
                    $totalbon[$j] = array_sum($nominalBon[$j]);
                    $arrbon[$j] = array($totalbon[$j]);
                }
            }
            foreach ($detgaji as $x => $tes3) {
                $pot[$x] = explode(',', $tes3->id_potongan);
                foreach ($pot[$x] as $tongan => $coba2) {
                    $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                    $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                    $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                    $valPot[$x] = $jenisPot[$x];
                    $nomPot[$x] = $nominalPot[$x];
                    $totalpot[$x] = array_sum($nominalPot[$x]);
                    $arrpot[$x] = array($totalpot[$x]);
                }
            }
            foreach ($detgaji as $a => $jab) {
                $jabat[$a] = explode(',', $jab->id_jabatan);

                foreach ($jabat[$a] as $batan => $tan) {
                    $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                    $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                    $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                    $valjab[$a] = $jenisjab[$a];
                    $nomjab[$a] = array_sum($jabgaji[$a]);
                    $arrjab[$a] = array($nomjab[$a]);
                }
            }

            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potong + $lembur) as $key) {
                $akhir[$key] = array($totaltun[$key] + $nomjab[$key] + $totalbon[$key] + $lembur[$key] - $totalpot[$key] - $potong[$key]);
            }
            foreach (array_keys($totaltun + $totalbon + $nomjab + $potong + $lembur) as $key) {
                $subtotal[$key] = array($totaltun[$key] + $nomjab[$key] + $lembur[$key] + $totalbon[$key] - $potong[$key]);
            }
            $riwayat = RiwayatGaji::where('id', $request->id)->update([
                'id_admin' => Auth::user()->id_admin,
                'email' => Auth::user()->email,
                'tanggal_ambil' => Carbon::now(),
                'id_jabatan' => Auth::user()->id_jabatan,
                'id_golongan' => Auth::user()->id_golongan,
                'tunjangan' => implode(",", $val[$i]),
                'nominal_tunjangan' => implode(",", $nom[$i]),
                'total_tunjangan' => implode(",", $arrtun[$i]),
                'bonus' => implode(",", $valBon[$j]),
                'nominal_bonus' => implode(",", $nomBon[$j]),
                'total_bonus' => implode(",", $arrbon[$j]),
                'potongan' => implode(",", $valPot[$x]),
                'nominal_potongan' => implode(",", $nomPot[$x]),
                'total_potongan' => implode(",", $arrpot[$x]),
                'gaji_kotor' => implode(",", $subtotal[$key]),
                'gaji_bersih' => implode(",", $akhir[$key]),
                'gaji_pokok' => implode(",", $arrjab[$a]),
                'status' => 'Belum Cair'


            ]);
            $timezone = 'Asia/Jakarta';
            $date = new DateTime('now', new DateTimeZone($timezone));
            $tanggal = $date->format('Y-m-d');
            $localtime = $date->format('H:i:s');
            Pemberitahuan::create([
                'id_admin' => Auth::user()->id_admin,
                'email' => Auth::user()->email,
                'judul' => 'Pengajuan Pengambilan Gaji ',
                'jenis' => 'Penggajin',
                'status' => 'Menunggu Cair',
                'tanggal' => $tanggal,
                'jam' => $localtime
            ]);
            return response()->json([
                'data' => $confirm,
                'riwayat' => $riwayat,
                'success' => true,
                'message' => 'Berhasil Ambil!',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Gagal lah
                    ',
            ]);
        }
    }
    public function ambilgajimobile(Request $request, $id)
    {
        $confirm = DB::table('penggajian')->where('id', $id)->update([
            'status' => "Belum Cair",
        ]);
        if ($confirm = 'Belum Cair') {
            $detgaji = DB::table('penggajian')
                ->where('id', $id)
                ->get()->toArray();
            $potong = DB::table('penggajian')
                ->where('id', $id)
                ->pluck('potong_absen')
                ->toArray();
            $lembur = DB::table('penggajian')
                ->where('id', $id)
                ->pluck('total_lembur')
                ->toArray();
            foreach ($detgaji as $i => $tes) {
                $tun[$i] = explode(',', $tes->id_tunjangan);
                foreach ($tun[$i] as $index => $row) {
                    $data[$i][$index] = DB::table('tunjangan')->where('id', $row)->first();
                    $nominalALL[$i][$index] = $data[$i][$index]->nominal;
                    $jenis[$i][$index] = $data[$i][$index]->jenis_tunjangan;
                    $val[$i] = $jenis[$i];
                    $nom[$i] = $nominalALL[$i];
                    $totaltun[$i] = array_sum($nominalALL[$i]);
                    $arrtun[$i] = array($totaltun[$i]);
                }
            }
            foreach ($detgaji as $j => $tes2) {
                $bon[$j] = explode(',', $tes2->id_bonus);
                foreach ($bon[$j] as $nus => $coba) {
                    $databon[$j][$nus] = DB::table('bonus')->where('id', $coba)->first();
                    $nominalBon[$j][$nus] = $databon[$j][$nus]->nominal;
                    $jenisBon[$j][$nus] = $databon[$j][$nus]->jenis_bonus;
                    $valBon[$j] = $jenisBon[$j];
                    $nomBon[$j] = $nominalBon[$j];
                    $totalbon[$j] = array_sum($nominalBon[$j]);
                    $arrbon[$j] = array($totalbon[$j]);
                }
            }
            foreach ($detgaji as $x => $tes3) {
                $pot[$x] = explode(',', $tes3->id_potongan);
                foreach ($pot[$x] as $tongan => $coba2) {
                    $datapot[$x][$tongan] = DB::table('potongan')->where('id', $coba2)->first();
                    $nominalPot[$x][$tongan] = $datapot[$x][$tongan]->nominal;
                    $jenisPot[$x][$tongan] = $datapot[$x][$tongan]->jenis_potongan;
                    $valPot[$x] = $jenisPot[$x];
                    $nomPot[$x] = $nominalPot[$x];
                    $totalpot[$x] = array_sum($nominalPot[$x]);
                    $arrpot[$x] = array($totalpot[$x]);
                }
            }
            foreach ($detgaji as $a => $jab) {
                $jabat[$a] = explode(',', $jab->id_jabatan);

                foreach ($jabat[$a] as $batan => $tan) {
                    $datajab[$a][$batan] = DB::table('jabatan')->where('id', $tan)->first();
                    $jabgaji[$a][$batan] = $datajab[$a][$batan]->gaji;
                    $jenisjab[$a][$batan] = $datajab[$a][$batan]->jabatan;
                    $valjab[$a] = $jenisjab[$a];
                    $nomjab[$a] = array_sum($jabgaji[$a]);
                    $arrjab[$a] = array($nomjab[$a]);
                }
            }

            foreach (array_keys($totaltun + $totalbon + $totalpot + $nomjab + $potong + $lembur) as $key) {
                $akhir[$key] = array($totaltun[$key] + $nomjab[$key] + $totalbon[$key] + $lembur[$key] - $totalpot[$key] - $potong[$key]);
            }
            foreach (array_keys($totaltun + $totalbon + $nomjab + $potong + $lembur) as $key) {
                $subtotal[$key] = array($totaltun[$key] + $nomjab[$key] + $lembur[$key] + $totalbon[$key] - $potong[$key]);
            }
            $riwayat = RiwayatGaji::where('id', $request->id)->update([
                'id_admin' => Auth::user()->id_admin,
                'email' => Auth::user()->email,
                'tanggal_ambil' => Carbon::now(),
                'id_jabatan' => Auth::user()->id_jabatan,
                'id_golongan' => Auth::user()->id_golongan,
                'tunjangan' => implode(",", $val[$i]),
                'nominal_tunjangan' => implode(",", $nom[$i]),
                'total_tunjangan' => implode(",", $arrtun[$i]),
                'bonus' => implode(",", $valBon[$j]),
                'nominal_bonus' => implode(",", $nomBon[$j]),
                'total_bonus' => implode(",", $arrbon[$j]),
                'potongan' => implode(",", $valPot[$x]),
                'nominal_potongan' => implode(",", $nomPot[$x]),
                'total_potongan' => implode(",", $arrpot[$x]),
                'gaji_kotor' => implode(",", $subtotal[$key]),
                'gaji_bersih' => implode(",", $akhir[$key]),
                'gaji_pokok' => implode(",", $arrjab[$a]),
                'status' => 'Belum Cair'


            ]);
            $timezone = 'Asia/Jakarta';
            $date = new DateTime('now', new DateTimeZone($timezone));
            $tanggal = $date->format('Y-m-d');
            $localtime = $date->format('H:i:s');
            Pemberitahuan::create([
                'id_admin' => Auth::user()->id_admin,
                'email' => Auth::user()->email,
                'judul' => 'Pengajuan Pengambilan Gaji ',
                'jenis' => 'Penggajin',
                'status' => 'Menunggu Cair',
                'tanggal' => $tanggal,
                'jam' => $localtime
            ]);
            return response()->json([
                'data' => $confirm,
                'riwayat' => $riwayat,
                'success' => true,
                'message' => 'Berhasil Ambil!',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Gagal lah
                    ',
            ]);
        }
    }
    public function detriwayatgaji($id)
    {
        $detgaji = DB::table('riwayatgaji')
            ->where('id', $id)
            ->get()->toArray();
        foreach ($detgaji as $i => $tes) {
            $tun[$i] = explode(',', $tes->tunjangan);
            $nomtun[$i] = explode(',', $tes->nominal_tunjangan);
            $bon[$i] = explode(',', $tes->bonus);
            $nombon[$i] = explode(',', $tes->nominal_bonus);
            $pot[$i] = explode(',', $tes->potongan);
            $nompot[$i] = explode(',', $tes->nominal_potongan);
        }
        return response()->json([
            'data' => $detgaji,
            'tunjangan' => $tun,
            'nomtun' => $nomtun,
            'bonus' => $bon,
            'nombon' => $nombon,
            'potongan' => $pot,
            'nompot' => $nompot,
            'message' => 'get data berhasil',
            'status' => true
        ]);
    }
    public function detriwayatmobile($id)
    {
        $detgaji = DB::table('riwayatgaji')
            ->where('id', $id)
            ->first();
        return response()->json([
            'data' => $detgaji,
            'message' => 'get data berhasil',
            'status' => true
        ]);
    }
    public function hapusgaji(Request $request, $id)
    {
        $data = Penggajian::findOrFail($id);
        $data->delete();
        DB::table('riwayatgaji')->where('id_admin', Auth::user()->id)->where('no_transaksi', $data->no_transaksi)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Hapus data berhasil'
        ]);
    }
}
