<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PotongAbsen extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "potongabsen";
    protected $fillable = [
        'email_admin',
        'potongan',
    ];
    protected $dates = ['deleted_at'];
}
