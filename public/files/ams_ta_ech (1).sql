-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 24, 2023 at 07:40 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams_ta_ech`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensipegawai`
--

CREATE TABLE `absensipegawai` (
  `uid` int(10) UNSIGNED NOT NULL,
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selfie_masuk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selfie_pulang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_pulang` time DEFAULT NULL,
  `jam_kerja` time DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latmasuk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lonmasuk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latpulang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lonpulang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `absensipegawai`
--

INSERT INTO `absensipegawai` (`uid`, `id`, `id_admin`, `name`, `nama_lengkap`, `email`, `selfie_masuk`, `selfie_pulang`, `tanggal`, `jam_masuk`, `jam_pulang`, `jam_kerja`, `keterangan`, `latmasuk`, `lonmasuk`, `latpulang`, `lonpulang`, `created_at`, `updated_at`) VALUES
(104, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-23', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-22 17:00:00', NULL),
(105, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', NULL, NULL, '2022-12-23', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-22 17:00:00', NULL),
(106, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', '63a5c41bac93f.png', '63a5c424a5e33.png', '2022-12-23', '22:07:07', '22:07:16', '00:00:09', 'Terlambat', 'undefined', 'undefined', 'undefined', 'undefined', '2022-12-22 17:00:00', '2022-12-23 08:07:07'),
(110, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2022-12-27', NULL, NULL, NULL, 'Cuti', NULL, NULL, NULL, NULL, '2022-12-26 17:00:00', NULL),
(111, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-27', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-26 17:00:00', NULL),
(112, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', NULL, NULL, '2022-12-27', NULL, NULL, NULL, 'Request Absen', NULL, NULL, NULL, NULL, '2022-12-26 17:00:00', NULL),
(139, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-28', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-27 17:00:00', NULL),
(140, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63ac06130764f.png', '63ac386d9b577.png', '2022-12-28', '16:02:11', '19:37:01', '03:34:50', 'On Time', '-7.7259062', '111.5349059', '-7.7259062', '111.5349059', '2022-12-27 17:00:00', '2022-12-28 02:02:11'),
(141, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2022-12-28', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-27 17:00:00', NULL),
(152, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', NULL, NULL, '2022-12-29', NULL, NULL, NULL, 'Cuti', NULL, NULL, NULL, NULL, '2022-12-28 17:00:00', NULL),
(153, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-29', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-28 17:00:00', NULL),
(154, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2022-12-29', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-28 17:00:00', NULL),
(155, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', NULL, NULL, '2022-12-30', NULL, NULL, NULL, 'Izin', NULL, NULL, NULL, NULL, '2022-12-29 17:00:00', NULL),
(156, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-30', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-29 17:00:00', NULL),
(157, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2022-12-30', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-29 17:00:00', NULL),
(158, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', NULL, NULL, '2022-12-31', NULL, NULL, NULL, 'Cuti', NULL, NULL, NULL, NULL, '2022-12-30 17:00:00', NULL),
(159, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2022-12-31', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-30 17:00:00', NULL),
(160, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2022-12-31', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2022-12-30 17:00:00', NULL),
(161, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2023-01-02', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-01 17:00:00', NULL),
(162, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63b238c685ff5.png', NULL, '2023-01-02', '08:52:06', NULL, NULL, 'Izin', '-7.5639466', '111.5110459', NULL, NULL, '2023-01-01 17:00:00', '2023-01-01 18:52:06'),
(163, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-02', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-01 17:00:00', NULL),
(164, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2023-01-03', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-02 17:00:00', NULL),
(165, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63b42ce68c281.png', '63b42cee3f0b0.png', '2023-01-03', '20:25:58', '20:26:06', '00:00:08', 'Terlambat', '-7.5639466', '111.5110459', '-7.5639466', '111.5110459', '2023-01-02 17:00:00', '2023-01-03 06:25:58'),
(166, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-03', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-02 17:00:00', NULL),
(179, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2023-01-04', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-03 17:00:00', NULL),
(180, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63b5307620e5a.png', NULL, '2023-01-04', '14:53:26', NULL, NULL, 'On Time', '-7.5639466', '111.5110459', NULL, NULL, '2023-01-03 17:00:00', '2023-01-04 00:53:26'),
(181, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-04', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-03 17:00:00', NULL),
(182, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2023-01-09', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-08 17:00:00', NULL),
(183, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63bb74ce96a92.png', NULL, '2023-01-09', '08:58:38', NULL, NULL, 'On Time', '-7.5639466', '111.5110459', NULL, NULL, '2023-01-08 17:00:00', '2023-01-08 18:58:38'),
(184, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-09', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-08 17:00:00', NULL),
(185, 2, 2, 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, '2023-01-16', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-15 17:00:00', NULL),
(186, 1, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '63c4e4808cdec.png', NULL, '2023-02-16', '12:45:36', NULL, NULL, 'On Time', '-7.6169674', '111.2725789', NULL, NULL, '2023-01-15 17:00:00', '2023-01-15 22:45:36'),
(187, 3, 2, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-16', NULL, NULL, NULL, 'Tidak Hadir', NULL, NULL, NULL, NULL, '2023-01-15 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `akunpegawai`
--

CREATE TABLE `akunpegawai` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` int(10) UNSIGNED NOT NULL,
  `id_golongan` int(10) UNSIGNED DEFAULT NULL,
  `role` enum('Manager','Admin','Pegawai') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pegawai',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_kerja` int(11) DEFAULT NULL,
  `tanggal_masuk` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aktif',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `akunpegawai`
--

INSERT INTO `akunpegawai` (`id`, `id_admin`, `name`, `id_jabatan`, `id_golongan`, `role`, `email`, `jumlah_kerja`, `tanggal_masuk`, `status`, `password`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`, `last_login`) VALUES
(1, 2, 'Fihan', 2, NULL, 'Pegawai', 'fihan2as@gmail.com', 629, '2021-05-02', 'Aktif', '$2y$10$dyb3strgX5FlK1.Kt3.OKurBcQvVp9FCBABdz/tEzT9wKhtQvn4RG', NULL, NULL, NULL, NULL, '2023-01-24 00:55:27'),
(2, 2, 'Muhammad Fihan Ashidiq', 4, NULL, 'Pegawai', 'fihan2222as@gmail.com', 459, '2022-11-06', 'Aktif', 'Password158329047', NULL, NULL, NULL, NULL, NULL),
(3, 2, 'Tes', 4, NULL, 'Pegawai', 'fihan2as@student.ub.ac.id', 494, '2022-01-18', 'Aktif', '$2y$10$dFsmP9GokaZPJXWPn2w1geUeFKuFDIVRl.WSA7zI2ERJt8GpRVAmK', NULL, NULL, NULL, NULL, '2023-01-08 21:33:58'),
(4, 2, 'g', 6, NULL, 'Pegawai', 'a@gmail.com', 1065, '2020-01-20', 'Aktif', 'Password029456318', NULL, NULL, NULL, NULL, NULL),
(5, 2, 'bhbh', 4, NULL, 'Pegawai', 'b@gmail.com', 791, '2020-10-20', 'Aktif', 'Password167240953', NULL, NULL, NULL, NULL, NULL),
(6, 2, 'MNS', 12, NULL, 'Pegawai', 'faasd@gmail.com', 8, '2023-01-01', 'Aktif', 'Password264803579', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_bonus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `email_admin`, `jenis_bonus`, `nominal`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'admin@gmail.com', 'Tes', 67000000, '2023-01-10 06:12:10', '2023-01-09 23:10:07', '2023-01-09 23:12:10'),
(3, 'admin@gmail.com', 'Bonus Makan', 2999999, '2023-01-10 06:19:25', '2023-01-09 23:12:46', '2023-01-09 23:19:25'),
(4, 'admin@gmail.com', '-', 0, NULL, '2023-01-09 23:14:41', '2023-01-09 23:14:41'),
(5, 'admin@gmail.com', 'Bonus', 300000, NULL, '2023-01-10 00:18:33', '2023-01-10 00:18:33'),
(6, 'admin@gmail.com', 'Tes Bonus', 200000, NULL, '2023-01-10 01:46:20', '2023-01-10 01:46:20'),
(7, 'admin2@gmail.com', 'Tidak Ada Bonus', 0, NULL, '2023-01-15 01:23:48', '2023-01-15 01:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_hari` int(11) NOT NULL,
  `tanggal_cuti` date NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `list_tanggal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_cuti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_cuti` enum('Diterima','Ditolak','Diproses') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Diproses',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cuti`
--

INSERT INTO `cuti` (`id`, `id_pegawai`, `name`, `id_admin`, `email`, `no_pegawai`, `nama_lengkap`, `jumlah_hari`, `tanggal_cuti`, `tanggal_mulai`, `tanggal_akhir`, `list_tanggal`, `jenis_cuti`, `keterangan`, `bukti_cuti`, `status_cuti`, `created_at`, `updated_at`) VALUES
(7, 1, 'Fihan', 2, 'fihan2as@gmail.com', '2578126312', 'Muhammad Fihan Ashidiq', 2, '2022-12-28', '2022-12-25', '2022-12-27', '[\"2022-12-25\",\"2022-12-26\",\"2022-12-27\"]', 'x', 's', 'bg-gradient.svg', 'Ditolak', '2022-12-27 18:41:56', '2022-12-27 18:41:56'),
(9, 1, 'Fihan', 2, 'fihan2as@gmail.com', '2578126312', 'Muhammad Fihan Ashidiq', 4, '2022-12-28', '2022-12-27', '2022-12-31', '[\"2022-12-27\",\"2022-12-28\",\"2022-12-29\",\"2022-12-30\",\"2022-12-31\"]', 'hjg', 'jhg', 'bg-gradient.svg', 'Ditolak', '2022-12-27 23:02:18', '2022-12-27 23:02:18'),
(10, 1, 'Fihan', 2, 'fihan2as@gmail.com', '2578126312', 'Muhammad Fihan Ashidiq', 2, '2022-12-28', '2022-12-29', '2022-12-31', '[\"2022-12-29\",\"2022-12-30\",\"2022-12-31\"]', 'a', 'a', 'menu.png', 'Diterima', '2022-12-28 00:19:27', '2022-12-28 00:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `cuti_perusahaans`
--

CREATE TABLE `cuti_perusahaans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cuti_tahunans`
--

CREATE TABLE `cuti_tahunans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `datagaji`
--

CREATE TABLE `datagaji` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` int(10) UNSIGNED NOT NULL,
  `id_golongan` int(10) UNSIGNED DEFAULT NULL,
  `id_tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_tunjangan` int(11) DEFAULT NULL,
  `total_bonus` int(11) DEFAULT NULL,
  `total_potongan` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam_kerja` time DEFAULT NULL,
  `status` enum('Aktif','Tidak Aktif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aktif',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `datagaji`
--

INSERT INTO `datagaji` (`id`, `id_admin`, `no_pegawai`, `name`, `nama_lengkap`, `email`, `id_jabatan`, `id_golongan`, `id_tunjangan`, `id_bonus`, `id_potongan`, `total_tunjangan`, `total_bonus`, `total_potongan`, `tanggal`, `jam_kerja`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '2578126312', 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '19:50:10', 'Aktif', '2022-11-04 20:55:26', '2022-11-04 20:55:26'),
(3, 2, 'JH123123', 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:13', 'Aktif', '2022-12-19 23:04:33', '2022-12-19 23:04:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `golongan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hari` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id`, `hari`, `created_at`, `updated_at`) VALUES
(1, 'Minggu', NULL, NULL),
(2, 'Senin', NULL, NULL),
(3, 'Selasa', NULL, NULL),
(4, 'Rabu', NULL, NULL),
(5, 'Kamis', NULL, NULL),
(6, 'Jumat', NULL, NULL),
(7, 'Sabtu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `harikerja`
--

CREATE TABLE `harikerja` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `hari_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_masuk` time NOT NULL DEFAULT '00:00:00',
  `jam_pulang` time NOT NULL DEFAULT '00:00:00',
  `buka_presensi` time NOT NULL DEFAULT '00:00:00',
  `tutup_presensi` time NOT NULL DEFAULT '00:00:00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `harikerja`
--

INSERT INTO `harikerja` (`id`, `id_admin`, `hari_kerja`, `jam_masuk`, `jam_pulang`, `buka_presensi`, `tutup_presensi`, `created_at`, `updated_at`) VALUES
(1, 2, 'Minggu,Senin,Selasa,Kamis,Rabu,Sabtu,Jumat', '18:00:00', '19:00:00', '04:25:00', '23:00:00', '2022-11-05 02:37:39', '2023-01-19 08:16:37'),
(2, 6, 'Minggu,Senin,Selasa,Rabu,Kamis', '07:00:00', '15:10:00', '06:00:00', '20:10:00', '2022-12-20 17:09:12', '2022-12-20 17:09:12');

-- --------------------------------------------------------

--
-- Table structure for table `hitung_gaji`
--

CREATE TABLE `hitung_gaji` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `no_gaji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal_tunjangan` int(11) DEFAULT NULL,
  `nominal_bonus` int(11) DEFAULT NULL,
  `nominal_potongan` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `izin`
--

CREATE TABLE `izin` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_izin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_izin` enum('Diproses','Diterima','Ditolak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Diproses',
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `izin`
--

INSERT INTO `izin` (`id`, `id_admin`, `id_pegawai`, `name`, `nama_lengkap`, `email`, `no_pegawai`, `jenis_izin`, `bukti`, `status_izin`, `tanggal`, `created_at`, `updated_at`) VALUES
(4, 2, 1, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'ABC', 'menu.png', 'Diproses', '2022-12-28', '2022-12-28 00:22:32', '2022-12-28 00:22:32'),
(5, 2, 1, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'h', 'blurry-gradient.png', 'Ditolak', '2022-12-22', '2022-12-28 01:14:48', '2022-12-28 01:14:48'),
(6, 2, 3, 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', 'JH123123', 'Get Izin', 'bg-gradient.svg', 'Ditolak', '2022-12-29', '2022-12-28 17:38:04', '2022-12-28 17:38:04'),
(7, 2, 1, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'mm', 'bg-gradient.svg', 'Diterima', '2022-12-30', '2022-12-28 22:25:47', '2022-12-28 22:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gaji` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `id_admin`, `jabatan`, `gaji`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'Manager', 8000000, '2022-11-05 03:55:59', NULL, '2022-11-04 20:55:59'),
(2, 2, 'Admin HRD', 3000000, NULL, NULL, NULL),
(3, 2, 'HRD', 5000000, '2022-11-05 03:59:46', NULL, '2022-11-04 20:59:46'),
(4, 2, 'Staff HRD', 4500000, NULL, NULL, NULL),
(5, 2, 'Programmer', 10000000, '2022-11-05 09:40:23', NULL, '2022-11-05 02:40:23'),
(6, 2, 'Manager', 10000000, NULL, '2022-12-19 09:09:42', '2022-12-19 09:09:42'),
(7, 2, 'b', 7, '2023-01-10 06:09:23', '2022-12-21 19:24:10', '2023-01-09 23:09:23'),
(8, 2, 'd', 2, NULL, '2022-12-21 19:24:17', '2022-12-21 19:24:17'),
(9, 2, 's', 2, NULL, '2022-12-21 19:24:21', '2022-12-21 19:24:21'),
(10, 2, 'ad', 3, NULL, '2022-12-21 19:24:29', '2022-12-21 19:24:29'),
(11, 2, 'g', 33, NULL, '2022-12-21 19:24:36', '2022-12-21 19:24:36'),
(12, 2, 'ds', 22, NULL, '2022-12-21 19:24:42', '2022-12-21 19:24:42'),
(13, 2, 'sa', 21, NULL, '2022-12-23 09:25:51', '2022-12-23 09:25:51'),
(14, 2, 'sda', 213, NULL, '2022-12-23 09:25:56', '2022-12-23 09:25:56'),
(15, 2, 'asd', 123, NULL, '2022-12-23 09:26:00', '2022-12-23 09:26:00'),
(16, 2, 'adads', 3213123, NULL, '2022-12-23 09:26:10', '2022-12-23 09:26:10'),
(17, 2, 'sadsa3', 21312, NULL, '2022-12-23 09:26:17', '2022-12-23 09:26:17'),
(18, 2, 'jhj', 8798, NULL, '2022-12-23 09:56:49', '2022-12-23 09:56:49'),
(19, 2, 'nhj', 657, NULL, '2022-12-23 09:56:57', '2022-12-23 09:56:57'),
(20, 2, 'gh', 875, NULL, '2022-12-23 09:57:05', '2022-12-23 09:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `jamabsen`
--

CREATE TABLE `jamabsen` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `jam_masuk` time NOT NULL DEFAULT '00:00:00',
  `jam_pulang` time NOT NULL DEFAULT '00:00:00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `email_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_job` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'In Progress',
  `pengumpulan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deadline` date NOT NULL,
  `terkumpul` date DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revisi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `id_admin`, `email_pegawai`, `judul_job`, `deskripsi`, `status`, `pengumpulan`, `nilai`, `deadline`, `terkumpul`, `keterangan`, `revisi`, `created_at`, `updated_at`) VALUES
(1, 2, 'fihan2as@gmail.com', 'Front End', 'Add Navbar', 'Complete', 'menu.png', NULL, '2023-01-10', '2023-01-03', 'On Time', 'Jangan terlalu banyak gambar', '2023-01-02 21:47:52', '2023-01-02 21:47:52'),
(2, 2, 'fihan2as@gmail.com', 'Tes', 'Hanya Tes ini', 'Complete', 'blurry-gradient.png', NULL, '2023-01-10', '2023-01-03', 'On Time', 'Jangan Terlalu Banyak Gambar Cok', '2023-01-02 22:03:14', '2023-01-02 22:03:14'),
(3, 2, 'fihan2as@gmail.com', 'Jancok', '1. Perbaiki nomer berikut', 'Complete', 'menu.png', NULL, '2023-01-04', '2023-01-03', 'On Time', 'Masih Salah', '2023-01-03 06:13:47', '2023-01-03 06:13:47'),
(4, 2, 'fihan2as@gmail.com', 'Tes', 'Jangan Pernah Menggunakan PHTOSHOP', 'Complete', 'blurry-gradient.png', NULL, '2023-01-04', '2023-01-03', 'On Time', 'Maaf Harus Revisi', '2023-01-03 06:18:36', '2023-01-03 06:20:45'),
(5, 2, 'fihan2as@student.ub.ac.id', 'Back end Developer', 'Tidak ada', 'Complete', 'apps (1).svg', NULL, '2023-01-11', '2023-01-09', 'On Time', NULL, '2023-01-03 22:26:05', '2023-01-03 22:26:05'),
(6, 2, 'fihan2as@gmail.com', 'Front end', 'Tes', 'Revisi', 'apps2.svg', NULL, '2023-01-11', '2023-01-09', 'On Time', 'Benerin yang serius lah anj', '2023-01-08 14:48:36', '2023-01-08 14:48:36'),
(7, 2, 'fihan2as@gmail.com', 'Lanjutan Kemarin', 'Kontol', 'In Progress', NULL, NULL, '2023-01-16', NULL, NULL, NULL, '2023-01-08 18:14:52', '2023-01-08 18:14:52'),
(8, 2, 'fihan2as@gmail.com', 'Tes', 'asd', 'In Progress', NULL, NULL, '2023-01-17', NULL, NULL, NULL, '2023-01-08 18:57:46', '2023-01-08 18:57:46'),
(9, 2, 'fihan2as@gmail.com', 'Lanjutkan tugas kemarin', 'Harus selesai tepat waktu', 'In Progress', NULL, NULL, '2023-01-21', NULL, NULL, NULL, '2023-01-13 19:29:23', '2023-01-13 19:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_laporan` date NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `lampiran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_laporan` enum('Diterima','Ditolak','Diproses') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Diproses',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id`, `id_admin`, `email`, `no_pegawai`, `nama_lengkap`, `tanggal_laporan`, `id_jabatan`, `deskripsi`, `lampiran`, `status_laporan`, `created_at`, `updated_at`) VALUES
(1, 2, 'fihan2as@gmail.com', '2578126312', 'Muhammad Fihan Ashidiq', '2022-12-28', 2, 'www', 'blurry-gradient.png', 'Diterima', '2022-12-28 00:32:46', '2022-12-28 00:32:46');

-- --------------------------------------------------------

--
-- Table structure for table `lembur`
--

CREATE TABLE `lembur` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lembur` date NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_jam` int(11) NOT NULL,
  `upah_lembur` int(11) DEFAULT NULL,
  `aktifitas` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `buktilembur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_lembur` enum('Diterima','Ditolak','Diproses') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Diproses',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lembur`
--

INSERT INTO `lembur` (`id`, `id_admin`, `email`, `tanggal_lembur`, `nama_lengkap`, `no_pegawai`, `jumlah_jam`, `upah_lembur`, `aktifitas`, `buktilembur`, `status_lembur`, `created_at`, `updated_at`) VALUES
(7, 2, 'fihan2as@gmail.com', '2023-01-23', 'Muhammad Fihan Ashidiq', '2578126312', 5, 164740, 'ABCD', 'blurry-gradient.png', 'Diterima', '2023-01-22 16:04:23', '2023-01-22 16:04:23'),
(8, 2, 'fihan2as@gmail.com', '2023-01-23', 'Muhammad Fihan Ashidiq', '2578126312', 3, 95376, 'Tes', 'apps (1).svg', 'Diterima', '2023-01-22 16:10:31', '2023-01-22 16:10:31'),
(9, 2, 'fihan2as@gmail.com', '2023-01-23', 'Muhammad Fihan Ashidiq', '2578126312', 7, NULL, 'A', 'blurry-gradient.png', 'Ditolak', '2023-01-22 16:46:15', '2023-01-22 16:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `lokasipegawai`
--

CREATE TABLE `lokasipegawai` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasipegawai`
--

INSERT INTO `lokasipegawai` (`id`, `id_admin`, `id_pegawai`, `email`, `latitude`, `longitude`, `expired_at`, `created_at`, `updated_at`) VALUES
(2, 2, 1, 'fihan2as@gmail.com', '-7.6169674', '111.2725789', '2023-01-19 15:45:53', '2022-12-18 16:31:33', '2022-12-18 16:31:33'),
(3, 2, 3, 'fihan2as@student.ub.ac.id', NULL, NULL, '2023-01-08 22:48:43', '2022-12-19 23:09:34', '2022-12-19 23:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `master_cuti_perusahaan`
--

CREATE TABLE `master_cuti_perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `jumlah_cuti` int(11) NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_cuti_perusahaan`
--

INSERT INTO `master_cuti_perusahaan` (`id`, `id_admin`, `jumlah_cuti`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 2, 45, '2022', NULL, NULL),
(2, 10, 30, '2023', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_05_07_013751_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(4, '2022_05_06_120505_create_perusahaan_table', 1),
(5, '2022_05_07_033322_create_jabatan_table', 1),
(6, '2022_05_7_160800_create_golongan_table', 1),
(7, '2022_06_04_103808_create_absensipegawai_table', 1),
(8, '2022_06_17_164136_create_tunjangan_table', 1),
(9, '2022_06_18_030251_create_bonus_table', 1),
(10, '2022_06_20_035749_create_pegawais_table', 1),
(11, '2022_06_20_041650_create_jamabsen_table', 1),
(12, '2022_06_22_031302_create_potongan_table', 1),
(13, '2022_06_24_175203_create_akunpegawai_table', 1),
(14, '2022_06_29_071830_create_izin_table', 1),
(15, '2022_06_29_125340_create_reqabsen_table', 1),
(16, '2022_07_01_041027_create_laporan_table', 1),
(17, '2022_07_01_131018_create_lembur_table', 1),
(18, '2022_07_04_043842_create_hitung_gaji_table', 1),
(19, '2022_07_04_105842_create_datagaji_table', 1),
(20, '2022_07_04_111420_master_cuti_perusahaan', 1),
(21, '2022_07_04_111425_create_table_master_cuti_tahunan', 1),
(22, '2022_07_04_111428_create_cuti_table', 1),
(23, '2022_07_27_171731_create_password_resets_table', 1),
(24, '2022_08_05_032039_create_penggajian_table', 1),
(25, '2022_08_05_100540_create_hari_table', 1),
(26, '2022_08_05_113124_create_riwayatgaji_table', 1),
(27, '2022_08_17_152810_create_cuti_tahunans_table', 1),
(28, '2022_08_17_191718_create_cuti_perusahaans_table', 1),
(29, '2022_09_06_041917_create_reset_code_passwords_table', 1),
(30, '2022_09_27_041248_alter_personal_access_tokens_table', 1),
(31, '2022_11_01_165632_create_pemberitahuan_table', 1),
(32, '2022_11_02_003756_create_harikerja_table', 1),
(33, '2022_06_04_122940_create_absensipegawai_table', 2),
(34, '2022_12_17_020228_create_lokasipegawai_table', 3),
(35, '2022_12_17_025030_create_lokasipegawai_table', 4),
(36, '2022_12_18_225631_create_lokasipegawai_table', 5),
(37, '2022_12_18_232512_create_lokasipegawai_table', 6),
(38, '2022_07_04_141621_create_table_master_cuti_tahunan', 7),
(39, '2022_06_04_041443_create_absensipegawai_table', 8),
(40, '2022_06_29_214253_create_izin_table', 9),
(41, '2022_12_24_041443_create_absensipegawai_table', 10),
(42, '2022_06_04_030106_create_absensipegawai_table', 11),
(43, '2022_07_04_025832_create_cuti_table', 11),
(44, '2022_07_04_104105_create_cuti_table', 12),
(45, '2022_07_04_105102_create_cuti_table', 13),
(46, '2022_07_04_132700_create_cuti_table', 14),
(47, '2022_07_04_133444_create_cuti_table', 15),
(48, '2022_07_04_144004_create_cuti_table', 16),
(49, '2022_07_04_145022_create_cuti_table', 17),
(50, '2022_12_29_130548_create_useractivity_table', 18),
(51, '2022_12_30_052653_add_last_login_to_akunpegawai', 19),
(52, '2022_12_31_024822_create_pengumuman_table', 20),
(53, '2022_12_31_051338_create_pengumuman_table', 21),
(54, '2022_12_31_055239_create_pengumuman_table', 22),
(55, '2022_12_31_074258_create_pengumuman_table', 23),
(56, '2022_12_31_213859_create_job_table', 24),
(57, '2023_01_01_064848_create_job_table', 25),
(58, '2023_01_01_070048_create_job_table', 26),
(59, '2023_01_03_044550_create_job_table', 27),
(60, '2023_01_04_080052_create_shift_table', 28),
(61, '2023_01_04_105258_create_shift_table', 29),
(62, '2022_08_05_010508_create_riwayatgaji_table', 30),
(63, '2022_08_05_010638_create_penggajian_table', 30),
(64, '2023_01_10_060050_create_bonus_table', 31),
(65, '2023_01_10_060214_create_tunjangan_table', 31),
(66, '2023_01_10_060249_create_potongan_table', 31),
(67, '2023_01_14_070046_create_potongabsen_table', 32),
(68, '2022_08_05_110921_create_penggajian_table', 33),
(69, '2022_08_05_111031_create_riwayatgaji_table', 33),
(70, '2022_08_05_102434_create_penggajian_table', 34),
(71, '2022_08_05_102529_create_riwayatgaji_table', 34),
(72, '2022_07_01_103553_create_lembur_table', 35),
(73, '2022_08_05_231841_create_penggajian_table', 36),
(74, '2022_08_05_232016_create_riwayatgaji_table', 36);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawais`
--

CREATE TABLE `pegawais` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_rek` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rek` bigint(20) DEFAULT NULL,
  `id_golongan` int(10) UNSIGNED DEFAULT NULL,
  `id_jabatan` int(10) UNSIGNED NOT NULL,
  `jam_kerja` time DEFAULT NULL,
  `no_ktp` bigint(20) DEFAULT NULL,
  `no_hp` bigint(20) DEFAULT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_kerja` int(11) DEFAULT NULL,
  `jatah_cuti` int(11) DEFAULT NULL,
  `tanggal_masuk` date NOT NULL,
  `ttl` date DEFAULT NULL,
  `status` enum('Aktif','Tidak Aktif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aktif',
  `gender` enum('Laki-Laki','Perempuan') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logout_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pegawais`
--

INSERT INTO `pegawais` (`id`, `id_admin`, `no_pegawai`, `name`, `nama_lengkap`, `email`, `pendidikan`, `jenis_rek`, `no_rek`, `id_golongan`, `id_jabatan`, `jam_kerja`, `no_ktp`, `no_hp`, `alamat`, `jumlah_kerja`, `jatah_cuti`, `tanggal_masuk`, `ttl`, `status`, `gender`, `email_verified_at`, `created_at`, `updated_at`, `logout_at`) VALUES
(4, 2, '7867ggg', 'g', NULL, 'a@gmail.com', NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 1065, NULL, '2020-01-20', NULL, 'Aktif', NULL, NULL, '2022-12-20 07:31:37', '2022-12-20 07:31:37', NULL),
(5, 2, 'hgf56', 'bhbh', NULL, 'b@gmail.com', NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 791, NULL, '2020-10-20', NULL, 'Aktif', NULL, NULL, '2022-12-20 07:34:26', '2022-12-20 07:34:26', NULL),
(6, 2, 'sadsad12321321', 'MNS', NULL, 'faasd@gmail.com', NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, 8, NULL, '2023-01-01', NULL, 'Aktif', NULL, NULL, '2023-01-09 08:33:05', '2023-01-09 08:33:05', NULL),
(2, 2, '22515020911101010', 'Muhammad Fihan Ashidiq', 'Naruto Uzumaki', 'fihan2222as@gmail.com', NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 5, NULL, '2022-11-06', NULL, 'Aktif', NULL, NULL, '2022-11-11 08:26:22', '2022-11-11 08:26:22', NULL),
(1, 2, '2578126312', 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', 'D3 Teknik Informatika', 'Mandiri', 12345678, NULL, 2, '19:50:10', 1234566, 123123142, 'Desa Bakur', 495, 12, '2021-11-02', '2022-11-02', 'Aktif', 'Laki-Laki', NULL, NULL, NULL, NULL),
(3, 2, 'JH123123', 'Tes', 'Nahif', 'fihan2as@student.ub.ac.id', 'D3', 'Mandiri', 12312312312, NULL, 4, '00:00:13', 3519141201010101, 82312312321, 'saasd', 338, NULL, '2022-01-18', '2022-12-20', 'Aktif', 'Laki-Laki', NULL, '2022-12-19 23:00:45', '2022-12-19 23:00:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id`, `id_admin`, `email`, `jenis`, `judul`, `status`, `tanggal`, `jam`, `created_at`, `updated_at`) VALUES
(1, 2, 'fihan2as@gmail.com', 'Update Data', 'Update Profil ', 'Berhasil', '2022-11-05', '10:55:26', '2022-11-04 20:55:26', '2022-11-04 20:55:26'),
(2, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-11-05', '10:55:41', '2022-11-04 20:55:41', '2022-11-04 20:55:41'),
(3, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-11-05', '10:58:09', '2022-11-04 20:58:09', '2022-11-04 20:58:09'),
(4, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2022-11-05', '16:35:19', '2022-11-05 02:35:19', '2022-11-05 02:35:19'),
(5, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-11-05', '16:39:56', '2022-11-05 02:39:56', '2022-11-05 02:39:56'),
(6, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-11-05', '18:12:14', '2022-11-05 04:12:14', '2022-11-05 04:12:14'),
(7, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2022-11-05', '18:13:51', '2022-11-05 04:13:51', '2022-11-05 04:13:51'),
(8, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-11-18', '16:16:23', '2022-11-18 02:16:23', '2022-11-18 02:16:23'),
(9, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-11-21', '12:23:31', '2022-11-20 22:23:31', '2022-11-20 22:23:31'),
(10, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2022-12-15', '18:32:13', '2022-12-15 04:32:13', '2022-12-15 04:32:13'),
(11, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-16', '05:56:23', '2022-12-15 15:56:23', '2022-12-15 15:56:23'),
(12, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-16', '19:38:52', '2022-12-16 05:38:52', '2022-12-16 05:38:52'),
(13, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-16', '19:44:01', '2022-12-16 05:44:01', '2022-12-16 05:44:01'),
(14, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-16', '19:47:18', '2022-12-16 05:47:18', '2022-12-16 05:47:18'),
(15, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-16', '19:47:38', '2022-12-16 05:47:38', '2022-12-16 05:47:38'),
(16, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-16', '20:01:15', '2022-12-16 06:01:15', '2022-12-16 06:01:15'),
(17, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-16', '20:06:44', '2022-12-16 06:06:44', '2022-12-16 06:06:44'),
(18, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '07:53:56', '2022-12-16 17:53:56', '2022-12-16 17:53:56'),
(19, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-17', '08:13:12', '2022-12-16 18:13:12', '2022-12-16 18:13:12'),
(20, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '12:16:46', '2022-12-16 22:16:46', '2022-12-16 22:16:46'),
(21, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '12:24:22', '2022-12-16 22:24:22', '2022-12-16 22:24:22'),
(22, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-17', '14:16:29', '2022-12-17 00:16:29', '2022-12-17 00:16:29'),
(23, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '14:36:21', '2022-12-17 00:36:21', '2022-12-17 00:36:21'),
(24, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-17', '14:37:31', '2022-12-17 00:37:31', '2022-12-17 00:37:31'),
(25, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '14:38:25', '2022-12-17 00:38:25', '2022-12-17 00:38:25'),
(26, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '15:01:30', '2022-12-17 01:01:30', '2022-12-17 01:01:30'),
(27, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-17', '15:02:39', '2022-12-17 01:02:39', '2022-12-17 01:02:39'),
(28, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-17', '16:02:56', '2022-12-17 02:02:56', '2022-12-17 02:02:56'),
(29, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-18', '12:47:28', '2022-12-17 22:47:28', '2022-12-17 22:47:28'),
(30, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '12:50:46', '2022-12-17 22:50:46', '2022-12-17 22:50:46'),
(31, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '13:39:47', '2022-12-17 23:39:47', '2022-12-17 23:39:47'),
(32, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '13:52:59', '2022-12-17 23:52:59', '2022-12-17 23:52:59'),
(33, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-18', '17:00:43', '2022-12-18 03:00:43', '2022-12-18 03:00:43'),
(34, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '19:18:33', '2022-12-18 05:18:33', '2022-12-18 05:18:33'),
(35, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '19:24:34', '2022-12-18 05:24:34', '2022-12-18 05:24:34'),
(36, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-18', '19:27:41', '2022-12-18 05:27:41', '2022-12-18 05:27:41'),
(37, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-18', '19:30:52', '2022-12-18 05:30:52', '2022-12-18 05:30:52'),
(38, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2022-12-18', '23:17:37', '2022-12-18 09:17:37', '2022-12-18 09:17:37'),
(39, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-19', '11:13:27', '2022-12-18 21:13:27', '2022-12-18 21:13:27'),
(40, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-19', '16:40:51', '2022-12-19 02:40:51', '2022-12-19 02:40:51'),
(41, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-19', '20:23:34', '2022-12-19 06:23:34', '2022-12-19 06:23:34'),
(42, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Update Pengajuan Request Attendance', 'Berhasil', '2022-12-19', '20:23:56', '2022-12-19 06:23:56', '2022-12-19 06:23:56'),
(43, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-19', '22:56:01', '2022-12-19 08:56:01', '2022-12-19 08:56:01'),
(44, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '22:56:22', '2022-12-19 08:56:22', '2022-12-19 08:56:22'),
(45, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-19', '22:57:45', '2022-12-19 08:57:45', '2022-12-19 08:57:45'),
(46, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '22:57:54', '2022-12-19 08:57:54', '2022-12-19 08:57:54'),
(47, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-19', '22:58:33', '2022-12-19 08:58:33', '2022-12-19 08:58:33'),
(48, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '22:58:41', '2022-12-19 08:58:41', '2022-12-19 08:58:41'),
(49, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-19', '23:01:14', '2022-12-19 09:01:14', '2022-12-19 09:01:14'),
(50, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '23:01:55', '2022-12-19 09:01:55', '2022-12-19 09:01:55'),
(51, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-19', '23:06:05', '2022-12-19 09:06:05', '2022-12-19 09:06:05'),
(52, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '23:06:15', '2022-12-19 09:06:15', '2022-12-19 09:06:15'),
(53, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-19', '23:07:23', '2022-12-19 09:07:23', '2022-12-19 09:07:23'),
(54, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '23:07:33', '2022-12-19 09:07:33', '2022-12-19 09:07:33'),
(55, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-19', '23:08:26', '2022-12-19 09:08:26', '2022-12-19 09:08:26'),
(56, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '23:08:30', '2022-12-19 09:08:30', '2022-12-19 09:08:30'),
(57, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-19', '23:08:37', '2022-12-19 09:08:37', '2022-12-19 09:08:37'),
(58, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-19', '23:08:44', '2022-12-19 09:08:44', '2022-12-19 09:08:44'),
(59, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Ditolak', '2022-12-20', '07:49:12', '2022-12-19 17:49:12', '2022-12-19 17:49:12'),
(60, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2022-12-20', '08:01:43', '2022-12-19 18:01:43', '2022-12-19 18:01:43'),
(61, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-12-20', '08:05:35', '2022-12-19 18:05:35', '2022-12-19 18:05:35'),
(62, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-20', '08:18:24', '2022-12-19 18:18:24', '2022-12-19 18:18:24'),
(63, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:32:49', '2022-12-19 18:32:49', '2022-12-19 18:32:49'),
(64, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:34:40', '2022-12-19 18:34:40', '2022-12-19 18:34:40'),
(65, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:37:31', '2022-12-19 18:37:31', '2022-12-19 18:37:31'),
(66, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:37:51', '2022-12-19 18:37:51', '2022-12-19 18:37:51'),
(67, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:45:08', '2022-12-19 18:45:08', '2022-12-19 18:45:08'),
(68, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:46:19', '2022-12-19 18:46:19', '2022-12-19 18:46:19'),
(69, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:48:00', '2022-12-19 18:48:00', '2022-12-19 18:48:00'),
(70, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:51:56', '2022-12-19 18:51:56', '2022-12-19 18:51:56'),
(71, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '08:52:35', '2022-12-19 18:52:35', '2022-12-19 18:52:35'),
(72, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '09:03:46', '2022-12-19 19:03:46', '2022-12-19 19:03:46'),
(73, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '09:04:47', '2022-12-19 19:04:47', '2022-12-19 19:04:47'),
(74, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '09:09:47', '2022-12-19 19:09:47', '2022-12-19 19:09:47'),
(75, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '09:10:32', '2022-12-19 19:10:32', '2022-12-19 19:10:32'),
(76, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2022-12-20', '09:11:00', '2022-12-19 19:11:00', '2022-12-19 19:11:00'),
(77, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-20', '09:31:23', '2022-12-19 19:31:23', '2022-12-19 19:31:23'),
(78, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '10:06:25', '2022-12-19 20:06:25', '2022-12-19 20:06:25'),
(79, 2, 'fihan2as@student.ub.ac.id', 'Update Data', 'Update Profil ', 'Berhasil', '2022-12-20', '13:04:33', '2022-12-19 23:04:33', '2022-12-19 23:04:33'),
(80, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-20', '13:04:55', '2022-12-19 23:04:55', '2022-12-19 23:04:55'),
(81, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '13:05:15', '2022-12-19 23:05:15', '2022-12-19 23:05:15'),
(82, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-20', '13:09:59', '2022-12-19 23:09:59', '2022-12-19 23:09:59'),
(83, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '13:10:13', '2022-12-19 23:10:13', '2022-12-19 23:10:13'),
(84, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-20', '13:12:20', '2022-12-19 23:12:20', '2022-12-19 23:12:20'),
(85, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '13:12:32', '2022-12-19 23:12:32', '2022-12-19 23:12:32'),
(86, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '16:35:34', '2022-12-20 02:35:34', '2022-12-20 02:35:34'),
(87, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '16:36:19', '2022-12-20 02:36:19', '2022-12-20 02:36:19'),
(88, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '16:36:24', '2022-12-20 02:36:24', '2022-12-20 02:36:24'),
(89, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '16:37:12', '2022-12-20 02:37:12', '2022-12-20 02:37:12'),
(90, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '16:37:16', '2022-12-20 02:37:16', '2022-12-20 02:37:16'),
(91, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '16:37:32', '2022-12-20 02:37:32', '2022-12-20 02:37:32'),
(92, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '16:38:19', '2022-12-20 02:38:19', '2022-12-20 02:38:19'),
(93, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '16:38:30', '2022-12-20 02:38:30', '2022-12-20 02:38:30'),
(94, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '17:26:14', '2022-12-20 03:26:14', '2022-12-20 03:26:14'),
(95, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-20', '17:28:53', '2022-12-20 03:28:53', '2022-12-20 03:28:53'),
(96, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-20', '17:29:34', '2022-12-20 03:29:34', '2022-12-20 03:29:34'),
(97, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '17:31:02', '2022-12-20 03:31:02', '2022-12-20 03:31:02'),
(98, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '17:32:05', '2022-12-20 03:32:05', '2022-12-20 03:32:05'),
(99, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '17:44:20', '2022-12-20 03:44:20', '2022-12-20 03:44:20'),
(100, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '17:44:50', '2022-12-20 03:44:50', '2022-12-20 03:44:50'),
(101, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '17:45:10', '2022-12-20 03:45:10', '2022-12-20 03:45:10'),
(102, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '17:45:24', '2022-12-20 03:45:24', '2022-12-20 03:45:24'),
(103, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-20', '17:47:57', '2022-12-20 03:47:57', '2022-12-20 03:47:57'),
(104, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-20', '18:20:01', '2022-12-20 04:20:01', '2022-12-20 04:20:01'),
(105, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-20', '18:20:49', '2022-12-20 04:20:49', '2022-12-20 04:20:49'),
(106, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-20', '23:13:10', '2022-12-20 09:13:10', '2022-12-20 09:13:10'),
(107, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan', 'Menunggu Konfirmasi', '2022-12-21', '07:16:08', '2022-12-20 17:16:08', '2022-12-20 17:16:08'),
(108, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-21', '07:17:10', '2022-12-20 17:17:10', '2022-12-20 17:17:10'),
(109, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-21', '07:17:42', '2022-12-20 17:17:42', '2022-12-20 17:17:42'),
(110, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-21', '07:19:56', '2022-12-20 17:19:56', '2022-12-20 17:19:56'),
(111, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:48:02', '2022-12-21 18:48:02', '2022-12-21 18:48:02'),
(112, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:49:33', '2022-12-21 18:49:33', '2022-12-21 18:49:33'),
(113, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:49:57', '2022-12-21 18:49:57', '2022-12-21 18:49:57'),
(114, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:52:49', '2022-12-21 18:52:49', '2022-12-21 18:52:49'),
(115, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:53:11', '2022-12-21 18:53:11', '2022-12-21 18:53:11'),
(116, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:53:14', '2022-12-21 18:53:14', '2022-12-21 18:53:14'),
(117, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:54:14', '2022-12-21 18:54:14', '2022-12-21 18:54:14'),
(118, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:54:16', '2022-12-21 18:54:16', '2022-12-21 18:54:16'),
(119, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:56:16', '2022-12-21 18:56:16', '2022-12-21 18:56:16'),
(120, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:56:19', '2022-12-21 18:56:19', '2022-12-21 18:56:19'),
(121, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:57:16', '2022-12-21 18:57:16', '2022-12-21 18:57:16'),
(122, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:57:20', '2022-12-21 18:57:20', '2022-12-21 18:57:20'),
(123, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:57:52', '2022-12-21 18:57:52', '2022-12-21 18:57:52'),
(124, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:57:55', '2022-12-21 18:57:55', '2022-12-21 18:57:55'),
(125, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:58:11', '2022-12-21 18:58:11', '2022-12-21 18:58:11'),
(126, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-22', '08:58:37', '2022-12-21 18:58:37', '2022-12-21 18:58:37'),
(127, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-22', '08:58:39', '2022-12-21 18:58:39', '2022-12-21 18:58:39'),
(128, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-22', '09:19:27', '2022-12-21 19:19:27', '2022-12-21 19:19:27'),
(129, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-23', '23:22:33', '2022-12-23 09:22:33', '2022-12-23 09:22:33'),
(130, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-23', '23:22:45', '2022-12-23 09:22:45', '2022-12-23 09:22:45'),
(131, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-24', '11:41:10', '2022-12-23 21:41:10', '2022-12-23 21:41:10'),
(132, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-24', '11:43:50', '2022-12-23 21:43:50', '2022-12-23 21:43:50'),
(133, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-24', '11:44:22', '2022-12-23 21:44:22', '2022-12-23 21:44:22'),
(134, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-26', '12:50:06', '2022-12-25 22:50:06', '2022-12-25 22:50:06'),
(135, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-26', '12:54:16', '2022-12-25 22:54:16', '2022-12-25 22:54:16'),
(136, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-26', '16:14:59', '2022-12-26 02:14:59', '2022-12-26 02:14:59'),
(137, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-27', '04:24:38', '2022-12-26 14:24:38', '2022-12-26 14:24:38'),
(138, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:25:09', '2022-12-26 14:25:09', '2022-12-26 14:25:09'),
(139, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-27', '04:30:54', '2022-12-26 14:30:54', '2022-12-26 14:30:54'),
(140, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:31:46', '2022-12-26 14:31:46', '2022-12-26 14:31:46'),
(141, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-27', '04:50:59', '2022-12-26 14:50:59', '2022-12-26 14:50:59'),
(142, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:51:18', '2022-12-26 14:51:18', '2022-12-26 14:51:18'),
(143, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:52:21', '2022-12-26 14:52:21', '2022-12-26 14:52:21'),
(144, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '04:55:41', '2022-12-26 14:55:41', '2022-12-26 14:55:41'),
(145, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:55:57', '2022-12-26 14:55:57', '2022-12-26 14:55:57'),
(146, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '04:57:26', '2022-12-26 14:57:26', '2022-12-26 14:57:26'),
(147, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:58:01', '2022-12-26 14:58:01', '2022-12-26 14:58:01'),
(148, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-27', '04:58:47', '2022-12-26 14:58:47', '2022-12-26 14:58:47'),
(149, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '04:59:17', '2022-12-26 14:59:17', '2022-12-26 14:59:17'),
(150, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '05:00:02', '2022-12-26 15:00:02', '2022-12-26 15:00:02'),
(151, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '05:00:16', '2022-12-26 15:00:16', '2022-12-26 15:00:16'),
(152, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '05:02:04', '2022-12-26 15:02:04', '2022-12-26 15:02:04'),
(153, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '05:13:02', '2022-12-26 15:13:02', '2022-12-26 15:13:02'),
(154, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '05:13:16', '2022-12-26 15:13:16', '2022-12-26 15:13:16'),
(155, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '05:13:47', '2022-12-26 15:13:47', '2022-12-26 15:13:47'),
(156, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-27', '05:13:50', '2022-12-26 15:13:50', '2022-12-26 15:13:50'),
(157, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-27', '05:16:47', '2022-12-26 15:16:47', '2022-12-26 15:16:47'),
(158, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-27', '05:17:00', '2022-12-26 15:17:00', '2022-12-26 15:17:00'),
(159, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:24:23', '2022-12-26 20:24:23', '2022-12-26 20:24:23'),
(160, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:24:51', '2022-12-26 20:24:51', '2022-12-26 20:24:51'),
(161, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:30:10', '2022-12-26 20:30:10', '2022-12-26 20:30:10'),
(162, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:31:45', '2022-12-26 20:31:45', '2022-12-26 20:31:45'),
(163, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:33:33', '2022-12-26 20:33:33', '2022-12-26 20:33:33'),
(164, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:35:30', '2022-12-26 20:35:30', '2022-12-26 20:35:30'),
(165, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:36:41', '2022-12-26 20:36:41', '2022-12-26 20:36:41'),
(166, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:40:20', '2022-12-26 20:40:20', '2022-12-26 20:40:20'),
(167, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:41:28', '2022-12-26 20:41:28', '2022-12-26 20:41:28'),
(168, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:42:22', '2022-12-26 20:42:22', '2022-12-26 20:42:22'),
(169, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:43:29', '2022-12-26 20:43:29', '2022-12-26 20:43:29'),
(170, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:55:59', '2022-12-26 20:55:59', '2022-12-26 20:55:59'),
(171, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '10:58:43', '2022-12-26 20:58:43', '2022-12-26 20:58:43'),
(172, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '11:00:45', '2022-12-26 21:00:45', '2022-12-26 21:00:45'),
(173, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '11:02:06', '2022-12-26 21:02:06', '2022-12-26 21:02:06'),
(174, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '11:05:02', '2022-12-26 21:05:02', '2022-12-26 21:05:02'),
(175, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '11:05:39', '2022-12-26 21:05:39', '2022-12-26 21:05:39'),
(176, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '11:06:06', '2022-12-26 21:06:06', '2022-12-26 21:06:06'),
(177, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '13:09:11', '2022-12-26 23:09:11', '2022-12-26 23:09:11'),
(178, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Hapus Pengajuan Cuti ', 'Berhasil', '2022-12-27', '13:10:22', '2022-12-26 23:10:22', '2022-12-26 23:10:22'),
(179, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '14:00:09', '2022-12-27 00:00:09', '2022-12-27 00:00:09'),
(180, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '14:04:55', '2022-12-27 00:04:55', '2022-12-27 00:04:55'),
(181, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '14:40:56', '2022-12-27 00:40:56', '2022-12-27 00:40:56'),
(182, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '14:40:59', '2022-12-27 00:40:59', '2022-12-27 00:40:59'),
(183, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '15:05:52', '2022-12-27 01:05:52', '2022-12-27 01:05:52'),
(184, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '15:06:06', '2022-12-27 01:06:06', '2022-12-27 01:06:06'),
(185, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '15:48:37', '2022-12-27 01:48:37', '2022-12-27 01:48:37'),
(186, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '15:48:51', '2022-12-27 01:48:51', '2022-12-27 01:48:51'),
(187, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '16:31:59', '2022-12-27 02:31:59', '2022-12-27 02:31:59'),
(188, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '16:35:45', '2022-12-27 02:35:45', '2022-12-27 02:35:45'),
(189, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '16:36:06', '2022-12-27 02:36:06', '2022-12-27 02:36:06'),
(190, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '16:38:10', '2022-12-27 02:38:10', '2022-12-27 02:38:10'),
(191, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '17:55:56', '2022-12-27 03:55:56', '2022-12-27 03:55:56'),
(192, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '18:28:51', '2022-12-27 04:28:51', '2022-12-27 04:28:51'),
(193, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '18:29:15', '2022-12-27 04:29:15', '2022-12-27 04:29:15'),
(194, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '18:29:17', '2022-12-27 04:29:17', '2022-12-27 04:29:17'),
(195, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '18:29:50', '2022-12-27 04:29:50', '2022-12-27 04:29:50'),
(196, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '18:30:00', '2022-12-27 04:30:00', '2022-12-27 04:30:00'),
(197, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '18:35:35', '2022-12-27 04:35:35', '2022-12-27 04:35:35'),
(198, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '20:35:58', '2022-12-27 06:35:58', '2022-12-27 06:35:58'),
(199, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '20:38:10', '2022-12-27 06:38:10', '2022-12-27 06:38:10'),
(200, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '20:42:42', '2022-12-27 06:42:42', '2022-12-27 06:42:42'),
(201, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '20:42:52', '2022-12-27 06:42:52', '2022-12-27 06:42:52'),
(202, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '20:44:21', '2022-12-27 06:44:21', '2022-12-27 06:44:21'),
(203, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '20:44:29', '2022-12-27 06:44:29', '2022-12-27 06:44:29'),
(204, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '20:51:09', '2022-12-27 06:51:09', '2022-12-27 06:51:09'),
(205, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:03:21', '2022-12-27 07:03:21', '2022-12-27 07:03:21'),
(206, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:06:38', '2022-12-27 07:06:38', '2022-12-27 07:06:38'),
(207, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:24:05', '2022-12-27 07:24:05', '2022-12-27 07:24:05'),
(208, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:25:10', '2022-12-27 07:25:10', '2022-12-27 07:25:10'),
(209, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Update Pengajuan Cuti ', 'Berhasil', '2022-12-27', '21:25:54', '2022-12-27 07:25:54', '2022-12-27 07:25:54'),
(210, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Update Pengajuan Cuti ', 'Berhasil', '2022-12-27', '21:27:51', '2022-12-27 07:27:51', '2022-12-27 07:27:51'),
(211, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '21:28:40', '2022-12-27 07:28:40', '2022-12-27 07:28:40'),
(212, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:42:22', '2022-12-27 07:42:22', '2022-12-27 07:42:22'),
(213, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:42:55', '2022-12-27 07:42:55', '2022-12-27 07:42:55'),
(214, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '21:43:05', '2022-12-27 07:43:05', '2022-12-27 07:43:05'),
(215, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '21:43:07', '2022-12-27 07:43:07', '2022-12-27 07:43:07'),
(216, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:52:18', '2022-12-27 07:52:18', '2022-12-27 07:52:18'),
(217, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '21:52:34', '2022-12-27 07:52:34', '2022-12-27 07:52:34'),
(218, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '21:52:42', '2022-12-27 07:52:42', '2022-12-27 07:52:42'),
(219, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-27', '21:52:44', '2022-12-27 07:52:44', '2022-12-27 07:52:44'),
(220, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-23', '22:07:07', '2022-12-23 08:07:07', '2022-12-23 08:07:07'),
(221, 2, 'fihan2as@student.ub.ac.id', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-23', '22:07:16', '2022-12-23 08:07:16', '2022-12-23 08:07:16'),
(222, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-27', '22:09:09', '2022-12-27 08:09:09', '2022-12-27 08:09:09'),
(223, 2, 'fihan2as@student.ub.ac.id', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-27', '22:09:13', '2022-12-27 08:09:13', '2022-12-27 08:09:13'),
(224, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2022-12-27', '22:16:52', '2022-12-27 08:16:52', '2022-12-27 08:16:52'),
(225, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '22:33:57', '2022-12-27 08:33:57', '2022-12-27 08:33:57'),
(226, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Hapus Pengajuan Cuti ', 'Berhasil', '2022-12-27', '22:36:10', '2022-12-27 08:36:10', '2022-12-27 08:36:10'),
(227, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '22:36:23', '2022-12-27 08:36:23', '2022-12-27 08:36:23'),
(228, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Hapus Pengajuan Cuti ', 'Berhasil', '2022-12-27', '22:44:42', '2022-12-27 08:44:42', '2022-12-27 08:44:42'),
(229, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-27', '22:45:01', '2022-12-27 08:45:01', '2022-12-27 08:45:01'),
(230, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-28', '08:29:32', '2022-12-27 18:29:32', '2022-12-27 18:29:32'),
(231, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-28', '08:29:57', '2022-12-27 18:29:57', '2022-12-27 18:29:57'),
(232, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-28', '08:41:56', '2022-12-27 18:41:56', '2022-12-27 18:41:56'),
(233, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-28', '08:42:09', '2022-12-27 18:42:09', '2022-12-27 18:42:09'),
(234, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-28', '08:42:49', '2022-12-27 18:42:49', '2022-12-27 18:42:49'),
(235, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Hapus Pengajuan Cuti ', 'Berhasil', '2022-12-28', '08:43:19', '2022-12-27 18:43:19', '2022-12-27 18:43:19'),
(236, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-28', '13:01:36', '2022-12-27 23:01:36', '2022-12-27 23:01:36'),
(237, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-28', '13:02:18', '2022-12-27 23:02:18', '2022-12-27 23:02:18'),
(238, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-28', '13:02:56', '2022-12-27 23:02:56', '2022-12-27 23:02:56'),
(239, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-28', '13:03:52', '2022-12-27 23:03:52', '2022-12-27 23:03:52'),
(240, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-28', '13:08:24', '2022-12-27 23:08:24', '2022-12-27 23:08:24'),
(241, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-28', '13:08:43', '2022-12-27 23:08:43', '2022-12-27 23:08:43'),
(242, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-28', '13:38:05', '2022-12-27 23:38:05', '2022-12-27 23:38:05'),
(243, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Ditolak', '2022-12-28', '14:18:57', '2022-12-28 00:18:57', '2022-12-28 00:18:57'),
(244, 2, 'fihan2as@gmail.com', 'Pengajuan Cuti', 'Pengajuan Cuti', 'Menunggu Konfirmasi', '2022-12-28', '14:19:27', '2022-12-28 00:19:27', '2022-12-28 00:19:27'),
(245, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-28', '14:22:32', '2022-12-28 00:22:32', '2022-12-28 00:22:32'),
(246, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2022-12-28', '14:23:52', '2022-12-28 00:23:52', '2022-12-28 00:23:52'),
(247, 2, 'fihan2as@gmail.com', 'Laporan', 'Pengajuan laporan ', 'Menunggu Konfirmasi', '2022-12-28', '14:32:46', '2022-12-28 00:32:46', '2022-12-28 00:32:46'),
(248, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-28', '15:14:48', '2022-12-28 01:14:48', '2022-12-28 01:14:48'),
(249, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-28', '15:14:59', '2022-12-28 01:14:59', '2022-12-28 01:14:59'),
(250, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Pengajuan Request Attendance', 'Menunggu Dikonfirmasi', '2022-12-28', '15:20:23', '2022-12-28 01:20:23', '2022-12-28 01:20:23'),
(251, 2, 'fihan2as@gmail.com', 'Laporan', 'Approvement Laporan', 'Diterima', '2022-12-28', '15:22:36', '2022-12-28 01:22:36', '2022-12-28 01:22:36'),
(252, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Ditolak', '2022-12-28', '15:40:01', '2022-12-28 01:40:01', '2022-12-28 01:40:01'),
(253, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-28', '15:41:19', '2022-12-28 01:41:19', '2022-12-28 01:41:19'),
(254, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-28', '15:44:35', '2022-12-28 01:44:35', '2022-12-28 01:44:35'),
(255, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-28', '15:45:16', '2022-12-28 01:45:16', '2022-12-28 01:45:16'),
(256, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-28', '16:02:11', '2022-12-28 02:02:11', '2022-12-28 02:02:11'),
(257, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2022-12-28', '19:37:01', '2022-12-28 05:37:01', '2022-12-28 05:37:01'),
(258, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2022-12-28', '20:14:54', '2022-12-28 06:14:54', '2022-12-28 06:14:54'),
(259, 2, 'fihan2as@gmail.com', 'Request Absensi', 'Approvement Request Attendance', 'Diterima', '2022-12-28', '20:15:35', '2022-12-28 06:15:35', '2022-12-28 06:15:35'),
(260, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-29', '07:33:45', '2022-12-28 17:33:45', '2022-12-28 17:33:45'),
(261, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-29', '07:34:49', '2022-12-28 17:34:49', '2022-12-28 17:34:49'),
(262, 2, 'fihan2as@student.ub.ac.id', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-29', '07:38:04', '2022-12-28 17:38:04', '2022-12-28 17:38:04'),
(263, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-29', '07:38:17', '2022-12-28 17:38:17', '2022-12-28 17:38:17'),
(264, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-29', '07:38:42', '2022-12-28 17:38:42', '2022-12-28 17:38:42'),
(265, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-29', '07:43:15', '2022-12-28 17:43:15', '2022-12-28 17:43:15'),
(266, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-29', '07:43:27', '2022-12-28 17:43:27', '2022-12-28 17:43:27'),
(267, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-29', '07:43:51', '2022-12-28 17:43:51', '2022-12-28 17:43:51'),
(268, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-29', '07:43:59', '2022-12-28 17:43:59', '2022-12-28 17:43:59'),
(269, 2, 'fihan2as@student.ub.ac.id', 'Approvement', 'Approvement Izin ', 'Ditolak', '2022-12-29', '07:44:09', '2022-12-28 17:44:09', '2022-12-28 17:44:09'),
(270, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Cuti ', 'Diterima', '2022-12-29', '07:45:46', '2022-12-28 17:45:46', '2022-12-28 17:45:46'),
(271, 2, 'fihan2as@student.ub.ac.id', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2022-12-29', '07:47:29', '2022-12-28 17:47:29', '2022-12-28 17:47:29'),
(272, 2, 'fihan2as@gmail.com', 'Pengajuan', 'Pengajuan Izin ', 'Belum Dikonfirmasi', '2022-12-29', '12:25:47', '2022-12-28 22:25:47', '2022-12-28 22:25:47'),
(273, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-12-29', '12:53:13', '2022-12-28 22:53:13', '2022-12-28 22:53:13'),
(274, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2022-12-29', '13:08:43', '2022-12-28 23:08:43', '2022-12-28 23:08:43'),
(275, 2, 'fihan2as@gmail.com', 'Approvement', 'Approvement Izin ', 'Diterima', '2022-12-31', '18:32:30', '2022-12-31 04:32:30', '2022-12-31 04:32:30'),
(276, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2023-01-02', '08:52:06', '2023-01-01 18:52:06', '2023-01-01 18:52:06'),
(277, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-02', '11:00:47', '2023-01-01 21:00:47', '2023-01-01 21:00:47'),
(278, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-02', '11:01:17', '2023-01-01 21:01:17', '2023-01-01 21:01:17'),
(279, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-02', '11:04:44', '2023-01-01 21:04:44', '2023-01-01 21:04:44'),
(280, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-02', '11:04:57', '2023-01-01 21:04:57', '2023-01-01 21:04:57'),
(281, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-02', '11:05:12', '2023-01-01 21:05:12', '2023-01-01 21:05:12'),
(282, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-02', '11:05:42', '2023-01-01 21:05:42', '2023-01-01 21:05:42'),
(283, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-02', '11:06:26', '2023-01-01 21:06:26', '2023-01-01 21:06:26'),
(284, 1, 'fihan2as@gmail.com', 'Pengajuan', 'Update Pengajuan Izin ', 'Update Berhasil', '2023-01-03', '05:51:19', '2023-01-02 15:51:19', '2023-01-02 15:51:19'),
(285, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'Terlambat', '2023-01-03', '20:25:58', '2023-01-03 06:25:58', '2023-01-03 06:25:58'),
(286, 2, 'fihan2as@gmail.com', 'Presensi Pulang', 'Presensi Pulang', 'Berhasil', '2023-01-03', '20:26:06', '2023-01-03 06:26:06', '2023-01-03 06:26:06'),
(287, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-04', '07:42:43', '2023-01-03 17:42:43', '2023-01-03 17:42:43'),
(288, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-04', '07:43:09', '2023-01-03 17:43:09', '2023-01-03 17:43:09'),
(289, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-04', '07:58:10', '2023-01-03 17:58:10', '2023-01-03 17:58:10'),
(290, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-04', '08:33:05', '2023-01-03 18:33:05', '2023-01-03 18:33:05'),
(291, 2, 'fihan2as@student.ub.ac.id', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-04', '08:36:09', '2023-01-03 18:36:09', '2023-01-03 18:36:09'),
(292, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-04', '08:36:44', '2023-01-03 18:36:44', '2023-01-03 18:36:44'),
(293, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-04', '08:39:29', '2023-01-03 18:39:29', '2023-01-03 18:39:29'),
(294, 2, 'fihan2as@student.ub.ac.id', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-04', '08:39:49', '2023-01-03 18:39:49', '2023-01-03 18:39:49'),
(295, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2023-01-04', '14:53:26', '2023-01-04 00:53:26', '2023-01-04 00:53:26'),
(296, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-04', '14:56:53', '2023-01-04 00:56:53', '2023-01-04 00:56:53'),
(297, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-04', '14:57:07', '2023-01-04 00:57:07', '2023-01-04 00:57:07'),
(298, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-04', '14:58:58', '2023-01-04 00:58:58', '2023-01-04 00:58:58'),
(299, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-04', '14:59:17', '2023-01-04 00:59:17', '2023-01-04 00:59:17'),
(300, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-06', '16:57:23', '2023-01-06 02:57:23', '2023-01-06 02:57:23'),
(301, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-07', '18:51:29', '2023-01-07 04:51:29', '2023-01-07 04:51:29'),
(302, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-07', '19:41:50', '2023-01-07 05:41:50', '2023-01-07 05:41:50'),
(303, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-07', '19:42:13', '2023-01-07 05:42:13', '2023-01-07 05:42:13'),
(304, 2, 'fihan2as@student.ub.ac.id', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-07', '19:44:29', '2023-01-07 05:44:29', '2023-01-07 05:44:29'),
(305, 2, 'fihan2as@student.ub.ac.id', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-07', '19:44:31', '2023-01-07 05:44:31', '2023-01-07 05:44:31'),
(306, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-07', '19:44:55', '2023-01-07 05:44:55', '2023-01-07 05:44:55'),
(307, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-07', '19:49:48', '2023-01-07 05:49:48', '2023-01-07 05:49:48'),
(308, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-07', '19:54:20', '2023-01-07 05:54:20', '2023-01-07 05:54:20'),
(309, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '07:58:10', '2023-01-08 17:58:10', '2023-01-08 17:58:10'),
(310, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '07:58:16', '2023-01-08 17:58:16', '2023-01-08 17:58:16'),
(311, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2023-01-09', '08:58:38', '2023-01-08 18:58:38', '2023-01-08 18:58:38'),
(312, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:35:26', '2023-01-09 06:35:26', '2023-01-09 06:35:26'),
(313, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:35:43', '2023-01-09 06:35:43', '2023-01-09 06:35:43'),
(314, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:40:45', '2023-01-09 06:40:45', '2023-01-09 06:40:45'),
(315, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:42:26', '2023-01-09 06:42:26', '2023-01-09 06:42:26'),
(316, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:45:55', '2023-01-09 06:45:55', '2023-01-09 06:45:55'),
(317, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '20:50:37', '2023-01-09 06:50:37', '2023-01-09 06:50:37'),
(318, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '20:50:45', '2023-01-09 06:50:45', '2023-01-09 06:50:45');
INSERT INTO `pemberitahuan` (`id`, `id_admin`, `email`, `jenis`, `judul`, `status`, `tanggal`, `jam`, `created_at`, `updated_at`) VALUES
(319, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:51:03', '2023-01-09 06:51:03', '2023-01-09 06:51:03'),
(320, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '20:53:22', '2023-01-09 06:53:22', '2023-01-09 06:53:22'),
(321, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '20:53:31', '2023-01-09 06:53:31', '2023-01-09 06:53:31'),
(322, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '20:53:45', '2023-01-09 06:53:45', '2023-01-09 06:53:45'),
(323, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:01:13', '2023-01-09 07:01:13', '2023-01-09 07:01:13'),
(324, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:02:52', '2023-01-09 07:02:52', '2023-01-09 07:02:52'),
(325, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:03:07', '2023-01-09 07:03:07', '2023-01-09 07:03:07'),
(326, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:04:43', '2023-01-09 07:04:43', '2023-01-09 07:04:43'),
(327, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:04:54', '2023-01-09 07:04:54', '2023-01-09 07:04:54'),
(328, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:05:10', '2023-01-09 07:05:10', '2023-01-09 07:05:10'),
(329, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:08:03', '2023-01-09 07:08:03', '2023-01-09 07:08:03'),
(330, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:08:16', '2023-01-09 07:08:16', '2023-01-09 07:08:16'),
(331, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:08:50', '2023-01-09 07:08:50', '2023-01-09 07:08:50'),
(332, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:20:25', '2023-01-09 07:20:25', '2023-01-09 07:20:25'),
(333, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:20:35', '2023-01-09 07:20:35', '2023-01-09 07:20:35'),
(334, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:20:52', '2023-01-09 07:20:52', '2023-01-09 07:20:52'),
(335, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:21:24', '2023-01-09 07:21:24', '2023-01-09 07:21:24'),
(336, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:28:10', '2023-01-09 07:28:10', '2023-01-09 07:28:10'),
(337, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:28:20', '2023-01-09 07:28:20', '2023-01-09 07:28:20'),
(338, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:28:34', '2023-01-09 07:28:34', '2023-01-09 07:28:34'),
(339, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:30:27', '2023-01-09 07:30:27', '2023-01-09 07:30:27'),
(340, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:31:10', '2023-01-09 07:31:10', '2023-01-09 07:31:10'),
(341, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:31:37', '2023-01-09 07:31:37', '2023-01-09 07:31:37'),
(342, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:34:43', '2023-01-09 07:34:43', '2023-01-09 07:34:43'),
(343, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:34:56', '2023-01-09 07:34:56', '2023-01-09 07:34:56'),
(344, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:35:07', '2023-01-09 07:35:07', '2023-01-09 07:35:07'),
(345, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:41:25', '2023-01-09 07:41:25', '2023-01-09 07:41:25'),
(346, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:41:37', '2023-01-09 07:41:37', '2023-01-09 07:41:37'),
(347, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:41:51', '2023-01-09 07:41:51', '2023-01-09 07:41:51'),
(348, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:44:12', '2023-01-09 07:44:12', '2023-01-09 07:44:12'),
(349, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:44:18', '2023-01-09 07:44:18', '2023-01-09 07:44:18'),
(350, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:44:30', '2023-01-09 07:44:30', '2023-01-09 07:44:30'),
(351, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:50:04', '2023-01-09 07:50:04', '2023-01-09 07:50:04'),
(352, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:50:10', '2023-01-09 07:50:10', '2023-01-09 07:50:10'),
(353, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:50:20', '2023-01-09 07:50:20', '2023-01-09 07:50:20'),
(354, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:52:58', '2023-01-09 07:52:58', '2023-01-09 07:52:58'),
(355, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:53:04', '2023-01-09 07:53:04', '2023-01-09 07:53:04'),
(356, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:53:13', '2023-01-09 07:53:13', '2023-01-09 07:53:13'),
(357, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '21:56:13', '2023-01-09 07:56:13', '2023-01-09 07:56:13'),
(358, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '21:56:50', '2023-01-09 07:56:50', '2023-01-09 07:56:50'),
(359, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '21:57:06', '2023-01-09 07:57:06', '2023-01-09 07:57:06'),
(360, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:01:21', '2023-01-09 08:01:21', '2023-01-09 08:01:21'),
(361, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '22:01:32', '2023-01-09 08:01:32', '2023-01-09 08:01:32'),
(362, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '22:01:45', '2023-01-09 08:01:45', '2023-01-09 08:01:45'),
(363, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:05:25', '2023-01-09 08:05:25', '2023-01-09 08:05:25'),
(364, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '22:05:32', '2023-01-09 08:05:32', '2023-01-09 08:05:32'),
(365, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '22:05:42', '2023-01-09 08:05:42', '2023-01-09 08:05:42'),
(366, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:07:38', '2023-01-09 08:07:38', '2023-01-09 08:07:38'),
(367, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '22:07:46', '2023-01-09 08:07:46', '2023-01-09 08:07:46'),
(368, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '22:07:57', '2023-01-09 08:07:57', '2023-01-09 08:07:57'),
(369, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:18:12', '2023-01-09 08:18:12', '2023-01-09 08:18:12'),
(370, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '22:18:21', '2023-01-09 08:18:21', '2023-01-09 08:18:21'),
(371, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '22:18:47', '2023-01-09 08:18:47', '2023-01-09 08:18:47'),
(372, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:24:57', '2023-01-09 08:24:57', '2023-01-09 08:24:57'),
(373, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-09', '22:25:05', '2023-01-09 08:25:05', '2023-01-09 08:25:05'),
(374, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-09', '22:25:15', '2023-01-09 08:25:15', '2023-01-09 08:25:15'),
(375, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:43:04', '2023-01-09 08:43:04', '2023-01-09 08:43:04'),
(376, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:44:07', '2023-01-09 08:44:07', '2023-01-09 08:44:07'),
(377, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:45:11', '2023-01-09 08:45:11', '2023-01-09 08:45:11'),
(378, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:45:53', '2023-01-09 08:45:53', '2023-01-09 08:45:53'),
(379, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:46:32', '2023-01-09 08:46:32', '2023-01-09 08:46:32'),
(380, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '22:54:06', '2023-01-09 08:54:06', '2023-01-09 08:54:06'),
(381, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:05:51', '2023-01-09 09:05:51', '2023-01-09 09:05:51'),
(382, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:06:26', '2023-01-09 09:06:26', '2023-01-09 09:06:26'),
(383, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:06:51', '2023-01-09 09:06:51', '2023-01-09 09:06:51'),
(384, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:10:09', '2023-01-09 09:10:09', '2023-01-09 09:10:09'),
(385, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:10:28', '2023-01-09 09:10:28', '2023-01-09 09:10:28'),
(386, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:10:40', '2023-01-09 09:10:40', '2023-01-09 09:10:40'),
(387, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:10:48', '2023-01-09 09:10:48', '2023-01-09 09:10:48'),
(388, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:10:56', '2023-01-09 09:10:56', '2023-01-09 09:10:56'),
(389, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:11:08', '2023-01-09 09:11:08', '2023-01-09 09:11:08'),
(390, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-09', '23:38:39', '2023-01-09 09:38:39', '2023-01-09 09:38:39'),
(391, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '08:15:11', '2023-01-09 18:15:11', '2023-01-09 18:15:11'),
(392, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '08:30:00', '2023-01-09 18:30:00', '2023-01-09 18:30:00'),
(393, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '08:30:52', '2023-01-09 18:30:52', '2023-01-09 18:30:52'),
(394, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '08:33:39', '2023-01-09 18:33:39', '2023-01-09 18:33:39'),
(395, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '08:36:07', '2023-01-09 18:36:07', '2023-01-09 18:36:07'),
(396, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '08:37:38', '2023-01-09 18:37:38', '2023-01-09 18:37:38'),
(397, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '08:48:16', '2023-01-09 18:48:16', '2023-01-09 18:48:16'),
(398, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '09:00:21', '2023-01-09 19:00:21', '2023-01-09 19:00:21'),
(399, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '09:07:00', '2023-01-09 19:07:00', '2023-01-09 19:07:00'),
(400, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '13:15:13', '2023-01-09 23:15:13', '2023-01-09 23:15:13'),
(401, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '13:15:45', '2023-01-09 23:15:45', '2023-01-09 23:15:45'),
(402, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '13:16:05', '2023-01-09 23:16:05', '2023-01-09 23:16:05'),
(403, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '13:17:51', '2023-01-09 23:17:51', '2023-01-09 23:17:51'),
(404, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:02:42', '2023-01-10 00:02:42', '2023-01-10 00:02:42'),
(405, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:16:37', '2023-01-10 00:16:37', '2023-01-10 00:16:37'),
(406, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:27:28', '2023-01-10 00:27:28', '2023-01-10 00:27:28'),
(407, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:28:15', '2023-01-10 00:28:15', '2023-01-10 00:28:15'),
(408, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:32:26', '2023-01-10 00:32:26', '2023-01-10 00:32:26'),
(409, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:33:16', '2023-01-10 00:33:16', '2023-01-10 00:33:16'),
(410, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:45:19', '2023-01-10 00:45:19', '2023-01-10 00:45:19'),
(411, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:46:10', '2023-01-10 00:46:10', '2023-01-10 00:46:10'),
(412, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:47:12', '2023-01-10 00:47:12', '2023-01-10 00:47:12'),
(413, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:47:36', '2023-01-10 00:47:36', '2023-01-10 00:47:36'),
(414, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:49:10', '2023-01-10 00:49:10', '2023-01-10 00:49:10'),
(415, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:49:30', '2023-01-10 00:49:30', '2023-01-10 00:49:30'),
(416, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:50:36', '2023-01-10 00:50:36', '2023-01-10 00:50:36'),
(417, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:51:44', '2023-01-10 00:51:44', '2023-01-10 00:51:44'),
(418, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:52:10', '2023-01-10 00:52:10', '2023-01-10 00:52:10'),
(419, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:52:38', '2023-01-10 00:52:38', '2023-01-10 00:52:38'),
(420, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:53:11', '2023-01-10 00:53:11', '2023-01-10 00:53:11'),
(421, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:53:50', '2023-01-10 00:53:50', '2023-01-10 00:53:50'),
(422, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:54:24', '2023-01-10 00:54:24', '2023-01-10 00:54:24'),
(423, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:58:46', '2023-01-10 00:58:46', '2023-01-10 00:58:46'),
(424, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:59:03', '2023-01-10 00:59:03', '2023-01-10 00:59:03'),
(425, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '14:59:15', '2023-01-10 00:59:15', '2023-01-10 00:59:15'),
(426, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:14:37', '2023-01-10 01:14:37', '2023-01-10 01:14:37'),
(427, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:17:19', '2023-01-10 01:17:19', '2023-01-10 01:17:19'),
(428, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:17:37', '2023-01-10 01:17:37', '2023-01-10 01:17:37'),
(429, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:18:46', '2023-01-10 01:18:46', '2023-01-10 01:18:46'),
(430, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:21:50', '2023-01-10 01:21:50', '2023-01-10 01:21:50'),
(431, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:21:57', '2023-01-10 01:21:57', '2023-01-10 01:21:57'),
(432, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:22:40', '2023-01-10 01:22:40', '2023-01-10 01:22:40'),
(433, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:23:02', '2023-01-10 01:23:02', '2023-01-10 01:23:02'),
(434, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:28:21', '2023-01-10 01:28:21', '2023-01-10 01:28:21'),
(435, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:28:30', '2023-01-10 01:28:30', '2023-01-10 01:28:30'),
(436, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:28:42', '2023-01-10 01:28:42', '2023-01-10 01:28:42'),
(437, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:31:02', '2023-01-10 01:31:02', '2023-01-10 01:31:02'),
(438, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:31:10', '2023-01-10 01:31:10', '2023-01-10 01:31:10'),
(439, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:31:26', '2023-01-10 01:31:26', '2023-01-10 01:31:26'),
(440, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:46:38', '2023-01-10 01:46:38', '2023-01-10 01:46:38'),
(441, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:46:49', '2023-01-10 01:46:49', '2023-01-10 01:46:49'),
(442, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:50:23', '2023-01-10 01:50:23', '2023-01-10 01:50:23'),
(443, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:50:46', '2023-01-10 01:50:46', '2023-01-10 01:50:46'),
(444, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:53:28', '2023-01-10 01:53:28', '2023-01-10 01:53:28'),
(445, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:55:06', '2023-01-10 01:55:06', '2023-01-10 01:55:06'),
(446, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:55:35', '2023-01-10 01:55:35', '2023-01-10 01:55:35'),
(447, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:57:30', '2023-01-10 01:57:30', '2023-01-10 01:57:30'),
(448, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:57:38', '2023-01-10 01:57:38', '2023-01-10 01:57:38'),
(449, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:57:51', '2023-01-10 01:57:51', '2023-01-10 01:57:51'),
(450, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '15:58:51', '2023-01-10 01:58:51', '2023-01-10 01:58:51'),
(451, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '15:59:04', '2023-01-10 01:59:04', '2023-01-10 01:59:04'),
(452, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '15:59:16', '2023-01-10 01:59:16', '2023-01-10 01:59:16'),
(453, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '16:01:43', '2023-01-10 02:01:43', '2023-01-10 02:01:43'),
(454, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-10', '16:02:13', '2023-01-10 02:02:13', '2023-01-10 02:02:13'),
(455, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-10', '16:02:23', '2023-01-10 02:02:23', '2023-01-10 02:02:23'),
(456, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '16:17:05', '2023-01-10 02:17:05', '2023-01-10 02:17:05'),
(457, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '16:45:01', '2023-01-10 02:45:01', '2023-01-10 02:45:01'),
(458, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-10', '16:46:11', '2023-01-10 02:46:11', '2023-01-10 02:46:11'),
(459, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-13', '10:04:19', '2023-01-12 20:04:19', '2023-01-12 20:04:19'),
(460, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-13', '10:04:24', '2023-01-12 20:04:24', '2023-01-12 20:04:24'),
(461, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-13', '20:15:55', '2023-01-13 06:15:55', '2023-01-13 06:15:55'),
(462, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-13', '22:21:25', '2023-01-13 08:21:25', '2023-01-13 08:21:25'),
(463, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-13', '22:22:06', '2023-01-13 08:22:06', '2023-01-13 08:22:06'),
(464, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-13', '22:22:19', '2023-01-13 08:22:19', '2023-01-13 08:22:19'),
(465, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-13', '22:22:38', '2023-01-13 08:22:38', '2023-01-13 08:22:38'),
(466, 2, 'fihan2as@gmail.com', 'Presensi Masuk', 'Presensi Masuk', 'On Time', '2023-01-16', '12:45:36', '2023-01-15 22:45:36', '2023-01-15 22:45:36'),
(467, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-20', '07:10:53', '2023-01-19 17:10:53', '2023-01-19 17:10:53'),
(468, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-20', '12:31:12', '2023-01-19 22:31:12', '2023-01-19 22:31:12'),
(469, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-20', '18:23:12', '2023-01-20 04:23:12', '2023-01-20 04:23:12'),
(470, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-20', '18:25:08', '2023-01-20 04:25:08', '2023-01-20 04:25:08'),
(471, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-20', '18:29:52', '2023-01-20 04:29:52', '2023-01-20 04:29:52'),
(472, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '08:54:47', '2023-01-20 18:54:47', '2023-01-20 18:54:47'),
(473, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '08:55:04', '2023-01-20 18:55:04', '2023-01-20 18:55:04'),
(474, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '08:56:31', '2023-01-20 18:56:31', '2023-01-20 18:56:31'),
(475, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '08:57:01', '2023-01-20 18:57:01', '2023-01-20 18:57:01'),
(476, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '08:57:52', '2023-01-20 18:57:52', '2023-01-20 18:57:52'),
(477, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '08:58:00', '2023-01-20 18:58:00', '2023-01-20 18:58:00'),
(478, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '08:58:55', '2023-01-20 18:58:55', '2023-01-20 18:58:55'),
(479, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '08:59:03', '2023-01-20 18:59:03', '2023-01-20 18:59:03'),
(480, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '08:59:45', '2023-01-20 18:59:45', '2023-01-20 18:59:45'),
(481, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '08:59:52', '2023-01-20 18:59:52', '2023-01-20 18:59:52'),
(482, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:05:45', '2023-01-20 19:05:45', '2023-01-20 19:05:45'),
(483, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '09:05:52', '2023-01-20 19:05:52', '2023-01-20 19:05:52'),
(484, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:13:26', '2023-01-20 19:13:26', '2023-01-20 19:13:26'),
(485, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '09:13:43', '2023-01-20 19:13:43', '2023-01-20 19:13:43'),
(486, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:16:33', '2023-01-20 19:16:33', '2023-01-20 19:16:33'),
(487, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:22:23', '2023-01-20 19:22:23', '2023-01-20 19:22:23'),
(488, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '09:22:31', '2023-01-20 19:22:31', '2023-01-20 19:22:31'),
(489, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:27:15', '2023-01-20 19:27:15', '2023-01-20 19:27:15'),
(490, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:28:29', '2023-01-20 19:28:29', '2023-01-20 19:28:29'),
(491, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:30:28', '2023-01-20 19:30:28', '2023-01-20 19:30:28'),
(492, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '09:31:19', '2023-01-20 19:31:19', '2023-01-20 19:31:19'),
(493, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '09:35:19', '2023-01-20 19:35:19', '2023-01-20 19:35:19'),
(494, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '09:35:27', '2023-01-20 19:35:27', '2023-01-20 19:35:27'),
(495, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:10:54', '2023-01-20 20:10:54', '2023-01-20 20:10:54'),
(496, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '10:11:04', '2023-01-20 20:11:04', '2023-01-20 20:11:04'),
(497, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:14:12', '2023-01-20 20:14:12', '2023-01-20 20:14:12'),
(498, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:20:44', '2023-01-20 20:20:44', '2023-01-20 20:20:44'),
(499, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:21:13', '2023-01-20 20:21:13', '2023-01-20 20:21:13'),
(500, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:21:25', '2023-01-20 20:21:25', '2023-01-20 20:21:25'),
(501, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:31:01', '2023-01-20 20:31:01', '2023-01-20 20:31:01'),
(502, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:33:01', '2023-01-20 20:33:01', '2023-01-20 20:33:01'),
(503, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:33:58', '2023-01-20 20:33:58', '2023-01-20 20:33:58'),
(504, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:34:49', '2023-01-20 20:34:49', '2023-01-20 20:34:49'),
(505, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:39:20', '2023-01-20 20:39:20', '2023-01-20 20:39:20'),
(506, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:42:58', '2023-01-20 20:42:58', '2023-01-20 20:42:58'),
(507, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:47:51', '2023-01-20 20:47:51', '2023-01-20 20:47:51'),
(508, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:51:09', '2023-01-20 20:51:09', '2023-01-20 20:51:09'),
(509, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '10:52:25', '2023-01-20 20:52:25', '2023-01-20 20:52:25'),
(510, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '11:03:47', '2023-01-20 21:03:47', '2023-01-20 21:03:47'),
(511, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '11:06:17', '2023-01-20 21:06:17', '2023-01-20 21:06:17'),
(512, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '11:08:33', '2023-01-20 21:08:33', '2023-01-20 21:08:33'),
(513, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '11:09:07', '2023-01-20 21:09:07', '2023-01-20 21:09:07'),
(514, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '11:15:49', '2023-01-20 21:15:49', '2023-01-20 21:15:49'),
(515, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '11:16:05', '2023-01-20 21:16:05', '2023-01-20 21:16:05'),
(516, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '11:21:33', '2023-01-20 21:21:33', '2023-01-20 21:21:33'),
(517, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-21', '13:21:12', '2023-01-20 23:21:12', '2023-01-20 23:21:12'),
(518, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '17:30:42', '2023-01-21 03:30:42', '2023-01-21 03:30:42'),
(519, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-21', '17:42:10', '2023-01-21 03:42:10', '2023-01-21 03:42:10'),
(520, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-21', '17:44:37', '2023-01-21 03:44:37', '2023-01-21 03:44:37'),
(521, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-21', '17:51:33', '2023-01-21 03:51:33', '2023-01-21 03:51:33'),
(522, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-22', '18:27:56', '2023-01-22 04:27:56', '2023-01-22 04:27:56'),
(523, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-22', '18:28:58', '2023-01-22 04:28:58', '2023-01-22 04:28:58'),
(524, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-22', '18:34:54', '2023-01-22 04:34:54', '2023-01-22 04:34:54'),
(525, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '05:07:57', '2023-01-22 15:07:57', '2023-01-22 15:07:57'),
(526, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '05:38:54', '2023-01-22 15:38:54', '2023-01-22 15:38:54'),
(527, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '05:40:30', '2023-01-22 15:40:30', '2023-01-22 15:40:30'),
(528, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '05:40:38', '2023-01-22 15:40:38', '2023-01-22 15:40:38'),
(529, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '05:41:28', '2023-01-22 15:41:28', '2023-01-22 15:41:28'),
(530, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '05:41:36', '2023-01-22 15:41:36', '2023-01-22 15:41:36'),
(531, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '05:44:06', '2023-01-22 15:44:06', '2023-01-22 15:44:06'),
(532, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '05:44:15', '2023-01-22 15:44:15', '2023-01-22 15:44:15'),
(533, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '05:47:23', '2023-01-22 15:47:23', '2023-01-22 15:47:23'),
(534, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '05:47:33', '2023-01-22 15:47:33', '2023-01-22 15:47:33'),
(535, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '06:04:23', '2023-01-22 16:04:23', '2023-01-22 16:04:23'),
(536, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Ditolak', '2023-01-23', '06:05:01', '2023-01-22 16:05:01', '2023-01-22 16:05:01'),
(537, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Ditolak', '2023-01-23', '06:05:23', '2023-01-22 16:05:23', '2023-01-22 16:05:23'),
(538, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '06:05:28', '2023-01-22 16:05:28', '2023-01-22 16:05:28'),
(539, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '06:10:31', '2023-01-22 16:10:31', '2023-01-22 16:10:31'),
(540, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Ditolak', '2023-01-23', '06:11:47', '2023-01-22 16:11:47', '2023-01-22 16:11:47'),
(541, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Diterima', '2023-01-23', '06:11:52', '2023-01-22 16:11:52', '2023-01-22 16:11:52'),
(542, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '06:43:49', '2023-01-22 16:43:49', '2023-01-22 16:43:49'),
(543, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '06:44:29', '2023-01-22 16:44:29', '2023-01-22 16:44:29'),
(544, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '06:45:49', '2023-01-22 16:45:49', '2023-01-22 16:45:49'),
(545, 2, 'fihan2as@gmail.com', 'Lembur', 'Pengajuan Lembur', 'Menunggu Disetujui', '2023-01-23', '06:46:15', '2023-01-22 16:46:15', '2023-01-22 16:46:15'),
(546, 2, 'fihan2as@gmail.com', 'Lembur', 'Approvement Lembur', 'Ditolak', '2023-01-23', '06:46:25', '2023-01-22 16:46:25', '2023-01-22 16:46:25'),
(547, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '06:46:44', '2023-01-22 16:46:44', '2023-01-22 16:46:44'),
(548, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '07:06:08', '2023-01-22 17:06:08', '2023-01-22 17:06:08'),
(549, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '07:24:32', '2023-01-22 17:24:32', '2023-01-22 17:24:32'),
(550, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-23', '07:24:48', '2023-01-22 17:24:48', '2023-01-22 17:24:48'),
(551, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-23', '07:27:47', '2023-01-22 17:27:47', '2023-01-22 17:27:47'),
(552, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '07:30:01', '2023-01-22 17:30:01', '2023-01-22 17:30:01'),
(553, 2, 'fihan2as@gmail.com', 'Penggajin', 'Pengajuan Pengambilan Gaji ', 'Menunggu Cair', '2023-01-23', '07:48:36', '2023-01-22 17:48:36', '2023-01-22 17:48:36'),
(554, 2, 'fihan2as@gmail.com', 'Penggajian', 'Pencairan Gaji ', 'Berhasil Cair', '2023-01-23', '07:49:14', '2023-01-22 17:49:14', '2023-01-22 17:49:14'),
(555, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '08:07:12', '2023-01-22 18:07:12', '2023-01-22 18:07:12'),
(556, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '08:08:16', '2023-01-22 18:08:16', '2023-01-22 18:08:16'),
(557, 2, 'fihan2as@gmail.com', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '16:27:45', '2023-01-23 02:27:45', '2023-01-23 02:27:45'),
(558, 2, 'fihan2as@student.ub.ac.id', 'Penggajian', 'Gaji Anda Telah Dibuat', 'Belum Diambil', '2023-01-23', '16:31:44', '2023-01-23 02:31:44', '2023-01-23 02:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `penggajian`
--

CREATE TABLE `penggajian` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `id_tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `id_bonus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `id_potongan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `total_tunjangan` int(11) DEFAULT NULL,
  `total_bonus` int(11) DEFAULT NULL,
  `total_potongan` int(11) DEFAULT NULL,
  `potong_absen` int(11) DEFAULT NULL,
  `total_lembur` int(11) DEFAULT NULL,
  `total_gaji` int(11) DEFAULT NULL,
  `tidak_masuk` int(11) DEFAULT NULL,
  `jamlembur_total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Diambil',
  `no_transaksi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penggajian`
--

INSERT INTO `penggajian` (`id`, `id_admin`, `email`, `id_jabatan`, `id_golongan`, `id_tunjangan`, `id_bonus`, `id_potongan`, `total_tunjangan`, `total_bonus`, `total_potongan`, `potong_absen`, `total_lembur`, `total_gaji`, `tidak_masuk`, `jamlembur_total`, `tanggal`, `status`, `no_transaksi`, `created_at`, `updated_at`) VALUES
(5, 2, 'fihan2as@gmail.com', 2, NULL, '3', '4', '1', NULL, NULL, NULL, 2800000, 260116, NULL, 28, 8, '2023-01-23', 'Belum Cair', '62419957', '2023-01-22 17:06:08', '2023-01-22 17:06:08'),
(6, 2, 'fihan2as@gmail.com', 2, NULL, '2,3', '6,5', '1', NULL, NULL, NULL, 2800000, 260116, NULL, 28, 8, '2023-01-23', 'Belum Cair', '26943300', '2023-01-22 17:24:32', '2023-01-22 17:24:32'),
(7, 2, 'fihan2as@gmail.com', 2, NULL, '3', '6', '1', NULL, NULL, NULL, 2800000, 260116, NULL, 28, 8, '2023-01-23', 'Belum Cair', '81157090', '2023-01-22 17:30:01', '2023-01-22 17:30:01'),
(8, 2, 'fihan2as@student.ub.ac.id', 4, NULL, '1', '4', '1', NULL, NULL, NULL, 4500000, 0, NULL, 30, 0, '2023-01-23', 'Belum Diambil', '46453566', '2023-01-22 18:07:12', '2023-01-22 18:07:12'),
(9, 2, 'fihan2as@gmail.com', 2, NULL, '1', '4', '1', NULL, NULL, NULL, 2800000, 260116, NULL, 28, 8, '2023-01-23', 'Belum Diambil', '15236474', '2023-01-22 18:08:16', '2023-01-22 18:08:16'),
(10, 2, 'fihan2as@gmail.com', 2, NULL, '3,2', '6', '2', NULL, NULL, NULL, 2800000, 260116, NULL, 28, 8, '2023-01-23', 'Belum Diambil', '45865361', '2023-01-23 02:27:45', '2023-01-23 02:27:45'),
(11, 2, 'fihan2as@student.ub.ac.id', 4, NULL, '1', '6', '2', NULL, NULL, NULL, 4500000, 0, NULL, 30, 0, '2023-01-23', 'Belum Diambil', '13271378', '2023-01-23 02:31:44', '2023-01-23 02:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pengumuman` date NOT NULL,
  `tanggal_dibuat` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `id_admin`, `judul`, `isi`, `tanggal_pengumuman`, `tanggal_dibuat`, `created_at`, `updated_at`) VALUES
(2, 2, 'Presentasi dengan client', 'Siapkan bahan-bahan yang telah kita bahas minggu lalu', '2023-01-05', '2022-12-31', '2022-12-31 00:47:20', '2022-12-31 00:47:20'),
(3, 2, 'Meeting', 'Meeting mingguan di lantai 3', '2023-01-03', '2022-12-31', '2022-12-31 04:28:28', '2022-12-31 04:28:28'),
(4, 2, 'Presentasi Project', 'Siapkan Semaksimal Mungkin', '2023-01-11', '2023-01-02', '2023-01-01 20:45:51', '2023-01-01 20:45:51'),
(5, 2, 'Testing Apps', 'QA dan Tester harap bisa melakukan yang terbaik', '2023-01-28', '2023-01-02', '2023-01-01 20:51:12', '2023-01-01 20:51:12'),
(6, 2, 'Remote Work', 'Hari ini bekerja secara remote, harap siapkan internet yang lancar dan memadai', '2023-01-04', '2023-01-02', '2023-01-01 20:51:28', '2023-01-01 20:51:28'),
(7, 2, 'Presentasi Project', 'Client dari Perusahan A', '2023-01-30', '2023-01-23', '2023-01-22 19:49:23', '2023-01-22 19:49:23'),
(8, 2, 'Meeting', 'Akan ada sesi tanya jawab, siapkan pertanyaan sebanyak mungkin', '2023-01-24', '2023-01-23', '2023-01-22 19:50:25', '2023-01-22 19:50:25'),
(9, 2, 'MOU', 'Kesepakatan dengan client baru', '2023-01-23', '2023-01-23', '2023-01-22 19:54:12', '2023-01-22 19:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expired_at`, `created_at`, `updated_at`) VALUES
(361, 'App\\Models\\AkunPegawai', 1, 'auth_token', '6d047cfe4fe15f8a2fc0ca203036ff8ca79433247091d68e43f493808e08c04c', '[\"*\"]', '2022-12-29 03:13:54', '2022-12-29 03:14:54', '2022-12-29 03:13:54', '2022-12-29 03:13:54'),
(362, 'App\\Models\\User', 2, 'auth_token', 'b49561a01aa72feb5eed0caf5f7424e4ced94ed3e04eb931be4f776c37aba3ec', '[\"*\"]', NULL, '2022-12-29 03:17:10', '2022-12-29 03:16:10', '2022-12-29 03:16:10'),
(363, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'ee32d1f6109c49eaff445a5a7e4a6f60fa71a393e9d5693e1b2575bdef9e10a4', '[\"*\"]', '2022-12-29 03:20:45', '2022-12-29 04:20:45', '2022-12-29 03:20:45', '2022-12-29 03:20:45'),
(364, 'App\\Models\\User', 2, 'auth_token', '3dddfa1b9b8cfb2b54f7c90bdd9dededd89dcdff0cf3a060fe2ef5b90aaed287', '[\"*\"]', NULL, '2022-12-29 06:12:53', '2022-12-29 05:12:53', '2022-12-29 05:12:53'),
(365, 'App\\Models\\User', 2, 'auth_token', '29708e10ad9af2dc3b4bb2184a98870aff8c1330ab28bd415ec72fa0640d3198', '[\"*\"]', NULL, '2022-12-29 06:15:24', '2022-12-29 05:15:24', '2022-12-29 05:15:24'),
(366, 'App\\Models\\User', 2, 'auth_token', '4a80031601483afabc782c4d08f410ee4cd28033ce3ce9fefea1fba212de4ac8', '[\"*\"]', NULL, '2022-12-29 05:21:41', '2022-12-29 05:20:41', '2022-12-29 05:20:41'),
(367, 'App\\Models\\User', 2, 'auth_token', 'd5629bc5dac73737c401dfaf301758b62c1feba1ca37019842755ea1ba0925a5', '[\"*\"]', NULL, '2022-12-29 05:35:23', '2022-12-29 05:35:23', '2022-12-29 05:35:23'),
(368, 'App\\Models\\User', 2, 'auth_token', 'cc6a33aa8c122a4619f77f1ceb3bf1b26c262bc44cabe1babfe2bcd1f35c20b3', '[\"*\"]', '2022-12-29 05:40:23', '2022-12-29 05:40:27', '2022-12-29 05:40:23', '2022-12-29 05:40:23'),
(369, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'ed68195ea92377800670034e841263b0bddd240ef7860af5aca79e982af51253', '[\"*\"]', '2022-12-29 05:40:45', '2022-12-29 05:40:45', '2022-12-29 05:40:45', '2022-12-29 05:40:45'),
(370, 'App\\Models\\User', 2, 'auth_token', '6b11d365e9c14f27f94e64643f2ed2f71d31d58b3100df358f9809cb47abb6ff', '[\"*\"]', NULL, '2022-12-29 06:46:05', '2022-12-29 05:46:05', '2022-12-29 05:46:05'),
(371, 'App\\Models\\User', 2, 'auth_token', '8c815e42444f942cc11d3d3dfc1138bbc1789ad01d782dea2bd3806093c05e28', '[\"*\"]', '2022-12-29 07:41:02', '2022-12-29 07:47:24', '2022-12-29 06:47:24', '2022-12-29 07:41:02'),
(372, 'App\\Models\\User', 2, 'auth_token', 'fc02cf8a2f805ef23dfbe5e95c1386634d0e770686dc2d268852e63450091323', '[\"*\"]', NULL, '2022-12-29 06:49:00', '2022-12-29 05:49:00', '2022-12-29 05:49:00'),
(373, 'App\\Models\\User', 2, 'auth_token', '192095cc3de641d6e88f94340d3bcc099048be35ce2da8b4aa8b41c05002a8d9', '[\"*\"]', '2022-12-29 18:51:26', '2022-12-29 19:36:02', '2022-12-29 18:36:02', '2022-12-29 18:51:26'),
(374, 'App\\Models\\User', 2, 'auth_token', '2f1683193695861936628c12722c4376ea4e2e2732cf17dd5689bb8567165daa', '[\"*\"]', NULL, '2022-12-29 18:53:37', '2022-12-29 18:51:37', '2022-12-29 18:51:37'),
(375, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'd6fa1d6830b4da63a23da3a79b6ca328c38c50291e3a72b2d89d3cf09837a73e', '[\"*\"]', NULL, '2022-12-29 18:57:01', '2022-12-29 18:55:01', '2022-12-29 18:55:01'),
(376, 'App\\Models\\User', 2, 'auth_token', '57d9c3aab878b3adafe7f12e7b7e7b8e8a418173276d81f7e34ca966a36eb47f', '[\"*\"]', NULL, '2022-12-29 19:04:00', '2022-12-29 19:02:00', '2022-12-29 19:02:00'),
(377, 'App\\Models\\User', 2, 'auth_token', '0d35e767de78c798ec7e7673714adcc975e21452f36bd0b333952c4d4a0949b0', '[\"*\"]', NULL, '2022-12-29 19:09:44', '2022-12-29 19:07:44', '2022-12-29 19:07:44'),
(378, 'App\\Models\\User', 2, 'auth_token', '3af9d76e86d95e93e289908bf8f6a460f64b30efdfed30dd1f9d8fb13d056d8a', '[\"*\"]', NULL, '2022-12-29 19:12:33', '2022-12-29 19:10:33', '2022-12-29 19:10:33'),
(379, 'App\\Models\\User', 2, 'auth_token', '281f364b8275a35dd80acb4299f619db79915eb078c5df3fdbf64e66b58bee91', '[\"*\"]', NULL, '2022-12-29 19:16:06', '2022-12-29 19:14:06', '2022-12-29 19:14:06'),
(380, 'App\\Models\\User', 2, 'auth_token', '89c5cd3394bdbfc32fd559576c6fcece8ea9bf27dfbe074a33c73d93257dd41a', '[\"*\"]', '2022-12-29 20:13:09', '2022-12-29 20:37:19', '2022-12-29 19:37:19', '2022-12-29 20:13:09'),
(381, 'App\\Models\\AkunPegawai', 1, 'auth_token', '11e385b40b7bb49fd8b6450adb8ce647ea418ce535bef25961b62ba5c25f6293', '[\"*\"]', NULL, '2022-12-29 20:38:27', '2022-12-29 19:38:27', '2022-12-29 19:38:27'),
(382, 'App\\Models\\AkunPegawai', 1, 'auth_token', '1221e7e8da2871220c1029f40fd7ff296687c8cfca66e70266952bcd79eec2d4', '[\"*\"]', NULL, '2022-12-29 23:32:45', '2022-12-29 22:32:45', '2022-12-29 22:32:45'),
(383, 'App\\Models\\AkunPegawai', 1, 'auth_token', '969b1a71c4345d63d6f6d0fc6563691b504ee3d072f8d7b4befa21464e2498ce', '[\"*\"]', NULL, '2022-12-29 23:33:39', '2022-12-29 22:33:39', '2022-12-29 22:33:39'),
(384, 'App\\Models\\AkunPegawai', 1, 'auth_token', '9973b2bcc736648bef271391c17a946371dba6b2ea158d1cc8a0f24caa727894', '[\"*\"]', NULL, '2022-12-30 00:21:38', '2022-12-29 23:21:38', '2022-12-29 23:21:38'),
(385, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'd60d4704041f15243dff91cac1c51edeeea0bf6d6e099d1c7fc0edec6daf6fd1', '[\"*\"]', NULL, '2022-12-30 00:22:31', '2022-12-29 23:22:31', '2022-12-29 23:22:31'),
(386, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'a9a0993cba6d1beacd0267be8f78e1dca9204a015d2d65b5868dff84e5f910c3', '[\"*\"]', NULL, '2022-12-30 00:22:51', '2022-12-29 23:22:51', '2022-12-29 23:22:51'),
(387, 'App\\Models\\AkunPegawai', 1, 'auth_token', '2a99d8e9c61efed433844867696245d626c9e72f1e84c8f2a15e5963b6bcba2d', '[\"*\"]', NULL, '2022-12-30 00:24:42', '2022-12-29 23:24:42', '2022-12-29 23:24:42'),
(388, 'App\\Models\\User', 2, 'auth_token', '4cb595cc109fb8fe1755589fb2177de1e158e047235c8f04f75d71b66d65d6ce', '[\"*\"]', '2022-12-30 05:57:29', '2022-12-30 06:08:05', '2022-12-30 05:08:05', '2022-12-30 05:57:29'),
(389, 'App\\Models\\AkunPegawai', 3, 'auth_token', '211e7c93e385c5f49eeeeccaa0437e24dbe18bad56f7f0dbeb9ee6ed4c7a7d1e', '[\"*\"]', NULL, '2022-12-30 06:46:37', '2022-12-30 05:46:37', '2022-12-30 05:46:37'),
(390, 'App\\Models\\User', 2, 'auth_token', '93a6da62850648f0bcf7793b226bae18f7f7f9b4e829b227d4a83bacd7a8da1e', '[\"*\"]', '2022-12-30 14:48:45', '2022-12-30 15:24:49', '2022-12-30 14:24:49', '2022-12-30 14:48:45'),
(391, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'db70e93dd4c1e3d740e0923a250873b9a000926a62045df5492d233dfba70f55', '[\"*\"]', NULL, '2022-12-30 15:25:41', '2022-12-30 14:25:41', '2022-12-30 14:25:41'),
(392, 'App\\Models\\User', 2, 'auth_token', '7307b7f0abeb1268bc8d847000f26fc5d6b4b7c445379d2a160308ce48bb70dd', '[\"*\"]', '2022-12-30 20:44:46', '2022-12-30 20:44:51', '2022-12-30 19:44:51', '2022-12-30 20:44:46'),
(393, 'App\\Models\\User', 2, 'auth_token', 'f4ea59220afc30b9dd7f5da464e65111a32ec464f3607054ad950d266df509a5', '[\"*\"]', '2022-12-30 23:49:33', '2022-12-30 23:51:01', '2022-12-30 22:51:01', '2022-12-30 23:49:33'),
(394, 'App\\Models\\User', 2, 'auth_token', '51cc1a23ada11b4a0813910ee11b7916812e464f5a10b517a1aa7dcd942e0729', '[\"*\"]', NULL, '2022-12-31 00:56:23', '2022-12-30 23:56:23', '2022-12-30 23:56:23'),
(395, 'App\\Models\\AkunPegawai', 1, 'auth_token', '9f3b1c46c0a31aae2de32e08c646f3c5295e24c9cbcdfb66c9c49d09789fb5ed', '[\"*\"]', NULL, '2022-12-31 01:47:52', '2022-12-31 00:47:52', '2022-12-31 00:47:52'),
(396, 'App\\Models\\User', 2, 'auth_token', '1f5d44059fe2507f883b7acdc143866c485620dff09470d240c31796f8884c3c', '[\"*\"]', '2022-12-31 03:13:12', '2022-12-31 03:35:58', '2022-12-31 02:35:58', '2022-12-31 03:13:12'),
(397, 'App\\Models\\AkunPegawai', 1, 'auth_token', '4e14ab633d32fef0b4ab0de8c6376721768d8a2c269975ea0f497486774779bb', '[\"*\"]', NULL, '2022-12-31 04:13:26', '2022-12-31 03:13:26', '2022-12-31 03:13:26'),
(398, 'App\\Models\\AkunPegawai', 1, 'auth_token', '71568d7480a678230dbd0f5df40b348db6225e0a5a03b0514fa63a5d4d5d649a', '[\"*\"]', NULL, '2022-12-31 05:24:16', '2022-12-31 04:24:16', '2022-12-31 04:24:16'),
(399, 'App\\Models\\User', 2, 'auth_token', '64f48e1cae6b2fbc3540c990acf301f2428f7a5a8914e6354fc94476bd1e6f68', '[\"*\"]', NULL, '2022-12-31 05:27:03', '2022-12-31 04:27:03', '2022-12-31 04:27:03'),
(400, 'App\\Models\\User', 2, 'auth_token', '5fbcc2d5a76cf347e52a847196ab858e4556473779818d85dd0947e75214f084', '[\"*\"]', '2022-12-31 07:47:34', '2022-12-31 08:25:03', '2022-12-31 07:25:03', '2022-12-31 07:47:34'),
(401, 'App\\Models\\AkunPegawai', 1, 'auth_token', '059a2cf169e3ad2d4bd635afac6bf2cbc34ecc44ea70b712028430e35d54510a', '[\"*\"]', NULL, '2022-12-31 08:25:18', '2022-12-31 07:25:18', '2022-12-31 07:25:18'),
(402, 'App\\Models\\User', 2, 'auth_token', '06cc7a98d124d2c2870b54d8e781a6d6381c145a167b993bfcc1e704ca0840fc', '[\"*\"]', NULL, '2022-12-31 22:46:44', '2022-12-31 21:46:44', '2022-12-31 21:46:44'),
(403, 'App\\Models\\User', 2, 'auth_token', 'b9feaa05a1a0133c52f8a565a30ea331254e2536cda60f954453d119ba65b0ea', '[\"*\"]', NULL, '2023-01-01 15:59:14', '2023-01-01 14:59:14', '2023-01-01 14:59:14'),
(404, 'App\\Models\\User', 2, 'auth_token', 'ec178242bfb3afd54d343b55139739ab8cf62e9911e1840eaeca987d81921d19', '[\"*\"]', NULL, '2023-01-01 18:57:14', '2023-01-01 17:57:14', '2023-01-01 17:57:14'),
(405, 'App\\Models\\AkunPegawai', 1, 'auth_token', '37da601b49f38bd8b5b0c904aa2591e9a2e65447d0e764f68cd35d828e3ef1d1', '[\"*\"]', NULL, '2023-01-01 19:51:36', '2023-01-01 18:51:36', '2023-01-01 18:51:36'),
(406, 'App\\Models\\User', 2, 'auth_token', '5f6c4ad5fe266bb339cbdf85f633c7140a78fe0f167a01db1ef379f97724e7e4', '[\"*\"]', NULL, '2023-01-01 21:44:49', '2023-01-01 20:44:49', '2023-01-01 20:44:49'),
(407, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'd3543eb6c84b84519185ca49e6ad9a95e8c1817afc3f89cee1fc47178efd056f', '[\"*\"]', NULL, '2023-01-01 21:45:15', '2023-01-01 20:45:15', '2023-01-01 20:45:15'),
(408, 'App\\Models\\User', 2, 'auth_token', '044390a3aee4e1608ab17e58931d18d445b8276ba788a66eb608671a512e783a', '[\"*\"]', NULL, '2023-01-02 07:18:35', '2023-01-02 06:18:35', '2023-01-02 06:18:35'),
(409, 'App\\Models\\AkunPegawai', 1, 'auth_token', '0ffe132c364823071f2eb432105aec3e6292a33b16a7bc208f615471848826eb', '[\"*\"]', NULL, '2023-01-02 07:18:46', '2023-01-02 06:18:46', '2023-01-02 06:18:46'),
(410, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'ca743fb021ade771e7a8ca164d86bd89691048a84655fbca65f32e78d0427c71', '[\"*\"]', NULL, '2023-01-02 15:34:27', '2023-01-02 14:34:27', '2023-01-02 14:34:27'),
(411, 'App\\Models\\AkunPegawai', 1, 'auth_token', '08cf526fc37677ab027d2aa5fcf0879407093b400842e1adb700840347ec1241', '[\"*\"]', NULL, '2023-01-02 16:50:58', '2023-01-02 15:50:58', '2023-01-02 15:50:58'),
(412, 'App\\Models\\AkunPegawai', 1, 'auth_token', '28d4ffd8e5d1ae1da3d449fe34d9e6a128956ca8a32735960f6841be2e5b6cff', '[\"*\"]', '2023-01-02 16:12:57', '2023-01-02 17:08:16', '2023-01-02 16:08:16', '2023-01-02 16:12:57'),
(413, 'App\\Models\\User', 2, 'auth_token', '1f36070d0ca82bc045fdc9fc753a0a676672a5d78d7a6ea9507c0d2b09d01acd', '[\"*\"]', '2023-01-02 19:39:27', '2023-01-02 20:12:53', '2023-01-02 19:12:53', '2023-01-02 19:39:27'),
(414, 'App\\Models\\User', 2, 'auth_token', 'c45181a7fdc63c76163881cebea4e42d33a3d18980a8d1da2df27fa556fc8f73', '[\"*\"]', '2023-01-02 21:52:16', '2023-01-02 21:52:51', '2023-01-02 20:52:51', '2023-01-02 21:52:16'),
(415, 'App\\Models\\AkunPegawai', 1, 'auth_token', '738c1548c1d71b4a97c005ded115e30bedd1d509954da7513842e554eadea284', '[\"*\"]', NULL, '2023-01-02 22:28:12', '2023-01-02 21:28:12', '2023-01-02 21:28:12'),
(416, 'App\\Models\\User', 2, 'auth_token', 'faf27c568eb0f83ce86ea99afd4fea855d2273d4c8fe1a9481552a824751d225', '[\"*\"]', '2023-01-02 22:45:44', '2023-01-02 23:01:51', '2023-01-02 22:01:51', '2023-01-02 22:45:44'),
(417, 'App\\Models\\User', 2, 'auth_token', '11dca0efb12060e149b3a44db66ffd24989d7acb746fc7a727b32338aabeebfb', '[\"*\"]', '2023-01-03 01:36:19', '2023-01-03 01:55:08', '2023-01-03 00:55:08', '2023-01-03 01:36:19'),
(418, 'App\\Models\\AkunPegawai', 1, 'auth_token', '5597ddbf120e34e7786509626de909e6e2b8c6e86bf3611346a44c70becf0500', '[\"*\"]', NULL, '2023-01-03 02:07:18', '2023-01-03 01:07:18', '2023-01-03 01:07:18'),
(419, 'App\\Models\\User', 2, 'auth_token', 'd1c58b95e6852db11deb0b3b77a239bd545345fdba750b5f9401e8014a76f5a3', '[\"*\"]', NULL, '2023-01-03 02:36:33', '2023-01-03 01:36:33', '2023-01-03 01:36:33'),
(420, 'App\\Models\\User', 2, 'auth_token', '55de2f45f9ed71d88bbf47f34ad3e6aff320c9952e3d2be7b6aabb82c1132a8c', '[\"*\"]', '2023-01-03 06:26:39', '2023-01-03 06:46:30', '2023-01-03 05:46:30', '2023-01-03 06:26:39'),
(421, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'b43978c222d9a7a9fdb23024d4b05a1a1fafd50a1d8b30ba8175f358cb8eb7d3', '[\"*\"]', NULL, '2023-01-03 06:53:28', '2023-01-03 05:53:28', '2023-01-03 05:53:28'),
(422, 'App\\Models\\User', 2, 'auth_token', '3b1752404c0e178b9a7589ef282ad8b109f267f43d097fa4b6f625494a95c697', '[\"*\"]', NULL, '2023-01-03 07:24:06', '2023-01-03 06:24:06', '2023-01-03 06:24:06'),
(423, 'App\\Models\\User', 2, 'auth_token', 'ab08bcea189f9de6927e5ad8086d236111fbc39d006f7b1f05988a106b0a508a', '[\"*\"]', NULL, '2023-01-03 15:36:18', '2023-01-03 14:36:18', '2023-01-03 14:36:18'),
(424, 'App\\Models\\AkunPegawai', 1, 'auth_token', '8871e9c5afed9ec97a175697e3e17760dcb4fe0581c95668a1d002416bf79879', '[\"*\"]', NULL, '2023-01-03 15:36:37', '2023-01-03 14:36:37', '2023-01-03 14:36:37'),
(425, 'App\\Models\\AkunPegawai', 1, 'auth_token', '07cc3d7c5581b0c240bd83ad439383fcb242e2e25393ec8de9f20bdc99f55ab9', '[\"*\"]', NULL, '2023-01-03 18:39:09', '2023-01-03 17:39:09', '2023-01-03 17:39:09'),
(426, 'App\\Models\\User', 2, 'auth_token', '60c4f6c630009b173fd32b6f4f7f986cf3f32615a377fdc6b8caab00ed560887', '[\"*\"]', NULL, '2023-01-03 18:39:19', '2023-01-03 17:39:19', '2023-01-03 17:39:19'),
(427, 'App\\Models\\AkunPegawai', 1, 'auth_token', '886f1ffe44a3583e9e5c2ea41b81eea245b610fd7b476103d7c5c94876c138db', '[\"*\"]', NULL, '2023-01-03 18:42:55', '2023-01-03 17:42:55', '2023-01-03 17:42:55'),
(428, 'App\\Models\\User', 2, 'auth_token', '72b07c694d70ce71c3af6ee382562dece5831f6b46161c0603d7337ec4d28b89', '[\"*\"]', NULL, '2023-01-03 19:25:22', '2023-01-03 18:25:22', '2023-01-03 18:25:22'),
(429, 'App\\Models\\User', 2, 'auth_token', '4801f80f9afa38375b249b17accb8fef196da2816a04bbf69fb0c3a4fec8b474', '[\"*\"]', NULL, '2023-01-03 19:27:19', '2023-01-03 18:27:19', '2023-01-03 18:27:19'),
(430, 'App\\Models\\AkunPegawai', 1, 'auth_token', '80e8bdc6303216e09ce8b9ac33385b0fa6b72a790ab8c00008dc82a1cce8a462', '[\"*\"]', NULL, '2023-01-03 19:33:26', '2023-01-03 18:33:26', '2023-01-03 18:33:26'),
(431, 'App\\Models\\AkunPegawai', 3, 'auth_token', '42d4fe0f9fb3433614c389fc6cd099a41df563e4e6a4bd881f2df1ecb82c5cb2', '[\"*\"]', NULL, '2023-01-03 19:34:23', '2023-01-03 18:34:23', '2023-01-03 18:34:23'),
(432, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'c8510db4d76a3aa191436987a987351dc20a12b07a22a6a549df2432be71c87b', '[\"*\"]', NULL, '2023-01-03 20:14:34', '2023-01-03 19:14:34', '2023-01-03 19:14:34'),
(433, 'App\\Models\\AkunPegawai', 3, 'auth_token', '18b43c0bb907024c151b02870449b6fe054f27f386410c97d373e7db7fedd6c9', '[\"*\"]', NULL, '2023-01-03 23:23:39', '2023-01-03 22:23:39', '2023-01-03 22:23:39'),
(434, 'App\\Models\\User', 2, 'auth_token', '5c8e42cc06907f6da692b661e27b82aa4dc5c1ca5582af05b2aa225953cfde0a', '[\"*\"]', NULL, '2023-01-03 23:25:11', '2023-01-03 22:25:11', '2023-01-03 22:25:11'),
(435, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'f6e016e2721d0c074b7380c060a1b88f6dc3578715844e89378afa728e4acc1c', '[\"*\"]', NULL, '2023-01-04 01:48:31', '2023-01-04 00:48:31', '2023-01-04 00:48:31'),
(436, 'App\\Models\\User', 2, 'auth_token', 'f862df680302c507bada1429d9bd53d191d06a021ffa0bd1179c3e475199e2c9', '[\"*\"]', '2023-01-04 01:22:35', '2023-01-04 01:49:03', '2023-01-04 00:49:03', '2023-01-04 01:22:35'),
(437, 'App\\Models\\User', 2, 'auth_token', 'e128e464cbbed05d4d99c0ee33fad9eb56f05faefabdadd02eee3ce0d73c334d', '[\"*\"]', NULL, '2023-01-04 02:49:57', '2023-01-04 01:49:57', '2023-01-04 01:49:57'),
(438, 'App\\Models\\User', 2, 'auth_token', 'f73461dea6033be3fc26a8405e00cf557b3d8c2f13ff59da5a3edfa8b4ab1e83', '[\"*\"]', '2023-01-04 03:57:19', '2023-01-04 03:57:24', '2023-01-04 02:57:24', '2023-01-04 03:57:19'),
(439, 'App\\Models\\User', 2, 'auth_token', 'ac2aca93c237845ea490d60d7b2105ca013db98649d8f2e75fe62c69282b1024', '[\"*\"]', NULL, '2023-01-04 04:57:53', '2023-01-04 03:57:53', '2023-01-04 03:57:53'),
(440, 'App\\Models\\User', 2, 'auth_token', '3964672e13653b495d62d346ec2d29ef4d2362ff725e4118083754e8b831daca', '[\"*\"]', NULL, '2023-01-04 06:50:25', '2023-01-04 05:50:25', '2023-01-04 05:50:25'),
(441, 'App\\Models\\User', 2, 'auth_token', 'dd0ea25995e024af8b1e4d7dfe5d9044f6ac070f5fcbf65c9758c66c31383d42', '[\"*\"]', NULL, '2023-01-04 08:21:10', '2023-01-04 07:21:10', '2023-01-04 07:21:10'),
(442, 'App\\Models\\User', 2, 'auth_token', '2a3075e1d0ba26a299a502474edda79992f4d7043830a9e11171a66e8b31fe1c', '[\"*\"]', NULL, '2023-01-06 03:42:41', '2023-01-06 02:42:41', '2023-01-06 02:42:41'),
(443, 'App\\Models\\User', 2, 'auth_token', '384c9ff2f737053b4a8d68aca3d4044c1ea64131e997c0454e52b710b152cf5c', '[\"*\"]', NULL, '2023-01-06 05:20:18', '2023-01-06 04:20:18', '2023-01-06 04:20:18'),
(444, 'App\\Models\\User', 2, 'auth_token', 'cfd705f7e526a102822f5f8ee07eb4f29f0cbc20e67af8e9f96040ca5905878e', '[\"*\"]', NULL, '2023-01-06 08:26:45', '2023-01-06 07:26:45', '2023-01-06 07:26:45'),
(445, 'App\\Models\\User', 2, 'auth_token', '58aecc152703a5cd45c856d6614f85e79ad98c9f655383b6c3ea8e1c422f5a29', '[\"*\"]', NULL, '2023-01-06 15:30:42', '2023-01-06 14:30:42', '2023-01-06 14:30:42'),
(446, 'App\\Models\\User', 2, 'auth_token', '374643f5dda881b1daef922ee5cc0893cb7c1c7c77192ad3ec834ef56316261d', '[\"*\"]', '2023-01-07 04:54:28', '2023-01-07 05:05:45', '2023-01-07 04:05:45', '2023-01-07 04:54:28'),
(447, 'App\\Models\\User', 2, 'auth_token', 'e1a7ea1796dc7bfc0bffb993ba13d982bf0f65c6ae7684ae915d4374ea5d2a8a', '[\"*\"]', '2023-01-07 06:12:24', '2023-01-07 06:12:31', '2023-01-07 05:12:31', '2023-01-07 06:12:24'),
(448, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'ef7672abf9b059e7ad22a57e50497b9aad797c8d7640448d5d78e30b1299aaf9', '[\"*\"]', NULL, '2023-01-07 06:41:39', '2023-01-07 05:41:39', '2023-01-07 05:41:39'),
(449, 'App\\Models\\AkunPegawai', 3, 'auth_token', '1d2e372b1114c657a15db5004c8675ecfde69508c359c2a59ad7199413696c08', '[\"*\"]', NULL, '2023-01-07 06:44:17', '2023-01-07 05:44:17', '2023-01-07 05:44:17'),
(450, 'App\\Models\\User', 2, 'auth_token', '36bd1fa714263d95bb76ef00ea58e56a8e0f832e153656ff05b32d170e7ed6e3', '[\"*\"]', '2023-01-07 07:10:31', '2023-01-07 07:13:16', '2023-01-07 06:13:16', '2023-01-07 07:10:31'),
(451, 'App\\Models\\User', 2, 'auth_token', '6e73d873fccc8a29df8ec8da399bcea19633a6f68d84aeec44ecd7c9998957d3', '[\"*\"]', NULL, '2023-01-07 08:18:10', '2023-01-07 07:18:10', '2023-01-07 07:18:10'),
(452, 'App\\Models\\User', 2, 'auth_token', 'ccb7586f95a930d0da2efe8d3338bf473289d2f1b0a7dcf125d30993c2416209', '[\"*\"]', NULL, '2023-01-07 17:44:43', '2023-01-07 16:44:43', '2023-01-07 16:44:43'),
(453, 'App\\Models\\User', 2, 'auth_token', 'ce250ec0496ece2466eff4f59f252fbf0205f174781de1769f5c7a440ccc192a', '[\"*\"]', '2023-01-07 19:00:18', '2023-01-07 19:07:47', '2023-01-07 18:07:47', '2023-01-07 19:00:18'),
(454, 'App\\Models\\User', 2, 'auth_token', 'a6dd7027c72ae02879624b43fb62363967bde023c11d962c0eae820e5cb9d926', '[\"*\"]', '2023-01-07 19:47:05', '2023-01-07 20:13:29', '2023-01-07 19:13:29', '2023-01-07 19:47:05'),
(455, 'App\\Models\\User', 2, 'auth_token', 'b43957c7e32b36839ef25e49576222a5952e21d02d2ae16ee38e2b9ded093e7f', '[\"*\"]', '2023-01-07 21:19:11', '2023-01-07 21:38:26', '2023-01-07 20:38:26', '2023-01-07 21:19:11'),
(456, 'App\\Models\\User', 2, 'auth_token', 'ad6703074d698cfc239f123aca75f108700c0a5a7e05c8e828429cf83527f165', '[\"*\"]', NULL, '2023-01-07 22:21:50', '2023-01-07 21:21:50', '2023-01-07 21:21:50'),
(457, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'e17b7f413467a4da05c78c0cd1d48742e0dfc49f274817e9f5b1bb29aa1b2613', '[\"*\"]', NULL, '2023-01-07 22:25:08', '2023-01-07 21:25:08', '2023-01-07 21:25:08'),
(458, 'App\\Models\\User', 2, 'auth_token', '7498eb0633b10ce472c115856f2f74ffd50a292c419ccce331ec086671b3c828', '[\"*\"]', NULL, '2023-01-07 22:31:40', '2023-01-07 21:31:40', '2023-01-07 21:31:40'),
(459, 'App\\Models\\User', 2, 'auth_token', 'dd6502e426de0c31dd929ea5e45d524e39f3564740070698a1b6f86ee6f36d24', '[\"*\"]', '2023-01-07 22:26:43', '2023-01-07 22:31:53', '2023-01-07 21:31:53', '2023-01-07 22:26:43'),
(460, 'App\\Models\\User', 2, 'auth_token', 'e67916d4348b29dc32468f3cca1c47760ccbaff0b76335b32832a47b08415046', '[\"*\"]', '2023-01-07 23:12:38', '2023-01-07 23:51:33', '2023-01-07 22:51:33', '2023-01-07 23:12:38'),
(461, 'App\\Models\\User', 2, 'auth_token', 'e1fc29358a04850c23e69d7e18d236796782538e573dca711f6c573bca39af43', '[\"*\"]', NULL, '2023-01-08 00:12:43', '2023-01-07 23:12:43', '2023-01-07 23:12:43'),
(462, 'App\\Models\\User', 2, 'auth_token', '2322d7d59d1917681778a6328f155b5f2aeb3e4e9c5462cde9b2b39941fc97de', '[\"*\"]', NULL, '2023-01-08 04:59:02', '2023-01-08 03:59:02', '2023-01-08 03:59:02'),
(463, 'App\\Models\\User', 2, 'auth_token', 'eddf91bb4c0bfeb14099736eeaf4e2fc7c669e7c225d892035499c8fb3b88066', '[\"*\"]', NULL, '2023-01-08 05:05:56', '2023-01-08 04:05:56', '2023-01-08 04:05:56'),
(464, 'App\\Models\\User', 2, 'auth_token', 'e48e411c16b2701e143dbdd3365414285dea11c83b94b37fcb46f67d0dcffb09', '[\"*\"]', NULL, '2023-01-08 05:22:45', '2023-01-08 04:22:45', '2023-01-08 04:22:45'),
(465, 'App\\Models\\AkunPegawai', 1, 'auth_token', '8acc6c532ac60ca5e9252e085fde8a02be8bd8eaba6f7f40cd012c5de425676d', '[\"*\"]', NULL, '2023-01-08 05:22:57', '2023-01-08 04:22:57', '2023-01-08 04:22:57'),
(466, 'App\\Models\\User', 2, 'auth_token', '586408ad0ffb260f76870cf40224674015abc8ac864e086d624565f0cf7e3234', '[\"*\"]', '2023-01-08 05:12:37', '2023-01-08 05:23:11', '2023-01-08 04:23:11', '2023-01-08 05:12:37'),
(467, 'App\\Models\\User', 2, 'auth_token', '2b3ebc82b9f341c0f9ce42a3db2c728782a01b5bc94652a1e81cd07c657b2158', '[\"*\"]', NULL, '2023-01-08 06:12:51', '2023-01-08 05:12:51', '2023-01-08 05:12:51'),
(468, 'App\\Models\\User', 2, 'auth_token', '7844dc924a7b27defa8d947dbe5719c69fa6db90392a5b05ddb2e961404254b1', '[\"*\"]', NULL, '2023-01-08 08:56:44', '2023-01-08 07:56:44', '2023-01-08 07:56:44'),
(469, 'App\\Models\\AkunPegawai', 1, 'auth_token', '8f9ae533c7a675ef31e7135cdce5421d7aa613cd38a7181d98cdef0a443a9d70', '[\"*\"]', NULL, '2023-01-08 08:58:21', '2023-01-08 07:58:21', '2023-01-08 07:58:21'),
(470, 'App\\Models\\User', 2, 'auth_token', 'cd9ade2c7629c3c91a5cf591324c9a62746dc34fe1e6c45bc01bc8655d23b50a', '[\"*\"]', NULL, '2023-01-08 09:21:24', '2023-01-08 08:21:24', '2023-01-08 08:21:24'),
(471, 'App\\Models\\User', 2, 'auth_token', '6e293b95ecbcd00bbd4f0b17a9be40a315b491030d0d375bfb67362d5b153dce', '[\"*\"]', NULL, '2023-01-08 10:34:25', '2023-01-08 09:34:25', '2023-01-08 09:34:25'),
(472, 'App\\Models\\User', 2, 'auth_token', 'fab5b4cd6c48256fb81ce5d077e74b1f8b3028409bce9afab7abcd734e42aff8', '[\"*\"]', NULL, '2023-01-08 15:26:01', '2023-01-08 14:26:01', '2023-01-08 14:26:01'),
(473, 'App\\Models\\AkunPegawai', 1, 'auth_token', '09b0295d27c666fe87874566142d0fefd74c9fa262503f7a86c05d36d5bd92f2', '[\"*\"]', NULL, '2023-01-08 15:27:20', '2023-01-08 14:27:20', '2023-01-08 14:27:20'),
(474, 'App\\Models\\AkunPegawai', 3, 'auth_token', '4c9e06d48d7659b1249e73b56893fb0652f2a54b8d3663c28bd929bcadcc3145', '[\"*\"]', NULL, '2023-01-08 15:33:58', '2023-01-08 14:33:58', '2023-01-08 14:33:58'),
(475, 'App\\Models\\User', 2, 'auth_token', '14c104ca22724ad30a021b7c600a977ac7d96b037975c0acf68e5e42da35fe3d', '[\"*\"]', '2023-01-08 15:01:08', '2023-01-08 15:41:36', '2023-01-08 14:41:36', '2023-01-08 15:01:08'),
(476, 'App\\Models\\AkunPegawai', 1, 'auth_token', '905d781fdf72f8129e909b2e93baf0fe74abd27b5f85f8114be6285ae2b200b0', '[\"*\"]', NULL, '2023-01-08 15:48:50', '2023-01-08 14:48:50', '2023-01-08 14:48:50'),
(477, 'App\\Models\\AkunPegawai', 1, 'auth_token', '7e118d435b07df78737b9525853522f2148dde40e5a04ec8db05585fa801b48b', '[\"*\"]', NULL, '2023-01-08 18:28:21', '2023-01-08 17:28:21', '2023-01-08 17:28:21'),
(478, 'App\\Models\\User', 2, 'auth_token', '7dcf47f92efe0272afb42e79a12046d6a1574d842a50f68faf2f8061554a641f', '[\"*\"]', '2023-01-08 18:35:08', '2023-01-08 18:37:26', '2023-01-08 17:37:26', '2023-01-08 18:35:08'),
(479, 'App\\Models\\User', 2, 'auth_token', '593d4e120ceb3fb1a1645a54be5c6576610ba035e44d179bfe5a6095f90f2e3d', '[\"*\"]', '2023-01-08 18:58:13', '2023-01-08 19:43:06', '2023-01-08 18:43:06', '2023-01-08 18:58:13'),
(480, 'App\\Models\\User', 2, 'auth_token', 'c9e68756018bcf9d943f00b43f2f88c4ef3b76ca595ae72badd737f9001caaf5', '[\"*\"]', NULL, '2023-01-08 19:58:22', '2023-01-08 18:58:22', '2023-01-08 18:58:22'),
(481, 'App\\Models\\User', 2, 'auth_token', 'b3fe3c73051509d38e463a41cd12fcd706c6156a70ef85a20f864c5a12ee9188', '[\"*\"]', '2023-01-08 20:29:22', '2023-01-08 20:59:01', '2023-01-08 19:59:01', '2023-01-08 20:29:22'),
(482, 'App\\Models\\User', 2, 'auth_token', 'ad88c073be79f947628e4ec55a6322f2ffc19bb398303b9a768b2804270c216c', '[\"*\"]', '2023-01-08 21:46:18', '2023-01-08 22:25:01', '2023-01-08 21:25:01', '2023-01-08 21:46:18'),
(483, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'bfa1ed5e87da2c8b8c88c5804690d68871c151e0a3f2cb9d01e6b5cf7dd098fc', '[\"*\"]', NULL, '2023-01-08 22:25:17', '2023-01-08 21:25:17', '2023-01-08 21:25:17'),
(484, 'App\\Models\\User', 2, 'auth_token', 'b78bfdfe177d84672d98010af8e6d5e413974ec0c230c3b3481acfac9c5b9167', '[\"*\"]', '2023-01-09 07:35:07', '2023-01-09 07:35:16', '2023-01-09 06:35:16', '2023-01-09 07:35:07'),
(485, 'App\\Models\\AkunPegawai', 1, 'auth_token', '42fb00b7d887df2a754634c1b5ad0e0563e1894c9a6912dbb59abcf65433ce1e', '[\"*\"]', NULL, '2023-01-09 07:45:34', '2023-01-09 06:45:34', '2023-01-09 06:45:34'),
(486, 'App\\Models\\User', 2, 'auth_token', '20ce4f8f19f477e85c41922867df29498828ce68ae2e679ce4a6ec8f8d03def0', '[\"*\"]', NULL, '2023-01-09 08:35:24', '2023-01-09 07:35:24', '2023-01-09 07:35:24'),
(487, 'App\\Models\\AkunPegawai', 6, 'auth_token', 'b290a07a7b92d78b650c5c6290376b2442c787e4fa875f844f732d7c4a8a3f3a', '[\"*\"]', NULL, '2023-01-09 09:33:05', '2023-01-09 08:33:05', '2023-01-09 08:33:05'),
(488, 'App\\Models\\User', 2, 'auth_token', 'e8a7ad1a8be2c84c06ef5a9b39543167fd9e2fda0d38463d6adbdd01dca2fe41', '[\"*\"]', NULL, '2023-01-09 10:05:33', '2023-01-09 09:05:33', '2023-01-09 09:05:33'),
(489, 'App\\Models\\User', 2, 'auth_token', 'f9809fb10a39e69812ddb9af839dc36940ea81f5d94d73d2a9b9e4a2a47889e1', '[\"*\"]', '2023-01-09 19:07:05', '2023-01-09 19:14:44', '2023-01-09 18:14:44', '2023-01-09 19:07:05'),
(490, 'App\\Models\\AkunPegawai', 1, 'auth_token', '291fb5b7aa5ed13241d872a3ce624445300a39f5824b529155b536edb080b002', '[\"*\"]', NULL, '2023-01-09 19:29:42', '2023-01-09 18:29:42', '2023-01-09 18:29:42'),
(491, 'App\\Models\\AkunPegawai', 1, 'auth_token', '059d15416f3695d88999d71340fdd148d2ce5f9d95933e1ff525ab59e5afd1b4', '[\"*\"]', NULL, '2023-01-09 23:25:32', '2023-01-09 22:25:32', '2023-01-09 22:25:32'),
(492, 'App\\Models\\User', 2, 'auth_token', '5cf93a918abb6c79004078df69ee36d86a990417606d04003720a0e076ffb0eb', '[\"*\"]', '2023-01-09 23:19:36', '2023-01-09 23:30:53', '2023-01-09 22:30:53', '2023-01-09 23:19:36'),
(493, 'App\\Models\\User', 2, 'auth_token', '5601f7fb89d35fa50a8c92c7cb9e6b0cae39b612fb3a611ed4fbbef8dff8eea2', '[\"*\"]', '2023-01-10 00:47:43', '2023-01-10 00:47:56', '2023-01-09 23:47:56', '2023-01-10 00:47:43'),
(494, 'App\\Models\\User', 2, 'auth_token', '2137a15959965bb1ceda04e28dd2d97ddab28169aef1ddcef4ec80e6d031728f', '[\"*\"]', '2023-01-10 01:46:55', '2023-01-10 01:48:54', '2023-01-10 00:48:54', '2023-01-10 01:46:55'),
(495, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'cde834034ccd737ca70dd4444d99e80d22e6681248bd517de2e428e43b20d921', '[\"*\"]', NULL, '2023-01-10 02:17:14', '2023-01-10 01:17:14', '2023-01-10 01:17:14'),
(496, 'App\\Models\\User', 2, 'auth_token', '673b41dde33ba5f84efe196d1cf6e035bba211b7b818f4ad017edc098d547551', '[\"*\"]', '2023-01-10 02:47:23', '2023-01-10 02:50:37', '2023-01-10 01:50:37', '2023-01-10 02:47:23'),
(497, 'App\\Models\\AkunPegawai', 1, 'auth_token', '269d19e9841b33fd26de9e51ffc36d9024e30c1aa5fe111da65926c1bd4b2b6e', '[\"*\"]', NULL, '2023-01-10 04:56:56', '2023-01-10 03:56:56', '2023-01-10 03:56:56'),
(498, 'App\\Models\\User', 2, 'auth_token', 'd25f3eeb4388e1057a237b15231d9a320156de9cc91fbeb5a6d2e52dc509d7ae', '[\"*\"]', NULL, '2023-01-10 05:02:08', '2023-01-10 04:02:08', '2023-01-10 04:02:08'),
(499, 'App\\Models\\User', 2, 'auth_token', '77e7a12582b95129b9af8fc1bd8b1c70a7ac4c3d2feca8b5a18b197d19b075e9', '[\"*\"]', '2023-01-12 16:01:47', '2023-01-12 16:40:12', '2023-01-12 15:40:12', '2023-01-12 16:01:47'),
(500, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'f7123fc0d6369171cf0027e068c98dfb4d6b91bb42d9e5363a70c9832b5f8684', '[\"*\"]', NULL, '2023-01-12 17:02:02', '2023-01-12 16:02:02', '2023-01-12 16:02:02'),
(501, 'App\\Models\\AkunPegawai', 1, 'auth_token', '0a56de533d972fd4fe777037fbfe5d891b51fdb5aa622188e964152f3ac338a5', '[\"*\"]', NULL, '2023-01-12 20:26:50', '2023-01-12 19:26:50', '2023-01-12 19:26:50'),
(502, 'App\\Models\\User', 2, 'auth_token', '9f26588e45897372d0d8310efaeca00a4719f9312dd118d750513e42fa40a535', '[\"*\"]', '2023-01-12 20:04:19', '2023-01-12 20:29:15', '2023-01-12 19:29:15', '2023-01-12 20:04:19'),
(503, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'df7b060658903af38681368d42c7189927115913ccdd7d07ae4bfdc00e17ae1c', '[\"*\"]', NULL, '2023-01-13 02:57:36', '2023-01-13 01:57:36', '2023-01-13 01:57:36'),
(504, 'App\\Models\\AkunPegawai', 1, 'auth_token', '2c5b04aa2c5034a6d546e9b037a6bf3c4f5c291a52aa29ff1d866c4fd8fecda3', '[\"*\"]', '2023-01-13 02:32:27', '2023-01-13 03:31:53', '2023-01-13 02:31:53', '2023-01-13 02:32:27'),
(505, 'App\\Models\\AkunPegawai', 1, 'auth_token', '3cefbb6377e0d901091f03a9d8e4bfc2fb04634266c3700a813615a546d55506', '[\"*\"]', NULL, '2023-01-13 06:07:53', '2023-01-13 05:07:53', '2023-01-13 05:07:53'),
(506, 'App\\Models\\User', 2, 'auth_token', '1c34377b4ae281e3a9e4215c7cd21a26df081d98765960ffc1c5d65cdd78476c', '[\"*\"]', NULL, '2023-01-13 07:15:45', '2023-01-13 06:15:45', '2023-01-13 06:15:45'),
(507, 'App\\Models\\AkunPegawai', 1, 'auth_token', '3365b955b626c0c95c40fa24cc5dddfc010cbb18836d09f7be5a6cd9415af3d7', '[\"*\"]', NULL, '2023-01-13 09:00:56', '2023-01-13 08:00:56', '2023-01-13 08:00:56'),
(508, 'App\\Models\\User', 2, 'auth_token', 'bb52bf83b54419f34c5da2da263ca7a57632465ab1df849632963c5dd1d12403', '[\"*\"]', NULL, '2023-01-13 09:21:14', '2023-01-13 08:21:14', '2023-01-13 08:21:14'),
(509, 'App\\Models\\User', 2, 'auth_token', '7c68c234570bab805dc21c88adbcab62c1ae1c053175e44c3e169dcbcfe35860', '[\"*\"]', '2023-01-13 14:57:42', '2023-01-13 15:32:53', '2023-01-13 14:32:53', '2023-01-13 14:57:42'),
(510, 'App\\Models\\AkunPegawai', 1, 'auth_token', '216f990a4ef9792678fc2060041b82b99291e536d9c49aaa4228f024d6e2887b', '[\"*\"]', NULL, '2023-01-13 15:56:46', '2023-01-13 14:56:46', '2023-01-13 14:56:46'),
(511, 'App\\Models\\User', 2, 'auth_token', '665aaa37bd271d0fcc7db2c787734fe2ca2f12ba548056be5002a00b235029f0', '[\"*\"]', '2023-01-13 20:17:42', '2023-01-13 20:27:00', '2023-01-13 19:27:00', '2023-01-13 20:17:42'),
(512, 'App\\Models\\AkunPegawai', 1, 'auth_token', '225721db1a46282e8e09a2df7832112428ccc85bc48d97ed22e5b87b350f22a7', '[\"*\"]', NULL, '2023-01-13 20:28:27', '2023-01-13 19:28:27', '2023-01-13 19:28:27'),
(513, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'ccbe69c006b1298c357113cb162577921ba1bcee302c7700ca5d9c3cb966656c', '[\"*\"]', NULL, '2023-01-14 00:42:28', '2023-01-13 23:42:28', '2023-01-13 23:42:28'),
(514, 'App\\Models\\User', 2, 'auth_token', '2bbf7862b89cce5b487467bdfc9a6e285ece03523b0f6f76b961d0a739b150ab', '[\"*\"]', '2023-01-14 00:22:25', '2023-01-14 00:43:07', '2023-01-13 23:43:07', '2023-01-14 00:22:25'),
(515, 'App\\Models\\User', 2, 'auth_token', '54750eb88ff6d51b7b6567c967d86269b9bcafad5bc65c38ae06c60b889606d1', '[\"*\"]', '2023-01-14 05:15:46', '2023-01-14 05:48:20', '2023-01-14 04:48:20', '2023-01-14 05:15:46'),
(516, 'App\\Models\\User', 2, 'auth_token', '67c14368df0398b58266fa07613e12490bbc8e9e66a76a21eb17f843e6ba0e12', '[\"*\"]', NULL, '2023-01-14 06:15:50', '2023-01-14 05:15:50', '2023-01-14 05:15:50'),
(517, 'App\\Models\\User', 2, 'auth_token', '0e37d7d0f349cf4acdfb4cb6694b220dc4b1a08999b1c4b0a152a81e8a55a965', '[\"*\"]', '2023-01-14 07:08:39', '2023-01-14 07:28:45', '2023-01-14 06:28:45', '2023-01-14 07:08:39'),
(518, 'App\\Models\\AkunPegawai', 1, 'auth_token', '18fe453428cbc121b8bcc785b8c05800ea0e28102515c0d3498f0f66cc14b13c', '[\"*\"]', NULL, '2023-01-14 07:33:52', '2023-01-14 06:33:52', '2023-01-14 06:33:52'),
(519, 'App\\Models\\AkunPegawai', 1, 'auth_token', '5b1f4014ea3506ff43ad0cec3cfc69306bf0dd92d7d5adff60f06dd163c6d3e2', '[\"*\"]', NULL, '2023-01-14 08:06:19', '2023-01-14 07:06:19', '2023-01-14 07:06:19'),
(520, 'App\\Models\\User', 2, 'auth_token', 'bba2d7db2cd8a80f1477e9b7ab8ce5598182aa5f8a9d6ba9167b5128a9cf5c14', '[\"*\"]', NULL, '2023-01-14 08:08:43', '2023-01-14 07:08:43', '2023-01-14 07:08:43'),
(521, 'App\\Models\\User', 3, 'auth_token', '6451ab5fcb6c9b71e514703347d1b374e027d14f674d6fae3cff48b6d769c503', '[\"*\"]', NULL, '2023-01-14 08:12:17', '2023-01-14 07:12:17', '2023-01-14 07:12:17'),
(522, 'App\\Models\\AkunPegawai', 1, 'auth_token', '0bc4da7cc7e86363677ed2b3faa653665810001b56aec1c0b29c766066d1de78', '[\"*\"]', NULL, '2023-01-14 08:12:27', '2023-01-14 07:12:27', '2023-01-14 07:12:27'),
(523, 'App\\Models\\AkunPegawai', 1, 'auth_token', '6df21a0c3b80f5da2c0ac1915f0841463f8317ed2ab683b074f408d387822e8b', '[\"*\"]', NULL, '2023-01-14 08:21:54', '2023-01-14 07:21:54', '2023-01-14 07:21:54'),
(524, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'a4530857aa79b66d120ef68911c76803497a32351ae1b759e7b873ba6cf5e07d', '[\"*\"]', NULL, '2023-01-14 08:22:36', '2023-01-14 07:22:36', '2023-01-14 07:22:36'),
(525, 'App\\Models\\User', 2, 'auth_token', 'c3c8611d2e49714124d93567bd2696f1f03d50d27f83392f17778e01d301d259', '[\"*\"]', NULL, '2023-01-14 08:31:57', '2023-01-14 07:31:57', '2023-01-14 07:31:57'),
(526, 'App\\Models\\AkunPegawai', 1, 'auth_token', '1e9cf655d5dd82e20949cb19a0aad385f1b1649051e286a0f9bc18090f565ae3', '[\"*\"]', NULL, '2023-01-14 08:34:35', '2023-01-14 07:34:35', '2023-01-14 07:34:35'),
(527, 'App\\Models\\User', 2, 'auth_token', '4b0157438024f89fc0286267b10f086235f0f88e4a3a1b70cc6a972cfa849e5d', '[\"*\"]', NULL, '2023-01-14 20:01:49', '2023-01-14 19:01:49', '2023-01-14 19:01:49'),
(528, 'App\\Models\\AkunPegawai', 1, 'auth_token', '52597250799d1381b4ca142e1424c9acaff89fd19989cf641635dccd0ab752c8', '[\"*\"]', NULL, '2023-01-14 20:03:46', '2023-01-14 19:03:46', '2023-01-14 19:03:46'),
(529, 'App\\Models\\AkunPegawai', 1, 'auth_token', '189647c91a600cf5cc07c0a784756177565ff2624b8d64d32b4958014a99cd25', '[\"*\"]', NULL, '2023-01-15 02:22:03', '2023-01-15 01:22:03', '2023-01-15 01:22:03'),
(530, 'App\\Models\\User', 10, 'auth_token', 'f099542994a1c76802342fbcc1b4a3f006201d506fe61d2c9475b44203df7aab', '[\"*\"]', NULL, '2023-01-15 02:23:48', '2023-01-15 01:23:48', '2023-01-15 01:23:48'),
(531, 'App\\Models\\User', 3, 'auth_token', 'e1eac33269aad976c01ab202092031e11e2d53616e3dd1456f95c4467d1dddb0', '[\"*\"]', NULL, '2023-01-15 02:50:04', '2023-01-15 01:50:04', '2023-01-15 01:50:04'),
(532, 'App\\Models\\User', 10, 'auth_token', 'a97d6a62c73d9744fb755ebe2d83bf4d7f9c75d0dd23748be71fa417e57195f6', '[\"*\"]', NULL, '2023-01-15 02:50:52', '2023-01-15 01:50:52', '2023-01-15 01:50:52'),
(533, 'App\\Models\\User', 2, 'auth_token', 'ee3f10e87bbc61fa97ce29ce6ffa450cb35e64d0f3c4590bb50692ab247e30de', '[\"*\"]', NULL, '2023-01-15 15:32:04', '2023-01-15 14:32:04', '2023-01-15 14:32:04'),
(534, 'App\\Models\\User', 2, 'auth_token', '1cb1bdbb8e04a414aa1db4eb10917a6f62ef38b0b371d0db3cfad42cd467d16e', '[\"*\"]', NULL, '2023-01-15 18:33:25', '2023-01-15 17:33:25', '2023-01-15 17:33:25'),
(535, 'App\\Models\\User', 2, 'auth_token', '7f452044456236637e772aa465e40139ea083d8a5a05265a7725015005f8e281', '[\"*\"]', '2023-01-15 21:46:01', '2023-01-15 21:50:03', '2023-01-15 20:50:03', '2023-01-15 21:46:01'),
(536, 'App\\Models\\AkunPegawai', 1, 'auth_token', '1736c111e2bef1c05c1e8d1e4689b9f9800ce313ec0696f60c10ae3fed2cca4e', '[\"*\"]', NULL, '2023-01-15 22:14:28', '2023-01-15 21:14:28', '2023-01-15 21:14:28'),
(537, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'bb0aab494762484dc0aa7dd84ede6024596609e1c5770525f1589350c7a4bbf6', '[\"*\"]', NULL, '2023-01-15 23:44:16', '2023-01-15 22:44:16', '2023-01-15 22:44:16'),
(538, 'App\\Models\\User', 2, 'auth_token', '5724d3bd697c16a17103245ed8d98c25be4237eebafe8db3251823ab1dbdd91b', '[\"*\"]', NULL, '2023-01-15 23:45:08', '2023-01-15 22:45:08', '2023-01-15 22:45:08'),
(539, 'App\\Models\\User', 2, 'auth_token', '71493fe1e7c6227a1ac435fa03783ead98975948e8ffeed86633047eb5b9c537', '[\"*\"]', '2023-01-16 05:45:58', '2023-01-16 05:46:03', '2023-01-16 04:46:03', '2023-01-16 05:45:58'),
(540, 'App\\Models\\User', 2, 'auth_token', 'c227426e7dacdaec341c1dd221731a7c64a9b9af23b401ec07386b7631b4c5f2', '[\"*\"]', NULL, '2023-01-16 06:46:19', '2023-01-16 05:46:19', '2023-01-16 05:46:19'),
(541, 'App\\Models\\User', 2, 'auth_token', '4656013e7e65ab4ee49cc41192e7bdab44567cfa93ab33525f775c833c2e7093', '[\"*\"]', NULL, '2023-01-16 10:38:25', '2023-01-16 09:38:25', '2023-01-16 09:38:25'),
(542, 'App\\Models\\AkunPegawai', 1, 'auth_token', '4cf23edd55666eb6ddd016f1c6a77118a22b5013404e3c586304ea70e7294b3c', '[\"*\"]', NULL, '2023-01-16 11:47:51', '2023-01-16 10:47:51', '2023-01-16 10:47:51'),
(543, 'App\\Models\\User', 2, 'auth_token', '85ccf5dbcf35ed90a8564fcb9ae43677f2a0edb847344d685b116220b9da713d', '[\"*\"]', '2023-01-16 16:54:01', '2023-01-16 17:20:57', '2023-01-16 16:20:57', '2023-01-16 16:54:01'),
(544, 'App\\Models\\User', 2, 'auth_token', 'abf168690728913660b1bf3758a1c9d43192abf4dd5b926eeab247dabaf6e2c8', '[\"*\"]', '2023-01-16 18:05:03', '2023-01-16 18:21:18', '2023-01-16 17:21:18', '2023-01-16 18:05:03'),
(545, 'App\\Models\\User', 2, 'auth_token', 'c41f40a754d8fca359c9ccfd51c241b9bf26186f38d7fdb22889e05077789a87', '[\"*\"]', NULL, '2023-01-16 20:15:08', '2023-01-16 19:15:08', '2023-01-16 19:15:08'),
(546, 'App\\Models\\User', 2, 'auth_token', '150b42fd8240ad6919a2f9da127f0a9b04868f354dd2f68dcdf54e0cefbc25cb', '[\"*\"]', NULL, '2023-01-16 21:42:01', '2023-01-16 20:42:01', '2023-01-16 20:42:01'),
(547, 'App\\Models\\User', 2, 'auth_token', '09d6bec2bfcc26a012f2aec76ef9cc817386f96c6365d2cccb8e388d4cd9fd5f', '[\"*\"]', '2023-01-17 07:32:39', '2023-01-17 07:33:49', '2023-01-17 06:33:49', '2023-01-17 07:32:39'),
(548, 'App\\Models\\User', 2, 'auth_token', 'f673cfd9f43518d10a9e5ce7b1097970b501909bd1fafde7c73faa7c7b0b8a13', '[\"*\"]', NULL, '2023-01-17 08:33:21', '2023-01-17 07:33:21', '2023-01-17 07:33:21'),
(549, 'App\\Models\\User', 2, 'auth_token', '8ff64c7f7b30124cea12711e724569692e38a5580d3176264439771045ce1f2a', '[\"*\"]', NULL, '2023-01-17 15:37:16', '2023-01-17 14:37:16', '2023-01-17 14:37:16'),
(550, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'b2a377918f8fbe2f682272d054a1d6d88a936fb036099326eb409f6ed7abf783', '[\"*\"]', NULL, '2023-01-17 15:37:38', '2023-01-17 14:37:38', '2023-01-17 14:37:38'),
(551, 'App\\Models\\User', 2, 'auth_token', '50637e7378feb8264709636c12718229670704f0337f594a08ec7bd783aa8401', '[\"*\"]', NULL, '2023-01-18 03:25:56', '2023-01-18 02:25:56', '2023-01-18 02:25:56'),
(552, 'App\\Models\\User', 2, 'auth_token', 'e108697695b247e843d2e8663a381b1142bb67fdc1797dd49546f23d789e9360', '[\"*\"]', '2023-01-18 20:51:19', '2023-01-18 21:03:49', '2023-01-18 20:03:49', '2023-01-18 20:51:19'),
(553, 'App\\Models\\AkunPegawai', 1, 'auth_token', '6b16cbfd144e7c12872ab6ba8387c8228acb41c681c3938571735dec266f7a55', '[\"*\"]', NULL, '2023-01-18 21:04:49', '2023-01-18 20:04:49', '2023-01-18 20:04:49'),
(554, 'App\\Models\\User', 2, 'auth_token', '2e4d0b8966736bbf48e4380061cc662ce850e915db14eb25c6068368a7ab7c89', '[\"*\"]', NULL, '2023-01-19 08:43:45', '2023-01-19 07:43:45', '2023-01-19 07:43:45'),
(555, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'bc75aa31f82c2f484a8aa2f5aed89fcb92f7f73e1e3b204deef0f88962266427', '[\"*\"]', NULL, '2023-01-19 08:45:11', '2023-01-19 07:45:11', '2023-01-19 07:45:11'),
(556, 'App\\Models\\User', 2, 'auth_token', '61f8010ea71f973e448fe903550526c92c7f362e43695dd89f5aab35357e482b', '[\"*\"]', NULL, '2023-01-19 09:16:27', '2023-01-19 08:16:27', '2023-01-19 08:16:27'),
(557, 'App\\Models\\User', 2, 'auth_token', '93eaab7380db3ad52a77611f96b177370de22c89fbd131aae393ca0cecc89976', '[\"*\"]', NULL, '2023-01-19 18:07:53', '2023-01-19 17:07:53', '2023-01-19 17:07:53'),
(558, 'App\\Models\\User', 2, 'auth_token', 'fe40dfeb46d44089c1c0aa17a7af5a49e19b3f8d851c8968b76c9265113769fd', '[\"*\"]', '2023-01-19 22:20:06', '2023-01-19 22:43:00', '2023-01-19 21:43:00', '2023-01-19 22:20:06'),
(559, 'App\\Models\\User', 2, 'auth_token', '60c9feca1cacf1c0e21658a0df68892060ef5f770fef3373d873de6649ae9d27', '[\"*\"]', NULL, '2023-01-19 23:20:16', '2023-01-19 22:20:16', '2023-01-19 22:20:16'),
(560, 'App\\Models\\User', 2, 'auth_token', '56ec3122aa484db65263c14003114998b12b90896dc62754a92383091c8e918c', '[\"*\"]', NULL, '2023-01-20 05:22:09', '2023-01-20 04:22:09', '2023-01-20 04:22:09'),
(561, 'App\\Models\\AkunPegawai', 1, 'auth_token', '942ca9e05b012bfe48f63a932accddc4b982d4b2046425d16c940f4a4006e106', '[\"*\"]', NULL, '2023-01-20 05:23:25', '2023-01-20 04:23:25', '2023-01-20 04:23:25'),
(562, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'd3219772390c018c4a884478f07b9ba4339a5ea9e2a183d52b113fcf13aee142', '[\"*\"]', NULL, '2023-01-20 19:54:24', '2023-01-20 18:54:24', '2023-01-20 18:54:24'),
(563, 'App\\Models\\User', 2, 'auth_token', 'de5f4203aa0e36d1fadf5330a20a190b666bb5215b96bb7ef2675910e28158b6', '[\"*\"]', NULL, '2023-01-20 19:54:37', '2023-01-20 18:54:37', '2023-01-20 18:54:37'),
(564, 'App\\Models\\User', 2, 'auth_token', '943ad645c3256339b3e880c05a0c0826b0c11c0b98151ec30c3c57b03e4cee70', '[\"*\"]', '2023-01-20 21:08:33', '2023-01-20 21:10:40', '2023-01-20 20:10:40', '2023-01-20 21:08:33'),
(565, 'App\\Models\\User', 2, 'auth_token', 'be16e580286fd13dad57bf3e5c7dd908bd3bb9741e50003b26091c3236428acb', '[\"*\"]', NULL, '2023-01-20 22:15:39', '2023-01-20 21:15:39', '2023-01-20 21:15:39'),
(566, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'b6b5de94ed7b7099b36c719ef05b118a3db83a1df47a9a5c0373599571e84605', '[\"*\"]', NULL, '2023-01-21 00:20:15', '2023-01-20 23:20:15', '2023-01-20 23:20:15'),
(567, 'App\\Models\\User', 2, 'auth_token', '1aa94052cc4442e1df4b7618865b33ebdc53348f47847a11cb51062f54ba150d', '[\"*\"]', NULL, '2023-01-21 00:20:54', '2023-01-20 23:20:54', '2023-01-20 23:20:54'),
(568, 'App\\Models\\User', 2, 'auth_token', 'b6fb9116858bebdb1bc723e87a02b585c9c27d3b83bed288e1b5137cccba0b18', '[\"*\"]', NULL, '2023-01-21 01:43:58', '2023-01-21 00:43:58', '2023-01-21 00:43:58'),
(569, 'App\\Models\\User', 2, 'auth_token', '5dec46f6cf1afc8fa8f551bc15b2239c91cb23abf5e21d9b33ccb0ebf71eac18', '[\"*\"]', NULL, '2023-01-21 04:17:42', '2023-01-21 03:17:42', '2023-01-21 03:17:42'),
(570, 'App\\Models\\AkunPegawai', 1, 'auth_token', '5ba10d545592546d5401188e8e431caa1466729edca4d9826bfaff6a7a3600df', '[\"*\"]', NULL, '2023-01-21 04:30:00', '2023-01-21 03:30:00', '2023-01-21 03:30:00'),
(571, 'App\\Models\\User', 2, 'auth_token', '888696a2de34b6cdfed86a35e84442c17301847af5d547b9b794a3107f55e910', '[\"*\"]', NULL, '2023-01-21 04:30:32', '2023-01-21 03:30:32', '2023-01-21 03:30:32'),
(572, 'App\\Models\\User', 2, 'auth_token', '98b319c549d6f58058ccf431c13181949f2780f8fa769493f4308fee546074db', '[\"*\"]', NULL, '2023-01-21 09:13:46', '2023-01-21 08:13:46', '2023-01-21 08:13:46'),
(573, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'aee276831eb888183e8a1715fe5e74217276d78dbaa06b3e711f0259f5411fe6', '[\"*\"]', NULL, '2023-01-22 04:15:16', '2023-01-22 03:15:16', '2023-01-22 03:15:16'),
(574, 'App\\Models\\AkunPegawai', 1, 'auth_token', '2cbc55eb1d339b0ddda69f072dfb04c7ebe3367658b4ffaaeab76302d3b5e119', '[\"*\"]', NULL, '2023-01-22 05:27:25', '2023-01-22 04:27:25', '2023-01-22 04:27:25'),
(575, 'App\\Models\\User', 2, 'auth_token', 'd215bc9e266155371ed772f255147ecbbdb2769ea1bd3e4414c253c1b2bbf0b0', '[\"*\"]', NULL, '2023-01-22 05:27:34', '2023-01-22 04:27:34', '2023-01-22 04:27:34'),
(576, 'App\\Models\\User', 2, 'auth_token', 'fba54adbd2b0e02d0d76ea298b68cb39a811d18f4b3e35336887c0d08dfd0c5f', '[\"*\"]', '2023-01-22 15:48:25', '2023-01-22 15:57:00', '2023-01-22 14:57:00', '2023-01-22 15:48:25'),
(577, 'App\\Models\\AkunPegawai', 1, 'auth_token', '486398206b44d93204a5f001b6ef58b80541c6362237d1c89c64e01074988cbe', '[\"*\"]', NULL, '2023-01-22 16:13:27', '2023-01-22 15:13:27', '2023-01-22 15:13:27'),
(578, 'App\\Models\\User', 2, 'auth_token', '6a4e7ec92b4c2c664fa45c04a7ef85729180abbb913825bd079fece491971c60', '[\"*\"]', '2023-01-22 17:03:05', '2023-01-22 17:03:56', '2023-01-22 16:03:56', '2023-01-22 17:03:05'),
(579, 'App\\Models\\User', 2, 'auth_token', 'd8e113aa58ec8766f6c7c9d2b09c296add0dcd1762a8bcbbe3e70b7ec9c058ce', '[\"*\"]', '2023-01-22 17:58:40', '2023-01-22 18:05:59', '2023-01-22 17:05:59', '2023-01-22 17:58:40'),
(580, 'App\\Models\\User', 2, 'auth_token', 'bde35d5430154d54567d3407ac7b6bce42886ef919a3d91717ea1cb7ce6211d5', '[\"*\"]', NULL, '2023-01-22 19:07:02', '2023-01-22 18:07:02', '2023-01-22 18:07:02'),
(581, 'App\\Models\\AkunPegawai', 1, 'auth_token', '6d2cbdd798138b4162a61fdd7564e8e153d9ea1e5b99dcfbf32fa597caee8e44', '[\"*\"]', NULL, '2023-01-22 20:23:30', '2023-01-22 19:23:30', '2023-01-22 19:23:30'),
(582, 'App\\Models\\User', 2, 'auth_token', 'fdf1f2b1c7d6514344d813139fa30e67d1c865cf9cba5f5a32d0021330dde055', '[\"*\"]', NULL, '2023-01-22 20:31:04', '2023-01-22 19:31:04', '2023-01-22 19:31:04'),
(583, 'App\\Models\\User', 2, 'auth_token', 'af998740c6f61df54603f1b17c3ea76c3468ce31970b445b6590289cd4fb5a40', '[\"*\"]', NULL, '2023-01-22 21:19:19', '2023-01-22 20:19:19', '2023-01-22 20:19:19'),
(584, 'App\\Models\\AkunPegawai', 1, 'auth_token', '4309833377a33fbfc58ff2ff6707044f219161c99a5fef5c0c0fe93b84db3cf0', '[\"*\"]', NULL, '2023-01-23 00:48:40', '2023-01-22 23:48:40', '2023-01-22 23:48:40'),
(585, 'App\\Models\\User', 2, 'auth_token', '65b081290c5741c63b6a113d7e4b411afc771f20f10324abefdad63f326e55e4', '[\"*\"]', '2023-01-23 00:28:45', '2023-01-23 00:49:49', '2023-01-22 23:49:49', '2023-01-23 00:28:45'),
(586, 'App\\Models\\User', 2, 'auth_token', '5c8c157a2affec0c34774fc45934421a875233babe62553cbd535761ee663840', '[\"*\"]', NULL, '2023-01-23 03:26:43', '2023-01-23 02:26:43', '2023-01-23 02:26:43'),
(587, 'App\\Models\\User', 2, 'auth_token', '13bbe6dd86fda7da531d412bbf0b13915bdd8267a9c50b1b35bdcd7a9170958a', '[\"*\"]', NULL, '2023-01-23 18:51:16', '2023-01-23 17:51:16', '2023-01-23 17:51:16'),
(588, 'App\\Models\\User', 3, 'auth_token', '6a299355539306c4edd7e91c48c92e9e795967ac4df6d3551a2f06d30f3860f9', '[\"*\"]', NULL, '2023-01-23 18:54:13', '2023-01-23 17:54:13', '2023-01-23 17:54:13'),
(589, 'App\\Models\\User', 2, 'auth_token', '12b9988ef960b74c28ce6b707a97307c38f1202e8e14f45d0ea077706c7ea877', '[\"*\"]', NULL, '2023-01-23 18:55:08', '2023-01-23 17:55:08', '2023-01-23 17:55:08'),
(590, 'App\\Models\\AkunPegawai', 1, 'auth_token', 'd905d6fc2fc1bdf40c51703151b064bd6b54c15c9c20ea76f1fe32dd14e77557', '[\"*\"]', NULL, '2023-01-23 18:55:27', '2023-01-23 17:55:27', '2023-01-23 17:55:27'),
(591, 'App\\Models\\User', 2, 'auth_token', 'd96cda98f288c35b30eeae791af29e1756ee6013fa4b5d3624b125197020a4a1', '[\"*\"]', NULL, '2023-01-24 00:29:45', '2023-01-23 23:29:45', '2023-01-23 23:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_karyawan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp` bigint(20) DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `det_alamat` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bidang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id`, `nama_perusahaan`, `jumlah_karyawan`, `admin_perusahaan`, `email_perusahaan`, `provinsi`, `kota`, `npwp`, `kode_pos`, `det_alamat`, `bidang`, `created_at`, `updated_at`) VALUES
(1, 'dasd', '123', 'ADMIN PT NARUTO', 'admin2as@gmail.com', 'asdsa', 'Madiun', 3213123, 63162, 'Desa Bakur', 'dsa', '2022-11-09 08:11:52', '2022-11-09 08:11:52'),
(2, 'dasd', '123', 'ADMIN PT NARUTO', 'admin2ass@gmail.com', 'asdsa', 'Madiun', 3213123, 63162, 'Desa Bakur', 'dsa', '2022-11-09 08:13:42', '2022-11-09 08:13:42'),
(3, 'PT HRIS Jaya', '34', 'Admin PT Naruto', 'fihan@gmail.com', 'Jawa Timur', 'Madiun', 12345, 63162, 'Desa Bakur', 'IT', '2022-12-20 17:05:01', '2022-12-20 17:05:01'),
(4, 'sda', '12321', 'Tes', 'g@gmail.com', 'sadasd', 'Madiun', 3213, 63162, 'Desa Bakur', 'dsa', '2022-12-25 21:42:09', '2022-12-25 21:42:09'),
(5, 'PT Ndeso', '12', 'Admin PT Fihan', 'fihan2as@student.ub.ac.id', 'Jawa Timur', 'Madiun', 12345, 63162, 'Desa Bakur', 'Pertanian', '2022-12-25 22:40:47', '2022-12-25 22:40:47'),
(6, 'dsa', '213', 'asd', 'fihanssssssssas@gmail.com', 'dsa', 'asd', 3213, 123, 'dsad', 'dsa', '2022-12-25 22:45:44', '2022-12-25 22:45:44'),
(7, 'PT Naruto', '60', 'Admin Jaya', 'admin2@gmail.com', 'Jawa Timur', 'Madiun', 1234, 63162, 'Desa Bakur', 'Pertanian', '2023-01-15 01:23:48', '2023-01-15 01:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `potongabsen`
--

CREATE TABLE `potongabsen` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `potongan` double(8,2) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `potongan`
--

CREATE TABLE `potongan` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `potongan`
--

INSERT INTO `potongan` (`id`, `email_admin`, `jenis_potongan`, `nominal`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', '-', 0, NULL, '2023-01-09 23:14:50', '2023-01-09 23:14:50'),
(2, 'admin@gmail.com', 'Telat Absen', 200000, NULL, '2023-01-10 00:46:52', '2023-01-10 00:46:52'),
(3, 'admin2@gmail.com', 'Tidak Ada Potongan', 0, NULL, '2023-01-15 01:23:48', '2023-01-15 01:23:48'),
(4, 'admin@gmail.com', 'Knteal', 90000, NULL, '2023-01-16 19:55:21', '2023-01-16 19:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `reqabsen`
--

CREATE TABLE `reqabsen` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pegawai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alasan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_pendukung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_req` enum('Diproses','Diterima','Ditolak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Diproses',
  `tanggal_req` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reqabsen`
--

INSERT INTO `reqabsen` (`id`, `id_admin`, `name`, `nama_lengkap`, `email`, `no_pegawai`, `alasan`, `bukti_pendukung`, `status_req`, `tanggal_req`, `created_at`, `updated_at`) VALUES
(5, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'Lupa', '1669008345070.png', 'Diterima', '2022-12-18', '2022-12-19 09:01:14', '2022-12-19 09:01:14'),
(6, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'Lupa bang', '2029_Muhammad Fihan Ashidiq_Surat Aktif Kuliah.pdf', 'Diterima', '2022-12-25', '2022-12-23 21:43:50', '2022-12-23 21:43:50'),
(7, 2, 'Fihan', 'Muhammad Fihan Ashidiq', 'fihan2as@gmail.com', '2578126312', 'Lupa', 'blurry-gradient.png', 'Diterima', '2022-12-27', '2022-12-28 01:20:23', '2022-12-28 01:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `reset_code_passwords`
--

CREATE TABLE `reset_code_passwords` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reset_code_passwords`
--

INSERT INTO `reset_code_passwords` (`id`, `email`, `code`, `created_at`, `updated_at`) VALUES
(2, 'admin@gmail.com', '257513', '2022-12-25 22:15:01', '2022-12-25 22:15:01'),
(5, 'sasasa@gmail.com', '985282', '2022-12-25 22:22:01', '2022-12-25 22:22:01'),
(6, 'admin@gmail.com', '122891', '2022-12-25 22:22:11', '2022-12-25 22:22:11'),
(7, 'sadas@gmail.com', '505559', '2022-12-25 22:26:35', '2022-12-25 22:26:35'),
(9, 'admin@gmail.com', '171751', '2022-12-25 22:28:31', '2022-12-25 22:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `riwayatgaji`
--

CREATE TABLE `riwayatgaji` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` int(10) UNSIGNED NOT NULL,
  `id_golongan` int(10) UNSIGNED DEFAULT NULL,
  `tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal_tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal_bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_potongan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potong_absen` int(11) DEFAULT NULL,
  `total_lembur` int(11) DEFAULT NULL,
  `jamlembur_total` int(11) DEFAULT NULL,
  `tidak_masuk` int(11) DEFAULT NULL,
  `gaji_kotor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji_bersih` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji_pokok` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Diambil',
  `tanggal_ambil` date DEFAULT NULL,
  `no_transaksi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `riwayatgaji`
--

INSERT INTO `riwayatgaji` (`id`, `id_admin`, `email`, `id_jabatan`, `id_golongan`, `tunjangan`, `nominal_tunjangan`, `total_tunjangan`, `bonus`, `nominal_bonus`, `total_bonus`, `potongan`, `nominal_potongan`, `total_potongan`, `potong_absen`, `total_lembur`, `jamlembur_total`, `tidak_masuk`, `gaji_kotor`, `gaji_bersih`, `gaji_pokok`, `status`, `tanggal_ambil`, `no_transaksi`, `created_at`, `updated_at`) VALUES
(5, 2, 'fihan2as@gmail.com', 2, NULL, 'Tunjangan Makan', '300000', '300000', '-', '0', '0', '-', '0', '0', 2800000, 260116, 8, 28, '760116', '760116', '3000000', 'Cair', '2023-01-23', '62419957', '2023-01-22 17:06:08', '2023-01-22 17:27:47'),
(6, 2, 'fihan2as@gmail.com', 2, NULL, 'Tunjangan Hari Raya,Tunjangan Makan', '3000000,300000', '3300000', 'Tes Bonus,Bonus', '200000,300000', '500000', '-', '0', '0', 2800000, 260116, 8, 28, '4000000', '4000000', '3000000', 'Mengajukan', '2023-01-23', '26943300', '2023-01-22 17:24:32', '2023-01-22 17:24:48'),
(7, 2, 'fihan2as@gmail.com', 2, NULL, 'Tunjangan Makan', '300000', '300000', 'Tes Bonus', '200000', '200000', '-', '0', '0', 2800000, 260116, 8, 28, '960116', '960116', '3000000', 'Cair', '2023-01-23', '81157090', '2023-01-22 17:30:01', '2023-01-22 17:48:36'),
(8, 2, 'fihan2as@student.ub.ac.id', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4500000, 0, 0, 30, NULL, NULL, NULL, 'Belum Diambil', '2023-01-23', '46453566', '2023-01-22 18:07:12', '2023-01-22 18:07:12'),
(9, 2, 'fihan2as@gmail.com', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2800000, 260116, 8, 28, NULL, NULL, NULL, 'Belum Diambil', '2023-01-23', '15236474', '2023-01-22 18:08:16', '2023-01-22 18:08:16'),
(10, 2, 'fihan2as@gmail.com', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2800000, 260116, 8, 28, NULL, NULL, NULL, 'Belum Diambil', '2023-01-23', '45865361', '2023-01-23 02:27:45', '2023-01-23 02:27:45'),
(11, 2, 'fihan2as@student.ub.ac.id', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4500000, 0, 0, 30, NULL, NULL, NULL, 'Belum Diambil', '2023-01-23', '13271378', '2023-01-23 02:31:44', '2023-01-23 02:31:44');

-- --------------------------------------------------------

--
-- Table structure for table `table_master_cuti_tahunan`
--

CREATE TABLE `table_master_cuti_tahunan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cuti` int(10) UNSIGNED NOT NULL,
  `jumlah_cuti` int(11) NOT NULL,
  `cuti_terpakai` int(11) NOT NULL,
  `sisa_cuti` int(11) NOT NULL,
  `tahun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_master_cuti_tahunan`
--

INSERT INTO `table_master_cuti_tahunan` (`id`, `id_admin`, `email`, `id_cuti`, `jumlah_cuti`, `cuti_terpakai`, `sisa_cuti`, `tahun`, `created_at`, `updated_at`) VALUES
(16, 2, 'fihan2as@gmail.com', 1, 45, 38, 7, '2022', NULL, NULL),
(17, 2, 'fihan2as@student.ub.ac.id', 1, 45, 28, 17, '2022', NULL, NULL),
(18, 2, 'a@gmail.com', 1, 45, 5, 40, '2022', NULL, NULL),
(19, 2, 'b@gmail.com', 1, 45, 0, 45, '2022', NULL, NULL),
(20, 2, 'fihan2222as@gmail.com', 1, 45, 0, 45, '2022', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tunjangan`
--

CREATE TABLE `tunjangan` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_tunjangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tunjangan`
--

INSERT INTO `tunjangan` (`id`, `email_admin`, `jenis_tunjangan`, `nominal`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', '-', 0, NULL, '2023-01-09 23:14:32', '2023-01-09 23:14:32'),
(2, 'admin@gmail.com', 'Tunjangan Hari Raya', 3000000, NULL, '2023-01-10 00:02:25', '2023-01-10 00:02:25'),
(3, 'admin@gmail.com', 'Tunjangan Makan', 300000, NULL, '2023-01-10 00:02:33', '2023-01-10 00:02:33'),
(4, 'admin2@gmail.com', 'Tidak Ada Tunjangan', 0, NULL, '2023-01-15 01:23:48', '2023-01-15 01:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `useractivity`
--

CREATE TABLE `useractivity` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Manager','Admin','Pegawai') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `det_alamat` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp` bigint(20) DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `bidang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `moto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_pt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logout_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `jabatan`, `nama_perusahaan`, `provinsi`, `kota`, `det_alamat`, `npwp`, `kode_pos`, `bidang`, `logo`, `gambar`, `status`, `moto`, `website_pt`, `jumlah_karyawan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `logout_at`) VALUES
(2, 'Admin', 'admin@gmail.com', 'Admin', 'CEO', 'PT AMS Indonesia', 'Jawa Tengah', 'Surakarta', 'Sekolah Vokasi Universitas Sebelas Maret, Surakarta, Jawa Tengah', 123456789, 12345, 'IT', 'logo.png', NULL, 'Diterima', NULL, NULL, 50, NULL, '$2y$10$Xo8rZOd7ToghZkiSicgyN.4lOc917ZEP7TxZBWrNGp9tVFqOO439y', NULL, NULL, '2022-12-26 03:39:07', NULL),
(3, 'Super Admin', 'superadmin@ams.com', 'Manager', 'SuperAdmin', 'PT AMS Indonesia', 'Jawa Tengah', 'Surakarta', 'Sekolah Vokasi Universitas Sebelas Maret, Surakarta, Jawa Tengah', 123456789, 12345, 'HR Management', NULL, NULL, 'Diterima', NULL, NULL, 1000, NULL, '$2y$10$xRCAa1sS3asYKsHnIic3KunJFzBdc9LsHxlRUEyhgouYSLF0YrlK2', NULL, NULL, NULL, NULL),
(4, 'ADMIN PT NARUTO', 'admin2as@gmail.com', 'Admin', 'Pegawai Negeri Sipil', 'dasd', 'asdsa', 'Madiun', 'Desa Bakur', 3213123, 63162, 'dsa', NULL, NULL, 'Nonaktif', 'dsa', 'dsa', 123, NULL, '$2y$10$yNqgUJj/iAW9K3yVBZ1/IOOsOavUKiGGlRwPA5ZSk/4ppFwqATAme', NULL, NULL, '2022-12-20 17:10:47', NULL),
(5, 'ADMIN PT NARUTO', 'admin2ass@gmail.com', 'Admin', 'Pegawai Negeri Sipil', 'dasd', 'asdsa', 'Madiun', 'Desa Bakur', 3213123, 63162, 'dsa', NULL, NULL, 'Diterima', 'dsa', 'dsa', 123, NULL, '$2y$10$Nwib0EXlOudl3jCkMBVZju7W4MrUA7/O2bXpKjoJgUbIOWP1IQ3XS', NULL, NULL, '2022-11-09 08:14:25', NULL),
(6, 'Admin PT Naruto', 'fihan@gmail.com', 'Admin', 'CEO', 'PT HRIS Jaya', 'Jawa Timur', 'Madiun', 'Desa Bakur', 12345, 63162, 'IT', NULL, NULL, 'Diterima', NULL, NULL, 34, NULL, '$2y$10$0omhInDWTDwkj5lOfzuwGeZtsh9Kv/o63/EfP/.PdHNkqEO89H2L.', NULL, NULL, '2022-12-20 17:05:56', NULL),
(7, 'Tes', 'g@gmail.com', 'Admin', 'Manager', 'sda', 'sadasd', 'Madiun', 'Desa Bakur', 3213, 63162, 'dsa', NULL, NULL, 'Pending', NULL, NULL, 12321, NULL, '$2y$10$rHnLkIP3a5sRxYt85LONKOf5Tb1YKSh1FXRCNijXb4iNj.KgLnDZ.', NULL, NULL, NULL, NULL),
(9, 'asd', 'fihanssssssssas@gmail.com', 'Admin', 'dasd', 'dsa', 'dsa', 'asd', 'dsad', 3213, 123, 'dsa', NULL, NULL, 'Pending', NULL, NULL, 213, NULL, '$2y$10$rQuL6ReFzceGMJ7b0Ll2te2boTssm8lb8MuHiyMvS3Qxbb5tJ1HnC', NULL, NULL, NULL, NULL),
(10, 'Admin Jaya', 'admin2@gmail.com', 'Admin', 'CTO and CEO', 'PT Naruto', 'Jawa Timur', 'Madiun', 'Desa Bakur', 1234, 63162, 'Pertanian', 'menu (1).png', NULL, 'Diterima', NULL, NULL, 60, NULL, '$2y$10$pa9nid5CVlaXqIuPb5I77.NQyc4/sfnZaV.z4Gf3I4mEM6EsMxWYi', NULL, NULL, '2023-01-15 01:50:33', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensipegawai`
--
ALTER TABLE `absensipegawai`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `akunpegawai`
--
ALTER TABLE `akunpegawai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `akunpegawai_email_unique` (`email`),
  ADD KEY `akunpegawai_id_admin_foreign` (`id_admin`),
  ADD KEY `akunpegawai_id_jabatan_foreign` (`id_jabatan`),
  ADD KEY `akunpegawai_id_golongan_foreign` (`id_golongan`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cuti_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `cuti_perusahaans`
--
ALTER TABLE `cuti_perusahaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti_tahunans`
--
ALTER TABLE `cuti_tahunans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datagaji`
--
ALTER TABLE `datagaji`
  ADD UNIQUE KEY `datagaji_email_unique` (`email`),
  ADD KEY `datagaji_id_jabatan_foreign` (`id_jabatan`),
  ADD KEY `datagaji_id_golongan_foreign` (`id_golongan`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `golongan_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harikerja`
--
ALTER TABLE `harikerja`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `harikerja_id_unique` (`id`);

--
-- Indexes for table `izin`
--
ALTER TABLE `izin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jabatan_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `jamabsen`
--
ALTER TABLE `jamabsen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jamabsen_id_unique` (`id`),
  ADD KEY `jamabsen_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laporan_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lembur_id_admin_foreign` (`id_admin`);

--
-- Indexes for table `lokasipegawai`
--
ALTER TABLE `lokasipegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_cuti_perusahaan`
--
ALTER TABLE `master_cuti_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pegawais`
--
ALTER TABLE `pegawais`
  ADD UNIQUE KEY `pegawais_email_unique` (`email`),
  ADD KEY `pegawais_id_golongan_foreign` (`id_golongan`),
  ADD KEY `pegawais_id_jabatan_foreign` (`id_jabatan`);

--
-- Indexes for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penggajian`
--
ALTER TABLE `penggajian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `perusahaan_email_perusahaan_unique` (`email_perusahaan`);

--
-- Indexes for table `potongabsen`
--
ALTER TABLE `potongabsen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `potongan`
--
ALTER TABLE `potongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reqabsen`
--
ALTER TABLE `reqabsen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_code_passwords`
--
ALTER TABLE `reset_code_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reset_code_passwords_email_index` (`email`);

--
-- Indexes for table `riwayatgaji`
--
ALTER TABLE `riwayatgaji`
  ADD PRIMARY KEY (`id`),
  ADD KEY `riwayatgaji_id_golongan_foreign` (`id_golongan`);

--
-- Indexes for table `table_master_cuti_tahunan`
--
ALTER TABLE `table_master_cuti_tahunan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `table_master_cuti_tahunan_id_cuti_foreign` (`id_cuti`);

--
-- Indexes for table `tunjangan`
--
ALTER TABLE `tunjangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useractivity`
--
ALTER TABLE `useractivity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensipegawai`
--
ALTER TABLE `absensipegawai`
  MODIFY `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `akunpegawai`
--
ALTER TABLE `akunpegawai`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cuti`
--
ALTER TABLE `cuti`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cuti_perusahaans`
--
ALTER TABLE `cuti_perusahaans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cuti_tahunans`
--
ALTER TABLE `cuti_tahunans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `harikerja`
--
ALTER TABLE `harikerja`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `izin`
--
ALTER TABLE `izin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `jamabsen`
--
ALTER TABLE `jamabsen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lokasipegawai`
--
ALTER TABLE `lokasipegawai`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_cuti_perusahaan`
--
ALTER TABLE `master_cuti_perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=559;

--
-- AUTO_INCREMENT for table `penggajian`
--
ALTER TABLE `penggajian`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=592;

--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `potongabsen`
--
ALTER TABLE `potongabsen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `potongan`
--
ALTER TABLE `potongan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reqabsen`
--
ALTER TABLE `reqabsen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reset_code_passwords`
--
ALTER TABLE `reset_code_passwords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `riwayatgaji`
--
ALTER TABLE `riwayatgaji`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `table_master_cuti_tahunan`
--
ALTER TABLE `table_master_cuti_tahunan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tunjangan`
--
ALTER TABLE `tunjangan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `useractivity`
--
ALTER TABLE `useractivity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `akunpegawai`
--
ALTER TABLE `akunpegawai`
  ADD CONSTRAINT `akunpegawai_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `akunpegawai_id_golongan_foreign` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`),
  ADD CONSTRAINT `akunpegawai_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`);

--
-- Constraints for table `cuti`
--
ALTER TABLE `cuti`
  ADD CONSTRAINT `cuti_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `akunpegawai` (`id_admin`);

--
-- Constraints for table `datagaji`
--
ALTER TABLE `datagaji`
  ADD CONSTRAINT `datagaji_id_golongan_foreign` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`),
  ADD CONSTRAINT `datagaji_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`);

--
-- Constraints for table `golongan`
--
ALTER TABLE `golongan`
  ADD CONSTRAINT `golongan_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `users` (`id`);

--
-- Constraints for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD CONSTRAINT `jabatan_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `users` (`id`);

--
-- Constraints for table `jamabsen`
--
ALTER TABLE `jamabsen`
  ADD CONSTRAINT `jamabsen_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `users` (`id`);

--
-- Constraints for table `laporan`
--
ALTER TABLE `laporan`
  ADD CONSTRAINT `laporan_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `akunpegawai` (`id_admin`);

--
-- Constraints for table `lembur`
--
ALTER TABLE `lembur`
  ADD CONSTRAINT `lembur_id_admin_foreign` FOREIGN KEY (`id_admin`) REFERENCES `akunpegawai` (`id_admin`);

--
-- Constraints for table `pegawais`
--
ALTER TABLE `pegawais`
  ADD CONSTRAINT `pegawais_id_golongan_foreign` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`),
  ADD CONSTRAINT `pegawais_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`);

--
-- Constraints for table `riwayatgaji`
--
ALTER TABLE `riwayatgaji`
  ADD CONSTRAINT `riwayatgaji_id_golongan_foreign` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

--
-- Constraints for table `table_master_cuti_tahunan`
--
ALTER TABLE `table_master_cuti_tahunan`
  ADD CONSTRAINT `table_master_cuti_tahunan_id_cuti_foreign` FOREIGN KEY (`id_cuti`) REFERENCES `master_cuti_perusahaan` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
